# WiscMhb: Open Source Multi-Harmonic Balance code for Nonlinear Dynamics #

This is an open source code to compute the periodic orbits of nonlinear 
dynamical systems. The code is based upon the Multi-Harmonic Balance (MHB)
method that allows for efficient computation of the periodic orbits. This can
be used to compute nonlinear normal modes (NNM)s of conservative systems or the
forced response of non-conservative systems. 

- Version: 2.0
- Date: 08/08/2019
- Authors: Christopher Van Damme, Matt Allen, Alecio Madrid.
- Contact: cvandamme@wisc.edu, cvandamme13@gmail.com, matt.allen@wisc.edu

## Capabilities #

- Nonlinear normal mode (NNM) computation for unforced systems with no damping
- Nonlinear frequency response functions (NLFRF) computation for forced systems with damping
- Pseudo arc-length continuation routine is used with a Moore-Penrose correction scheme
- Graphical user interface (GUI) is available for users to be able to visualize and control the continuation

## Dynamic Systems Supported #

- Geometrically nonlinear finite element models via the open source finite element code Osfern (https://bitbucket.org/cvandamme/osfern/src/master/)
- Geometrically nonlinear reduced order models 
- Iwan models representing the nonlinearity in jointed structures 
- Spring-mass systems with discerete nonlinearities (under development)

## Where to Start #

- The best way to start is to to src/demos to become familiar with the 
modeling stragey and analysis setup. The demonstrations are
intended to show not only how to use the code, but also point to class 
methods which provide critical code functionality.

- The entirety of the MATLAB code proper is contained in /src/main. 

- User should run the script /src/SetPathScriptOsFern.m to add all required paths. 

- Future documentation will be located in the /doc/ folder.

- Finally, the best way to see a list of all class methods and properties (aside from reading
the source code in detail) is to type "doc Class" at the MATLAB command prompt to open
the HTML documentation tool. 

## Current TODO List #

- Spring-Mass system subclass and plotting features
- An automated testing framework. 
- Iterative sparse solvers during correction routine for large finite element models




