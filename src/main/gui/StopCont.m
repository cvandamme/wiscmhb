function StopCont(obj)
% StopCont sets endcont to 0 so that continuation stops at the end of the next
% continuation cycle and saves the HB object data.
%
% Input:
%           obj = (HB object) HB class instance
%

    % Display stoping continuation message to console
    disp(' ')
    disp('----------------------------------------------------------------')
    disp('                 Stopping WiscMHB Continuation                  ')
    disp('----------------------------------------------------------------')
    
    % Set endcont equal to 0 to signal that  
    obj.HBcontrol.endcont = 0;
    
    % Save HB object data
    try
        if obj.HBcontrol.save
            save([obj.SAVEdir,'\',obj.Name],'obj', '-v7.3' )
        end
    catch
        save([obj.SAVEdir,'\',obj.Name],'obj', '-v7.3' )
    end
end