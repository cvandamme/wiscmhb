function RestartCont(obj)
% RestartCont enables the user to select a  solution point on the NNM curve
% and restart the continuation from there
%
% Input:
%           obj = (HB object) HB class instance
%

    % Display solution restart message to console
    disp('------------------------------------------------------------------------')
    disp('Restarting Continuation at Selected Point with Current Solution Controls')
    disp('------------------------------------------------------------------------')

    % Enable crosshair pointer for secting solution to restart continuation
    set(gcf,'pointer','crosshair')
    keydown=waitforbuttonpress;
    set(gcf,'pointer','arrow')
    if keydown
        return
    end
    
    % Get data corresponding to selected point
    FEPinput=get(gca,'currentpoint');
    
    % Remove extraneous information
    FEPinput=FEPinput(1,1:2);
    
    % Get energy for selected point
    Et = FEPinput(1); 
    
    % Get frequency for selected point
    Wt = FEPinput(2);
    
    % Get vector of calculated solution energy values
    EnergyVec = obj.E(1:obj.solpt);
    
    % Get vector of calculated solution frequency values
    FreqVec = obj.w(1:obj.solpt);
    
    % Check if frequency values are in Hz or rad/s
    if strcmp(obj.HBcontrol.freq,'Hz')
        FreqVec = FreqVec/(2*pi);
    end
    
    % Solve minimization problem to get closes solution to selected point
    [~,Ip]=min((FreqVec-Wt).^2+((EnergyVec)-Et).^2);
    
    % Temporarily display chosen solution
    hold on
    restart_mark=plot(EnergyVec(Ip),FreqVec(Ip),'sk','tag','temp','MarkerSize',20);
    pause(1)
    hold off
    delete(restart_mark)
    
    % Renumber and Order Solution Matrices
    obj.solpt = Ip;
    obj.E(:,Ip+1:end) = 0;
    obj.w(:,Ip+1:end) = 0;
    obj.x(:,Ip+1:end) = 0;
    obj.z(:,Ip+1:end) = 0;
    obj.P(:,Ip+1:end) = 0;
%     obj.FloqExp(:,Ip+1:end) = 0;
    
    % Update GUI to reflect reordered solution matrices
    WiscMHBGUI('plotSOL')
end