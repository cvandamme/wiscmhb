function ContSettings(obj)
% AdvancedCont provides additional controls for 'advanced' continuation
% options
%
% Input:
%           obj = (HB object) HB class instance
%

    % Advanced dialog box for additional solution controls.
    prompt = {  'Sign(Step)',...
                '|Step Min|',...
                '|Step Max|'};
    dlg_title = 'Continuation Settings';
    num_lines = 1;
    if obj.HBcontrol.stpSign > 0
        sign = '+';
    else
        sign = '-';
    end
    def = {sign,num2str(obj.HBcontrol.stpmin),num2str(obj.HBcontrol.stpmax)};
    [advctparam] = inputdlg(prompt,dlg_title,num_lines,def);
    if ~isempty(advctparam)
        if strcmp(advctparam{1},'+')
            obj.HBcontrol.stpSign = 1;
        elseif strcmp(advctparam{1},'-')
            obj.HBcontrol.stpSign = -1;
        end
       
        obj.HBcontrol.stpmin = str2double(advctparam{2});
        obj.HBcontrol.stpmax = str2double(advctparam{3});
        
    end
end  
