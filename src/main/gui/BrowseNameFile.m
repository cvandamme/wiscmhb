function BrowseNameFile(obj)

    [fname]=uigetfile('*.mat','Select result MAT file','name_file');

    if fname~=0
        % obj.Name=fname(1:end-4);
        load(fname,'obj'); clear fname; close gcf
        obj.HBcontrol.start = 1; 
        obj.solpt = obj.solpt-1;
        WiscMHBGUI('initialize',obj)
        WiscMHBGUI('plotSOL')
    end
        
end