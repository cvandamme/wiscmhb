function CloseMHB(obj)
% CloseMHB closes the current WiscMHBGUI, deletes the figure pointer and
% saves the HB class data to its corresponding save directory.
%
% Input:
%           obj = (HB object) HB class instance
%

    % Display closing message on console
    disp(' ')
    disp('----------------------------------------------------------------')
    disp('                         Closing WiscMHB                        ')
    disp('----------------------------------------------------------------')
    
    % Check if there is any data to save
    if ~isempty(obj.solpt)
        
        % Set endcont to 0 so that continuation ends at the end of current
        % continuation cycle
        obj.HBcontrol.endcont = 0;
        
        % Save class data
        try
            if obj.HBcontrol.save
                save([obj.SAVEdir,'\',obj.Name],'obj', '-v7.3')
            end
        catch
            warning('save failed')
        end
    end

    % Close gui and delete figure pointer
    delete(gcf);
    
end
