function StartCont(obj)
% Start continuation updates continuation variables from GUI handles and
% then pass control to HBcont to begin continuation from the current 
% solution point
%
% Input:
%           obj = (HB object) HB class instance
%

    % Display starting message on console
    disp('------------------------------------------------------------------------')
    disp('Starting Continuation')
    disp('------------------------------------------------------------------------')

    % Get updated properties from GUI handles
    obj.HBcontrol.nbstep = str2double(get(obj.GUI.ControlHandle.nbstep,'String'));
%     obj.HBcontrol.stpint = str2double(get(obj.GUI.ControlHandle.stpint,'String'));               
    obj.Name = get(obj.GUI.ControlHandle.name_file,'String');

    

%     obj.HBcontrol.LinSolve = 'Direct';

    %   obj.HBcontrol.stpdf = 3; %Need input space for this
    % Add Control to Select Initial Point
    % -- code here or new function?? --

    % Resume Continuation
    obj.HBcontrol.endcont=1;
    
    % Pass control over to HB continuation method
    obj.Continue();
end