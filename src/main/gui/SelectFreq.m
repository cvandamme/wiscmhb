function SelectFreq(obj)
% 
%
% Input:
%           obj = (HB object) HB class instance
%
    if strcmp(obj.HBcontrol.freq,'Hz')
        obj.HBcontrol.freq='rad/s';
    elseif strcmp(obj.HBcontrol.freq,'rad/s')
        obj.HBcontrol.freq='Hz';
    else
        obj.HBcontrol.freq='rad/s';
    end
    uicontrol('Style','Pushbutton','String',['Freq (' obj.HBcontrol.freq ')'],...
        'uni','nor','pos',[0.84 0.07 0.149 0.05],'callback',...
        'WiscMHBGUI(''selectfreq'')');
    if obj.HBcontrol.endcont==0
        obj.solpt=obj.solpt-1;
        WiscMHBGUI('plotSOL');
        obj.solpt=obj.solpt+1;
    end
    sp_axes=getappdata(gcf,'Subplot_axes');
    set(get(sp_axes{1},'XLabel'),'string',['Frequency (' obj.HBcontrol.freq ')'],'FontSize',16);
    set(get(sp_axes{2},'XLabel'),'string',['Frequency (' obj.HBcontrol.freq ')'],'FontSize',16);
    set(get(sp_axes{3},'YLabel'),'string',['Frequency (' obj.HBcontrol.freq ')'],'FontSize',16);    
end