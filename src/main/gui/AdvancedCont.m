function AdvancedCont(obj)
% AdvancedCont provides additional controls for 'advanced' continuation
% options
%
% Input:
%           obj = (HB object) HB class instance
%

    % Advanced dialog box for additional solution controls.
    prompt = {  'Tolerance',...
                'NH',...
                'min w (rad/sec)',...
                'max w (rad/sec)',...
                'Optimal # of Iterations',...
                'Max # of Iterations',...
                '# of Time Steps'};
    dlg_title = 'Advanced Continuation Parameters';
    num_lines = 1;
    def = {num2str(obj.HBcontrol.tol),num2str(obj.nH),num2str(obj.stopCriteria.frequency(1)),...
        num2str(obj.stopCriteria.frequency(2)),num2str(obj.HBcontrol.optiter),...
        num2str(obj.HBcontrol.maxiter),num2str(obj.HBcontrol.nT)};
    [advctparam] = inputdlg(prompt,dlg_title,num_lines,def);
    if ~isempty(advctparam)
        obj.HBcontrol.tol = str2double(advctparam{1});
        
        % Get current value from nH control handle
        nH = str2double(advctparam{2});

        % Check if the number of harmonics was changed
        if nH > obj.nH

            % Resize prediction matrix
            P = zeros((2*nH+1)*obj.nDOF+1,obj.HBcontrol.nSols);

            % Fill in P with already found values
            P(1:obj.nDOF*(2*obj.nH+1)+1,:) = obj.P(1:obj.nDOF*(2*obj.nH+1)+1,:);

            % Update obj.P
            obj.P = P;

            % Resize fourier coefficients matrix
            z = zeros((2*nH+1)*obj.nDOF,obj.HBcontrol.nSols);

            % Fill in z with already found values
            z(1:(2*obj.nH+1)*obj.nDOF,1:size(obj.z,2)) = obj.z;

            % Update obj.z
            obj.z = z;

            % Update obj.nH
            obj.nH = nH;

        % Check if the number of harmonics was decreased
        elseif nH < obj.nH

            % Raise error and specify that number of harmonics must be >= to existing number
            error('NH must be greater than or equal to the current number of harmonics');
        end
        obj.stopCriteria.frequency(1) = str2double(advctparam{3});
        obj.stopCriteria.frequency(2) = str2double(advctparam{4});
        obj.HBcontrol.optiter = str2double(advctparam{5});
        obj.HBcontrol.maxiter = str2double(advctparam{6});
        obj.HBcontrol.nT = str2double(advctparam{7});
    end
end  

