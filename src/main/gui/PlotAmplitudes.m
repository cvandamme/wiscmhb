function PlotAmplitudes(obj)
% PlotAmplitudes takes the object data and updates the GUI harmonic
% amplitude plots.
%
% Input:
%           obj = (HB object) HB class instance
%
    n_plot=obj.solpt-1;
    z_plot=obj.z(:,1:(n_plot));
    w_plot=obj.w(1:(n_plot));
    if strcmp(obj.HBcontrol.freq,'Hz')
        w_plot=w_plot/2/pi;
    end
    for n=1:obj.nDOF
        reg{n}=['DOF' num2str(n)];
    end

    harm_sin_fgn=figure('Visible','off','Renderer','zbuffer','NumberTitle','off','Name','SIN Harmonics');
    set(harm_sin_fgn,'uni','nor','position',[.05 .1 .85 .75],'Color',[1 1 1],'toolbar','figure');
    subplot(2,round(.5*(obj.nH+1)),1); plot(w_plot,z_plot(1:obj.nDOF,1:n_plot)); 
    xlim([w_plot(1) w_plot(end)]); grid on; legend(reg,'Location','best');
    title('0 Harmonic'); xlabel(['Frequency (' obj.HBcontrol.freq ')']); 
    for jj=0:length(0:2:obj.nH)-2
        subplot(2,round(.5*(obj.nH+1)),jj+2);
        plot(w_plot,z_plot((1:4)+(3+obj.nDOF*jj)*obj.nDOF,1:n_plot))
        title(['Sin_{' num2str(2*jj+2) '} Harmonic']); grid on;
        xlabel(['Frequency (' obj.HBcontrol.freq ')']); xlim([w_plot(1) w_plot(end)])
    end
    for jj=0:length(1:2:obj.nH)-1
        subplot(2,round(.5*(obj.nH+1)),round(.5*(obj.nH+1))+jj+1);
        plot(w_plot,z_plot((1:4)+(1+obj.nDOF*jj)*obj.nDOF,1:n_plot))
        title(['Sin_{' num2str(2*jj+1) '} Harmonic']); grid on;
        xlabel(['Frequency (' obj.HBcontrol.freq ')']); xlim([w_plot(1) w_plot(end)])
    end

    harm_cos_fgn=figure('Visible','off','Renderer','zbuffer','NumberTitle','off','Name','COS Harmonics');
    set(harm_cos_fgn,'uni','nor','position',[.1 .1 .85 .75],'Color',[1 1 1],'toolbar','figure');
    subplot(2,round(.5*(obj.nH+1)),1); plot(w_plot,z_plot(1:obj.nDOF,1:n_plot)); 
    grid on; xlim([w_plot(1) w_plot(end)]); legend(reg,'Location','best');
    title('0 Harmonic'); xlabel(['Frequency (' obj.HBcontrol.freq ')']);
    for jj=0:length(0:2:obj.nH)-2
        subplot(2,round(.5*(obj.nH+1)),jj+2);
        plot(w_plot,z_plot((1:4)+(4+obj.nDOF*jj)*obj.nDOF,1:n_plot))
        title(['Cos_{' num2str(2*jj+2) '} Harmonic']); grid on;
        xlabel(['Frequency (' obj.HBcontrol.freq ')']); xlim([w_plot(1) w_plot(end)])
    end
    for jj=0:length(1:2:obj.nH)-1
        subplot(2,round(.5*(obj.nH+1)),round(.5*(obj.nH+1))+jj+1);
        plot(w_plot,z_plot((1:4)+(2+obj.nDOF*jj)*obj.nDOF,1:n_plot))
        title(['Cos_{' num2str(2*jj+1) '} Harmonic']); grid on;
        xlabel(['Frequency (' obj.HBcontrol.freq ')']); xlim([w_plot(1) w_plot(end)])
    end

    set(harm_sin_fgn,'Visible','on'); set(harm_cos_fgn,'Visible','on');

end