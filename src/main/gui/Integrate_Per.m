function [] = Integrate_Per(obj)
    
    % Get pointer to edit text field
    editText = obj.GUI.ControlHandle.NSteps_Edit;

    % Get string value of edit text field
    val = editText.String;

    % Check if val is empty
    if isempty(val)
        fprintf('Number of integration time steps was empty, defaulting to 250'); 

        % Default to 250 time steps
        nSteps = 250;
    else
        % Check if val is an integer
        try
            nSteps = ceil(str2num(val)); %#ok<*ST2NM>
        catch
            fprintf('Number of integration time steps was not understood, defaulting to 250');

            % Default to 250 time steps
            nSteps = 250;
        end
    end

    % Call Calc_Periodicity
    obj.Calc_Periodicity(nSteps);
end