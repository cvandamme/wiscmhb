function PlotSolution()
% PlotSolution takes the object data and updates the NNM solution plot and
% physical displacement vs frequency plot
%

    % grab information from the current figure 
    % !!! This needs to be changed to a class, if the current figure is not
    % this then an error occurs !!! 
    sp_data=getappdata(gcf,'Subplot_data');
    sp_axes=getappdata(gcf,'Subplot_axes');
    obj=getappdata(gcf,'obj');

    % Check whether frequency is in rad/s or in hz
    if strcmp(obj.HBcontrol.freq,'Hz')
        % Divide by 2 pi to convert frequency to hz
        w_plot = obj.w(1:obj.solpt)/(2*pi);
    else
        % If frequency is already in hz than just get frequency values
        w_plot = obj.w(1:obj.solpt);
    end

    % Plot physical amplitude
    obj.PlotPhysical(sp_axes,sp_data);
    
    % Modal amplitude vs frequency plot
    obj.PlotModal(sp_axes,sp_data)
    
    % Frequency Energy plot
    set(sp_data{3}, 'XData',obj.E(1:obj.solpt), 'YData', w_plot)

    % Harmonic Coefficient Plot
    set(sp_data{4}, 'YData',abs(obj.z(:,obj.solpt)))

    % limitrate
    drawnow 
    
end