function DeleteCont(obj)
%
%
% Input:
%           obj = (HB object) HB class instance
%
    disp('Deleting Current Continuation File')
    if exist([obj.Name,'.mat'],'file')~=0
        button = questdlg(['Do you want to delete the file ',obj.Name,'.mat?'],'Delete','Yes','No','No');
        if strcmp(button,'Yes')
            delete([obj.Name,'.mat']);
        end
    else
        button = questdlg(['The file ',obj.Name,' does not exist'],'Delete','Ok','Ok');
    end


end