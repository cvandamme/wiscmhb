function [] = WiscMhbGui(varargin)
% WiscMHBGUI generates the GUI for WiscMHB continuation and manages future
% action or changes made using GUI handles.
%
% Input:
%      varargin{1} = Empty or 'initialize' for initialization otherwise
%                         name of desired GUI action
%      varargin{2} = (HB object) HB object instance to load data from
%
%
% Add folder with subfunctions to search path
fullpath = mfilename('fullpath');

endout=regexp(fullpath,filesep,'split');
endout = endout(1:end-1);

path = [];

for i=2:length(endout)
    path = [path, '\',endout{i}];
end

addpath([path,'\gui'])

% Check if arguments were not included
if isempty(varargin)
    
    % If varargin is empty then the default action is initialization
    action = 'initialize';
    
    % Check which action was passed in
else
    
    % Set action to first argument
    action=varargin{1};
    
    % If not initializing a new instance then load object data from GUI
    if strcmp(action,'initialize')==0
        
        % Get pointer to HB object reference from GUI
        obj=getappdata(gcf,'obj');
        
        % If initializing a new GUI instance then get HB object pointer
    else
        
        % Make sure object pointer was passed into function
        if length(varargin) < 2
            
            % Throw an error if object reference was not passed to
            % function
            error('Object reference is required to initialize a new instance')
        end
        
        % Set obj pointer to the reference that was passed in
        obj=varargin{2};
    end
end

% Initialize a new GUI instance with data from the passed in object
% reference
if strcmp(action,'initialize')
    
    % Call subfunction to initialzie object
    Initialize(obj);
    
    % Start continuation and assign any GUI values to the control structure
elseif strcmp(action,'startCONT')
    
    % Call subfunction to start continuation
    StartCont(obj);
    
    % Restarts analysis at a selected solution point while retaining all previous solution data
    % and uses the FEP plot to determine the desired solution.
elseif strcmp(action,'restartCONT')
    
    % Call subfunction to restart continuation
    RestartCont(obj);
    
    % Stop continuation at current solution and save HB object instance
elseif strcmp(action,'stopCONT')
    
    % Call subfunction to stop continuation
    StopCont(obj);
    
    %
elseif strcmp(action,'selectfreq')
    
    % Call subfunction to select frequency
    SelectFreq(obj);
    
    %
elseif strcmp(action,'browse_namefile')
    
    % Call browse name file subfunction
    BrowseNameFile(obj);
    
    % Stop continuation at end of current solution point loop and delete
    % data
elseif strcmp(action,'deleteCONT')
    
    % Call delete cont subfunction
    DeleteCont(obj);
    
    % Stop continuation at end of current continuation loop and close
    % current figure
elseif strcmp(action,'closeMHB')
    
    % Call close mhb subfunction
    CloseMHB(obj);
    
elseif strcmp(action,'plotSOL')
    
    % Call plot solution subfunction
    PlotSolution()
    
elseif strcmp(action,'plotAMPS')
    
    % Call plot amplitudes subfunction
    PlotAmplitudes(obj)
    
elseif strcmp(action,'advancedCONTBOX')
    
    % Call advanced continuation subfunction
    AdvancedCont(obj)
    
elseif strcmp(action,'stepControl')
    
    ContSettings(obj);
    
    % Start continuation and assign any GUI values to the control structure
elseif strcmp(action,'Integrate')
    
    % Call integration and periodicity calculation subfunction
    Integrate_Per(obj)
    
    
end
end