path(path,genpath(cd)); close all;

disp('----------------------------------------------------------------')
disp('Harmonic Balance for NNM and Periodic Forced Response Calculation')
disp('V1.0 (August 2019)')
disp('Christopher Van Damme - [cvandamme@wisc.edu, cvandamme13@gmail.com]')
disp('Matt Allen - [msallen@engr.wisc.edu]')
disp('Ben Moldenhauer - [bmoldenhauer@wisc.edu]')
disp('----------------------------------------------------------------')