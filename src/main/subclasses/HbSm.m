classdef HbSm < HB & handle
    % This subclass of the harmonic balance code is used to represent
    % spring-mass systems with discrete nonlinear elements between 
    % the masses. The systems of equations should be as follows: 
    % 
    %          m*a + c*v +k*x + fnl(x,v) = fext
    % 
    % The user should supply a function handle that outputs
    % the nonlinear force as a [nDof x 1] vector that takes 
    % displacement (x) and velocity (v) at an instant in time. 
    
    properties
        fnl_x  % function handle that accepts displacement and outputs
               % nonlinear portion of internal force
        dfnldx % gradient of nonlinear internal force wrt x
        fnl_v  % function handle that accepts velocity and outputs
               % nonlinear portion of internal force
        dfnldv % gradient of nonlinear internal force wrt v
        fdOpts = struct('domain','complex','step',1e-12)
        modal = struct('w',[],'phi',[]);
        smPos = struct('x0',[],'y0',[],'conn',[]);
    end
    
    methods
        % --- Initialization of NLROM form of HB Continuation --- %
        function [obj] = HbSm(Name,savdeDir,M,C,K)
                        
            % Call Explicit Initialization
            obj = obj@HB(Name,savdeDir,M,C,K);
            
            % Compute mode shapes of physical system
            [lambda,psi] =  eig(K,M);
            
            % Mass Normalize Modes
            for i = 1:length(M)
                psi(:, i) = psi(:, i)/sqrt(psi(:, i)'*M*psi(:, i));
            end
            
            % Store results in structure in object
            [lambda, I] = sort(diag(lambda));
            obj.modal.phi= psi(:, I);
            obj.modal.w = sqrt(lambda);
        end
                
        % Internal (nonlinear) and external force in frequency domain
        function [b,varargout]  = Calc_b(obj,zt,wt)
            % Calculate Period
            Tt = 1/(wt/2/pi);

            % Grab number of DOF and time samples
            nT = obj.HBcontrol.nT;

            % External Forcing
            t = linspace(0,Tt,nT);

            % Generate FT and IFT Tranformation Matrices
            [IFT,FT] = obj.Calc_Gamma(wt);

            % Compute gamma for velocity
            [dIFTdw,~] = obj.Calc_dGammadw(wt);
            
            % Tranform to time domain
            if size(IFT,2) == size(zt,1)
                xdt = IFT*zt; vdt = dIFTdw*zt;
            elseif size(IFT,2) == size(zt,2)
                xdt = IFT*zt; vdt = dIFTdw*zt;
            else
                error('Incorrect size')
            end

            % Check if nonlinear force is function of displacement
            if nargout > 1
                [fIntNL,dfdx,dfdv] = obj.Calc_f(xdt,vdt);
            else
                [fIntNL] = obj.Calc_f(xdt,vdt);
            end
            
            % External harmonic force
            fExt = (obj.Fext*(cos(wt*t/obj.nu)))';

            % Reorder
            fExt = fExt(:);

            % Combine and Tranform
            b = FT*(fExt-fIntNL);

            % Ouput jacobian if desired
            if nargout > 1
                varargout{1} = FT*(dfdx*IFT+dfdv*dIFTdw);
            end
    
        end
        
        % Internal force in time domain
        function [fint,varargout] = Calc_f(obj,xtil,vtil)
            % Computes the internal (nonlinear) forces of the system. 
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            % Outputs : 
            %           fint = nonlinear internal force value [[nDOF*nSAMPLES , 1]
            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Take the time domain results 
            % Reshape x_til at the current solution point
            x_d = reshape(xtil,nT,[])'; v_d = reshape(vtil,nT,[])'; 
            
            % Call internal force function here
            fint = zeros(obj.nDOF,nT);
            
            % Loop through each time point
            for ii = 1:nT
                if ~isempty(obj.fnl_x)
                	fint(:,ii) = obj.fnl_x(x_d(:,ii));
                end
                if ~isempty(obj.fnl_v)
                    fint(:,ii) = fint(:,ii) + obj.fnl_v(v_d(:,ii));
                end
            end
            
            % Reshape Nonlinear Forcing
            fint = reshape(fint',[],1);
            
            % If additional output arguments are requested
            if nargout > 1
                varargout{1} = sparse(obj.nDOF*nT,obj.nDOF*nT);
                varargout{2} = sparse(obj.nDOF*nT,obj.nDOF*nT);
                if ~isempty(obj.fnl_x)
                    varargout{1} = obj.Calc_dfdx(xtil);
                end
                if ~isempty(obj.fnl_v)
                    varargout{2} = obj.Calc_dfdv(vtil);
                end
            end
                  
        end
        
        % Gradient of internal force in time domain wrt to x
        function [dfdx] = Calc_dfdx(obj,xtil)
            % Computation of the first derivative of the internal forces, with
            % respect to the displacements J = d(fint_nl(q))/d(q), of NLROMs
            %
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            %              xdt=[x1(t1),x1(t2)...,x1(nT),x2(t1),...].'
            % Outputs : 
            %           dfdx = Jacobian of nonlinear forces [nDOF*nSAMPLES, nDOF*nSAMPLES ]

            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Reshape xtil and vtil at the current solution point
            x_d = reshape(xtil,nT,[])';
            
            % Initalize Matrices
            rowGlobal = cell(nT,1);
            colGlobal = cell(nT,1);
            valGlobal = cell(nT,1);
            
            % Call internal force function here
            for ii = 1:nT
                if isempty(obj.fnl_x)
                    dfdx_i = zeros(obj.nDOF);
                else
                    if isempty(obj.dfnldx)
                        dfdx_i = obj.ComputeDerivative(obj.fnl_x,x_d(:,ii));
                    else
                        dfdx_i= obj.dfnldx(x_d(:,ii));
                    end
                end
                [rowLocal,colLocal,val] = find(dfdx_i);
                rowGlobal{ii} = (rowLocal-1)*nT + ii;
                colGlobal{ii} = (colLocal-1)*nT + ii;
                valGlobal{ii} = val;
            end
            
            % Combine all the time samples
            rowGlobal = vertcat(cell2mat(rowGlobal));
            colGlobal = vertcat(cell2mat(colGlobal));
            valGlobal = vertcat(cell2mat(valGlobal));
            
            % Create sparse tangent stiffness matrix across orbit
            dfdx = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                nT*obj.nDOF,nT*obj.nDOF);
        end
        
        % Gradient of internal force in time domain wrt to v
        function [dfdv] = Calc_dfdv(obj,vtil)
            % Computation of the first derivative of the internal forces, with
            % respect to the displacements J = d(fint_nl(q))/d(q), of NLROMs
            %
            % Inputs : 
            %           obj = HBnlrom object
            %           vdt = displacement values [nDOF*nSAMPLES , 1]
            %              xdt=[x1(t1),x1(t2)...,x1(nT),x2(t1),...].'
            % Outputs : 
            %           dfdv = Jacobian of nonlinear forces [nDOF*nSAMPLES, nDOF*nSAMPLES ]

           % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Reshape xtil and vtil at the current solution point
            v_d = reshape(vtil,nT,[])';
            
            % Initalize Matrices
            rowGlobal = cell(nT,1);
            colGlobal = cell(nT,1);
            valGlobal = cell(nT,1);
            
            % Call internal force function here
            for ii = 1:nT
                if isempty(obj.fnl_v)
                    dfdv_i = zeros(obj.nDOF);
                else
                    if isempty(obj.dfnldv)
                        dfdv_i = obj.ComputeDerivative(obj.fnl_v,v_d(:,ii));
                    else
                        dfdv_i = obj.dfnldv(v_d(:,ii));
                    end
                end
                [rowLocal,colLocal,val] = find(dfdv_i);
                rowGlobal{ii} = (rowLocal-1)*nT + ii;
                colGlobal{ii} = (colLocal-1)*nT + ii;
                valGlobal{ii} = val;
            end
            
            % Combine all the time samples
            rowGlobal = vertcat(cell2mat(rowGlobal));
            colGlobal = vertcat(cell2mat(colGlobal));
            valGlobal = vertcat(cell2mat(valGlobal));
            
            % Create sparse tangent stiffness matrix across orbit
            dfdv = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                nT*obj.nDOF,nT*obj.nDOF);
        end
        
        % Energy calculation
        function [energy] = Calc_Energy(obj,xt,vt)
            % This function computes the energy of a specific solution
            % 
            % Inputs: 
            %           obj = HBnlrom Class Object
            %           xt = displacement [nDOF x 1]
            %           vt = velocity [nDOF x 1]
            % Outputs:
            %           energy = energy at solutoin [1x1]
            %           
          
            % Compute Energy - this needs to be updated
            energy = (1/2)*xt.'*obj.K*xt + 0.5*vt'*obj.M*vt;
            
        end
        
        % Perodicity Check
        function Calc_Periodicity(varargin)
             error('This method is not currently availble for spring-mass system')
        end

        % Set the nonlinear force for displacement
        function Set_NonlinearForce_Displacement(obj,fnl,varargin)
            
            obj.fnl_x = fnl;
            
            if nargin > 2 
                obj.dfnldx = varargin{1};
            end
        end
        
        % Set the nonlinear force for velocity
        function Set_NonlinearForce_Velocity(obj,fnl,varargin)
            
            obj.fnl_v = fnl;
            
            if nargin > 2 
                obj.dfnldv = varargin{1};
            end
        end
        
        % Complex step finite differnce estimation
        function dfds = ComputeDerivative(obj,funHandle,s0)
            % This function computes the derivative of a function using the
            % complex step finite difference method.
            
            % Initialize matrix
            dfds = zeros(length(s0));
            
            % Loop through each free variable
            for ii = 1:length(s0)
                
                % Perturb the ith dof in the complex plane
                si = s0; si(ii) = s0(ii) + 1i*obj.fdOpts.step;
                
                % Take the complex part and divide by step size
                try
                    dfds(:,ii) = imag(funHandle(si))/obj.fdOpts.step;
                catch
                    % If complex doesn't work (i.e. for interpolation)
                    si(ii) = s0(ii) + obj.fdOpts.step;
                    dfds(:,ii) = (funHandle(si)-funHandle(s0))/obj.fdOpts.step;
                end
            end
        end
        
        % Plot functions
        function [sp_axes,sp_data] = InitializePlot(obj,sp_axes,sp_data)
            
            % Get number of modes
            for ii = 1:obj.romObj.m
                lString{ii} = ['Mode ',...
                    num2str(obj.romObj.modeIndices(m))]; %#ok<AGROW>
            end
            
            % Initialize plot in upper left hand corner
            sp_data = plot(nan(1,obj.nDOF+1),nan(obj.nDOF,obj.nDOF+1),...
                'LineStyle','-','LineWidth',2,'Marker','.','MarkerSize',18);
            legend(lString); 
        end
        function PlotPhysical(obj,sp_axes,sp_data)
            for k = 1:numel(sp_data{2})
                set(sp_data{2}(k),'XData',obj.x(:,obj.solpt),...
                                  'YData',zeros(obj.nDOF,1))
            end
            
        end
        function PlotModal(obj,sp_axes,sp_data)
            
            
            % Check whether frequency is in rad/s or in hz
            if strcmp(obj.HBcontrol.freq,'Hz')
                % Divide by 2 pi to convert frequency to hz
                w_plot = obj.w(1:obj.solpt)/(2*pi);
            else
                % If frequency is already in hz than just get frequency values
                w_plot = obj.w(1:obj.solpt);
            end
            
            % Project onto modal domain
            q = pinv(obj.modal.phi)*obj.x(:,1:obj.solpt);
            for k = 1:numel(sp_data{1})
                set(sp_data{1}(k),'XData',w_plot,'YData',q(k,:))
            end
        end
    end
    
end