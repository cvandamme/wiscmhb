classdef HBfem < HB & handle
    % The HBfem handle subclass is used to interface with OsFern matlab based
    % finite element code. The MHB procedure is applied directly to the
    % full FE model. 
    
    % Class properties
    properties
        mf        % osfern class object
        nlStatic  % nonlinear static step object
        modal     % modal object
    end
    
    methods
        
        % --- Initialization of NLROM form of HB Continuation --- %
        function [obj] = HBfem(Name,SaveDir,M,C,K,matFemObj,modalObj,staticObj)
            
            % Call  Initialization
            obj = obj@HB(Name,SaveDir,M,C,K);
            
            % Store the matfem object
            obj.mf = matFemObj;
            
            % Store Static Object
            obj.nlStatic = staticObj;
            
            % Store Modal Object
            obj.modal = modalObj;
        end
        
        % Iternal (nonlinear) and external forcing in frequency domain
        function [b,varargout]  = Calc_b(obj,zt,wt)
            
            % Calculate Period
            Tt = 1/(wt/2/pi);
            
            % Grab number of DOF and time samples
            nT = obj.HBcontrol.nT;
            
            % External Forcing
            t = (0:nT-1)*((2*pi/wt))/nT;
            
            % Generate FT and IFT Tranformation Matrices
            [IFT,FT] = obj.Calc_Gamma(wt);
            
            % Tranform to Physical Domain
            if size(IFT,2) == size(zt,1)
                xdt = IFT*zt;
            elseif size(IFT,2) == size(zt,2)
                xdt = IFT*zt';
            else
                error('Incorrect size')
            end
            
            % Call Internal Nonlinear Force Functions
            if nargout > 1
                [fIntNL,dfdx] = obj.Calc_f(xdt);
            else
                [fIntNL] = obj.Calc_f(xdt);
            end
            fintEXT = (obj.Fext*(cos(wt*t/obj.nu)))';
            
            % Reorder
            fintEXT = fintEXT(:);
            
            % Combine and Tranform
            b = FT*(fintEXT-fIntNL);
            
            % Ouput jacobian if desired
            if nargout > 1
                varargout{1} = FT*dfdx*IFT;
            end
        end
        
        % Compute the nonlinear internal force across the orbit
        function [fint,varargout] = Calc_f(obj,xdt)
            % Computes the internal (nonlinear) forces of the system.
            % Inputs :
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            % Outputs :
            %           fint = nonlinear internal force value [[nDOF*nSAMPLES , 1]
            
            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Check if jacobian is desired
            if nargout > 1
                compKt = true;
                rowGlobal = cell(nT,1);
                colGlobal = cell(nT,1);
                valGlobal = cell(nT,1);
            else
                compKt = false;
            end
            
            % Take the time domain results
            freeDof = obj.mf.BuildConstraintVec();
            
            % Obtain vector of free DOF Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])';
            
            % Initialize
            fintNL = zeros(nT,obj.nDOF);
            nDofFem = obj.mf.nDof;
            
            % Loop Through and Generate Internal Force
            if obj.parallelOpts.logic
                
                % Initialize some variables
                sharedObj = parallel.pool.Constant(obj.nlStatic());
                
                % Loop Through each time point and generate tangent stiffness
                parfor (ii=1:nT,obj.parallelOpts.numWorkers)
                    xTemp = zeros(nDofFem,1); xTemp(freeDof) = x_d(:,ii);
                    [Kt,~,fintNLtemp] = sharedObj.Value.RunEnforcedDisp(xTemp);
                    [rowLocal,colLocal,val] = find(Kt);
                    fintNL(ii,:) = fintNLtemp';
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
                
            else
                
                for ii = 1:nT
                    xTemp = zeros(nDofFem,1);
                    xTemp(freeDof) = x_d(:,ii);
%                     [fIntNltemp,Kt,~] = obj.mf.GetFintNl(xTemp);
                    [Kt,~,fIntNltemp] = obj.nlStatic.RunEnforcedDisp(xTemp);
                    fintNL(ii,:) = fIntNltemp';
                    [rowLocal,colLocal,val] = find(Kt);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            end
            
            % If additional argument is passed provide jacobian
            if compKt
                rowGlobal = vertcat(cell2mat(rowGlobal));
                colGlobal = vertcat(cell2mat(colGlobal));
                valGlobal = vertcat(cell2mat(valGlobal));
                varargout{1} = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                    nT*sum(freeDof),nT*sum(freeDof));
            end
            
            % Reshape Nonlinear Forcing
            fint = fintNL(:);
        end
        
        % Compute the nonlinear force gradient across the orbit
        function [dfdx] = Calc_dfdx(obj,xdt)
            % Computation of the first derivative of the internal forces, with
            % respect to the displacements J = d(fint_nl(q))/d(q), of NLROMs
            %
            % Inputs :
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            %              xdt=[x1(t1),x1(t2)...,x1(nT),x2(t1),...].'
            % Outputs :
            %           dfdx = Jacobian of nonlinear forces [nDOF*nSAMPLES, nDOF*nSAMPLES ]
            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])';
            
            % Obtain vector of free DOF
            freeDof = obj.mf.BuildConstraintVec();
            nDofFem = obj.mf.nDof;
            
            % initialize some variables
            rowGlobal = cell(nT,1);colGlobal = cell(nT,1);valGlobal = cell(nT,1);
            
            if obj.parallelOpts.logic
                % initialize some variables
                sharedObj = parallel.pool.Constant(obj.nlStatic());
                % Loop Through each time point and generate tangent stiffness
                parfor (ii=1:nT,obj.parallelOpts.numWorkers)
                    xTemp = zeros(nDofFem,1); xTemp(freeDof) = x_d(:,ii);
                    [Kt,Kn,~] = sharedObj.Value.RunEnforcedDisp(xTemp);
                    [rowLocal,colLocal,val] = find(Kt);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                    if kTkepp
                        kThistory{ii} = Kt;
                        kNhistory{ii} = Kn;
                    end
                end
            else
                for ii=1:nT
                    xTemp = zeros(nDofFem,1); xTemp(freeDof) = x_d(:,ii);
                    [Kt,Kn,~] = obj.nlStatic.RunEnforcedDisp(xTemp);
                    [rowLocal,colLocal,val] = find(Kt);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            end
            % Combine all the time samples
            rowGlobal = vertcat(cell2mat(rowGlobal));
            colGlobal = vertcat(cell2mat(colGlobal));
            valGlobal = vertcat(cell2mat(valGlobal));
            
            % Create sparse tangent stiffness matrix across orbit
            dfdx = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                nT*sum(freeDof),nT*sum(freeDof));

        end
        
        % Compute energy at specific time point
        function [energy] = Calc_Energy(obj,xt,vt)
            % This function computes the energy of a specific solution
            %
            % Inputs:
            %           obj = HBnlrom Class Object
            %           xt = displacement [nDOF x 1]
            % Outputs:
            %           energy = energy at solutoin [1x1]
            %
            
            % Obtain vector of free DOF
            freeDof = obj.mf.BuildConstraintVec();
            
            xTemp = zeros(obj.mf.nDof,1);
            xTemp(freeDof) = xt;
            
            % Compute the internal force stiffness matrix
            [~,Kn,~] = obj.nlStatic.RunEnforcedDisp(xTemp);
            
            % Compute Energy
            energy = (1/2)*xt'*(obj.K+Kn)*xt + (1/2)*vt'*obj.M*vt;
            
        end
        
        % Integrate the system and compute periodicity of solution
        function [obj] = Calc_Periodicity(obj,nSteps,varargin)
            % This function integrates the previous solution point, calculates the
            % periodicity, and prints the output to the command window.
            %
            % Inputs :
            %           obj
            %           nSteps = number of integration points
            %           vaarargin{1} = solution point of interest
            % Outputs :
            %           obj
            %
            
            % Check additional arguments
            if nargin > 2
                intSol = varargin{1};
                % Print message to command window
                fprintf('Beginning integration at provided solution point \n')
            else
                intSol = obj.solpt-1;
                % Print message to command window
                fprintf('Beginning integration of previous solution point \n')
            end
            
            % Grab FE properties
            mfObj = obj.mf;
            
            % Get FE Model properties
            freeDof = mfObj.BuildConstraintVec(); %#ok<*UNRCH>
            xSol = zeros(mfObj.nDof,1);
            
            % Get displacement vector and frequency for previous solution
            xSol(freeDof) =  obj.x(:,intSol);
            fSol = obj.w(intSol)/2/pi;
            
            % Set integration parameters
            T = 1/fSol; dt = T/nSteps; name = 'intMhbSol';
            
            % Create dynamic object
            dynamic = Dynamic(name, mfObj, xSol);

            % dynamic.SetPlotOpts(true,5);
            
            % Integrate solution
            dynamic.Solve(T, dt);
            
            % Calculate Periodicity
            xPer = norm((dynamic.initialDisplacement - dynamic.finalDisplacement))/...
                norm(dynamic.initialDisplacement);
            
            % Plot some informatoin
            figure;
            subplot(1,3,1); mfObj.Plot(dynamic.initialDisplacement);
            title('Initial Displacement');
            subplot(1,3,2); mfObj.Plot(dynamic.finalDisplacement);
            title('Final Displacement');
            subplot(1,3,3); mfObj.Plot(dynamic.initialDisplacement-dynamic.finalDisplacement);
            title(['Residaul, \epsilon = ',num2str(xPer)]);
            
            % Print periodicity results to command window
            fprintf(['Periodicity =', num2str(xPer), '\n'])
            
        end
        
        % -- Plotting functions
        function [sp_axes,sp_data] = InitializePlot(obj,sp_axes,sp_data)
            
            % Get number of modes
            nModal = length(obj.modal.fn);
            for ii = 1:nModal
                lString{ii} = ['Mode ', num2str(ii)]; %#ok<AGROW>
            end

            % Initialize plot in upper left hand corner
            sp_data = plot(nan(1,nModal+1),nan(nModal,nModal+1),'LineStyle','-',...
                'LineWidth',2,'Marker','.','MarkerSize',18); 
            legend(lString); 
            
        end
        function PlotPhysical(obj,sp_axes,sp_data)

            % Update for full fe model
            xFullFem = zeros(obj.mf.nDof,1);
            xFullFem(obj.mf.freeDof) = obj.x(:,obj.solpt);
            
            % Plot using FERN Plotting Tool
            cla(sp_axes{2});
            obj.mf.Plot(xFullFem,'norm',sp_axes{2});
        end
        function PlotModal(obj,sp_axes,sp_data)
            % Find Constraints
            freeDof = obj.mf.freeDof();
            
            % This should be put somewhere else
            phiFree = obj.modal.phi(freeDof,:);
            
            % Project the fem down to modal using phi'Mx
            q = real((phiFree'*obj.M)*obj.x(:,1:obj.solpt));
            
            % Check whether frequency is in rad/s or in hz
            if strcmp(obj.HBcontrol.freq,'Hz')
                % Divide by 2 pi to convert frequency to hz
                w_plot = obj.w(1:obj.solpt)/(2*pi);
            else
                % If frequency is already in hz than just get frequency values
                w_plot = obj.w(1:obj.solpt);
            end
            
            % Add to plot
            q(isnan(q)) = 0;
            for k = 1:size(q,1)
                set(sp_data{1}(k), 'XData',w_plot, 'YData',q(k,:))
            end
        end
        
    end
    
end