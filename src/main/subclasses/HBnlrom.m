classdef HBnlrom < HB & handle
    % This subclass of Hb is used to compute periodic orbits of
    % reduced order models. In this case the rom is a geometrically
    % nonlinear rom with the nonlinearities represented as cubic and
    % quadractic nonlinear stiffness terms. 
    % 
    % Note : it is recommended that the HbRom class is used instead of
    % this class because it has more features and support.
    properties
        N1    % quadtric stiffness (nash form)
        N2    % cubic stiffness (nash form)
        Nlist % nashform nonlinear stiffness index
        Knl   % quadractic and cubic nonlinear stiffness value matrix
        tlist % quadractic and cubic nonlinear stiffness index matrix
        FEM = struct('phi',[],'dof2plot',[])  % struct of FEM properties
    end
    
    methods
        % --- Initialization of NLROM form of HB Continuation --- %
        function [obj] = HBnlrom(Name,SAVEdir,M,C,K,N1,N2,Nlist,FEdata)
            
            warning('It is recommended to use the HbRom class instead')
            
            % Call Explicit Initialization
            obj = obj@HB(Name,SAVEdir,M,C,K);
            
            % Store NLROM Data
            obj.N1 = N1; obj.N2 = N2;  obj.Nlist = Nlist;

            % Store FEM data
            if ~isempty(FEdata)
                if isfield(FEdata,'phi')
                    obj.FEM.phi = FEdata.phi;
                end
                if isfield(FEdata,'nodes')
                    obj.FEM.nodes = FEdata.nodes;
                end
                if isfield(FEdata,'elements')
                    obj.FEM.elements = FEdata.elements;
                end
                if isfield(FEdata,'nset')
                    obj.FEM.nset = FEdata.nset;
                end
                if isfield(FEdata,'dof2plot')
                    obj.FEM.dof2plot = FEdata.dof2plot;
                end
            else
                warning('No FE Data Supplied, Some Plotting Features will Not be Supported')
            end
            
        end
                
        % Iternal (nonlinear) and external forcing in frequency domain
        function [b,varargout]  = Calc_b(obj,zt,wt)
            % Calculate Period
            Tt = 1/(wt/2/pi);

            % Grab number of DOF and time samples
            nT = obj.HBcontrol.nT;

            % External Forcing
            t = linspace(0,Tt,nT);

            % Generate FT and IFT Tranformation Matrices
            [IFT,FT] = obj.Calc_Gamma(wt);

            % Tranform to Physical Domain
            if size(IFT,2) == size(zt,1)
                xdt = IFT*zt; 
            elseif size(IFT,2) == size(zt,2)
                xdt = IFT*zt';
            else
                error('Incorrect size')
            end

            % Call Internal Nonlinear Force Functions
            if nargout > 1
                try
                    [fIntNL,dfdx] = obj.Calc_f(xdt);
                catch
                    [fIntNL] = obj.Calc_f(xdt);
                    [dfdx] = obj.Calc_dfdx(xdt);
                end
            else
                [fIntNL] = obj.Calc_f(xdt);
            end
            fintEXT = (obj.Fext*(cos(wt*t/obj.nu)))';

            % Reorder
            fintEXT = fintEXT(:);

            % Combine and Tranform
            b = FT*(fintEXT-fIntNL);

            % Ouput jacobian if desired
            if nargout > 1
                varargout{1} = FT*dfdx*IFT;
            end
    
        end
        
        % Compute the nonlinear internal force across the orbit
        function [fint,varargout] = Calc_f(obj,xdt)
            % Computes the internal (nonlinear) forces of the system. 
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            % Outputs : 
            %           fint = nonlinear internal force value [[nDOF*nSAMPLES , 1]
            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])'; 
           
            % Generate Quadractic Terms [nDOF x nT]
            xd_xd = x_d(obj.Nlist(:,1),:).*x_d(obj.Nlist(:,2),:);

            % Initialize
            fintNL = zeros(nT,obj.nDOF);
            N1_x = zeros(obj.nDOF,obj.nDOF,nT);
            N2_x = zeros(obj.nDOF,obj.nDOF,nT);
                       
            % Loop Through and Generate Internal Force
            for ii = 1:nT
                for jj=1:obj.nDOF
                    N1_x(:,jj,ii) = obj.N1(:,:,jj)*x_d(:,ii);
                    N2_x(:,jj,ii) = obj.N2(:,:,jj)*xd_xd(:,ii);
                end
                fintNL(ii,:) = ((1/2)*N1_x(:,:,ii)*x_d(:,ii) + (1/3)*N2_x(:,:,ii)*x_d(:,ii))';
            end
            % Reshape Nonlinear Forcing
            fint = fintNL(:);
            
            % Compute gradient if requested
            if nargout > 1
                varargout{1} = obj.Calc_dfdx(xdt);
            end
                  
        end
        
        % Compute the nonlinear force gradient across the orbit
        function [dfdx] = Calc_dfdx(obj,xdt)
            % Computation of the first derivative of the internal forces, with
            % respect to the displacements J = d(fint_nl(q))/d(q), of NLROMs
            %
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            %              xdt=[x1(t1),x1(t2)...,x1(nT),x2(t1),...].'
            % Outputs : 
            %           dfdx = Jacobian of nonlinear forces [nDOF*nSAMPLES, nDOF*nSAMPLES ]

            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])'; 

            % Multiply to get Quadratic Combinations
            xd_xd = x_d(obj.Nlist(:,1),:).*x_d(obj.Nlist(:,2),:);

            % Initalize Matrices
            dfdxCell = cell(obj.nDOF);

            % Loop Through and Generate
            for jj=1:obj.nDOF
                N1_x = obj.N1(:,:,jj)*x_d;
                N2_x = obj.N2(:,:,jj)*xd_xd;
                dfdxtemp = - N1_x - N2_x;
                for ii = 1:obj.nDOF
                    dfdxCell{ii,jj} = diag(dfdxtemp(ii,:));
                end
            end
				
            % Convert cell of individual matrices to global matrix
            dfdx = cell2mat(dfdxCell);			
        end
        
        % Compute energy at specific time point
        function [energy] = Calc_Energy(obj,xt,vt)
            % This function computes the energy of a specific solution
            % 
            % Inputs: 
            %           obj = HBnlrom Class Object
            %           xt = displacement [nDOF x 1]
            % Outputs:
            %           energy = energy at solutoin [1x1]
            %           
           
            % Initialize Variables
            N = length(xt);            
            N1_x=zeros(N,N);
            N2_x=zeros(N,N);
            
            % Compute Quadratic Combinations 
            xt_xt = xt(obj.Nlist(:,1)).*xt(obj.Nlist(:,2));
            
            % Compute Tangent Stiffness Matrices 
            for jj=1:N
                N1_x(:,jj)= obj.N1(:,:,jj) * xt;    % Quadractic 
                N2_x(:,jj)= obj.N2(:,:,jj) * xt_xt; % Cubic 
            end
            
            % Compute Energy
            energy = (1/2)*xt.'*obj.K*xt + ...
                     (1/6)*xt.'*N1_x*xt + (1/12)*xt.'*N2_x*xt +...
                     0.5*vt'*obj.M*vt;
            
        end
        
        % Functions to change parameters without creating new object
        function [obj] = SetKnl(obj,Knl,tlist)
            % This function resets (indirectly) the Nash form of the
            % noninear stiffness terms by first setting the Knl values
            
            obj.Knl = Knl;
            obj.tlist = tlist;
            [obj.N1,obj.N2,obj.Nlist] = Knl2Nash(Knl,tlist);
        end
        function [obj] = SetNash(obj,N1,N2,Nlist)
            % This function directly resets the nash form the nonlinear
            % stiffnss values by supplying them.
            
            % Store NLROM Data
            obj.N1 = N1;
            obj.N2 = N2;
            obj.Nlist = Nlist;
        end
        function [obj] = Update(obj)
            [obj.N1,obj.N2,obj.Nlist] = Knl2Nash(obj.Knl,obj.tlist);
        end

        % Integrate the system (in this case goes back to full FEM)
        function obj = Calc_Periodicity(obj,nSteps,varargin)
            error('Not implemented for the nlrom subclass yet')
        end
        
        % Plot functions
        function [sp_axes,sp_data] = InitializePlot(obj,sp_axes,sp_data)
            
            % Get number of modes
            for ii = 1:obj.nDOF
                lString{ii} = ['Mode ',...
                    num2str(ii)]; %#ok<AGROW>
            end
            
            % Initialize plot in upper left hand corner
            sp_data = plot(nan(1,obj.nDOF+1),nan(obj.nDOF,obj.nDOF+1),...
                'LineStyle','-','LineWidth',2,'Marker','.','MarkerSize',18);     
        end
        function PlotPhysical(obj,sp_axes,sp_data)
            
            % Check whether frequency is in rad/s or in hz
            if strcmp(obj.HBcontrol.freq,'Hz')
                % Divide by 2 pi to convert frequency to hz
                w_plot = obj.w(1:obj.solpt)/(2*pi);
            else
                % If frequency is already in hz than just get frequency values
                w_plot = obj.w(1:obj.solpt);
            end
            
            % Check if fem properties are provided
            if ~isempty(obj.FEM)
                if ~isempty(obj.FEM.dof2plot)
                    dof2plot = obj.FEM.phi(obj.FEM.dof2plot,:)*obj.x(:,1:obj.solpt);
                else
                    obj.x(obj.HBcontrol.mode,1:obj.solpt);
                end
                for k = 1:numel(sp_data{2})
                    set(sp_data{2}(k), 'XData',w_plot, 'YData',dof2plot(k,1:obj.solpt))
                end
            else
                dof2plot = obj.z(1,1:obj.solpt);
                set(sp_data{2}, 'XData',w_plot, 'YData',dof2plot)
            end
        end
        function PlotModal(obj,sp_axes,sp_data)
            
            % Check whether frequency is in rad/s or in hz
            if strcmp(obj.HBcontrol.freq,'Hz')
                % Divide by 2 pi to convert frequency to hz
                w_plot = obj.w(1:obj.solpt)/(2*pi);
            else
                % If frequency is already in hz than just get frequency values
                w_plot = obj.w(1:obj.solpt);
            end
            
            for k = 1:numel(sp_data{1})
                set(sp_data{1}(k), 'XData',w_plot,...
                    'YData',obj.x(k,1:obj.solpt))
            end
        end
        
    end
    
end