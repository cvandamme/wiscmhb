classdef HbRom < HB & handle
    % This subclass of Hb is used to compute periodic orbits of
    % reduced order models. THis will work with any of rom subclasses
    % within the rom object superclass from the osfern finite element code. 
    % Rom : Reduced Order Model Super Class with following subclasses
    %   -IcRom : Implicit Condensation
    %   -EdRom : Enforced Displacement
    %   -MdRom : Modal derivatives
    %   -SmRom : Shi and Mei          
    properties
        romObj
    end
    
    methods
        % --- Initialization of NLROM form of HB Continuation --- %
        function [obj] = HbRom(name,saveDir,romObj)
            
            % Call Explicit Initialization
            obj = obj@HB(name,saveDir,...
                romObj.Mbb,zeros(size(romObj.Mbb)),romObj.Kbb);
            obj.romObj = romObj;
        end
                
        % Iternal (nonlinear) and external forcing in frequency domain
        function [b,varargout]  = Calc_b(obj,zt,wt)

            % Grab number of DOF and time samples
            nT = obj.HBcontrol.nT;
            
            % Time vector
            t = (0:nT-1)*((2*pi/wt))/nT;

            % Generate FT and IFT Tranformation Matrices
            [IFT,FT] = obj.Calc_Gamma(wt);

            % Tranform to Physical Domain
            if size(IFT,2) == size(zt,1)
                xdt = IFT*zt; 
            elseif size(IFT,2) == size(zt,2)
                xdt = IFT*zt';
            else
                error('Incorrect size')
            end

            % Call Internal Nonlinear Force Functions
            if nargout > 1
                [fIntNL,dfdx] = obj.Calc_f(xdt);
            else
                [fIntNL] = obj.Calc_f(xdt);
            end
            fintEXT = (obj.Fext*(cos(wt*t/obj.nu)))';

            % Reorder
            fintEXT = fintEXT(:);

            % Combine and transform
            b = FT*(fintEXT-fIntNL);

            % Ouput jacobian if desired
            if nargout > 1
                varargout{1} = FT*dfdx*IFT;
            end
    
        end
        
        % Compute the nonlinear internal force across the orbit
        function [f,dfdx] = Calc_f(obj,xdt)
            % Computes the internal (nonlinear) forces of the system. 
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            % Outputs : 
            %           fint = nonlinear internal force value [[nDOF*nSAMPLES , 1]
            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Take the time domain results 
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])'; 
            
            % Initialize internal force matrix
            fIntNl = zeros(obj.nDOF,nT);
%             Knl = cell(nT,1);
            rowGlobal = cell(nT,1);
            colGlobal = cell(nT,1);
            valGlobal = cell(nT,1);
            % Check if parallel option is supplied
            if obj.parallelOpts.logic
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.romObj());
                
                % Loop Through and Generate Internal Force
                parfor ii = 1:nT
                    [fIntNl(:,ii),Knl] = sharedObj.Get_NlForce(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            else
                % Loop Through and Generate Internal Force
                for ii = 1:nT
                    [fIntNl(:,ii),Knl] = obj.romObj.Get_NlForce(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            end
            
            % Reshape Nonlinear Forcing
            f = reshape(fIntNl',[],1); %(:);  
            
            % If nonlinear stiffness is requested
            if nargout > 1
                % Combine all the time samples
                rowGlobal = vertcat(cell2mat(rowGlobal));
                colGlobal = vertcat(cell2mat(colGlobal));
                valGlobal = vertcat(cell2mat(valGlobal));

                % Create sparse tangent stiffness matrix across orbit
                dfdx = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                       nT*obj.nDOF,nT*obj.nDOF);   
                varargout{1} = dfdx; 
            end
        end
        
        % Compute the nonlinear force gradient across the orbit
        function [dfdx] = Calc_dfdx(obj,xdt)
            % Computation of the first derivative of the internal forces, with
            % respect to the displacements J = d(fint_nl(q))/d(q), of NLROMs
            %
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            %              xdt=[x1(t1),x1(t2)...,x1(nT),x2(t1),...].'
            % Outputs : 
            %           dfdx = Jacobian of nonlinear forces [nDOF*nSAMPLES, nDOF*nSAMPLES ]

            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Take the time domain results 
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])'; 
            
            % Initialize internal force matrix
%             Knl = cell(nT,1);
            
            % Initalize Matrices
            rowGlobal = cell(nT,1);
            colGlobal = cell(nT,1);
            valGlobal = cell(nT,1);
                
            % Decide if doing serial or parallel
            if obj.parallelOpts.logic
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.romObj());
                
                % Loop Through and Generate Internal Force
                parfor ii = 1:nT
                    Knl = sharedObj.Get_NlStiffness(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            else
                % Loop Through and Generate Internal Force
                for ii = 1:nT
                    Knl = obj.romObj.Get_NlStiffness(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            end
            
            % Combine all the time samples
            rowGlobal = vertcat(cell2mat(rowGlobal));
            colGlobal = vertcat(cell2mat(colGlobal));
            valGlobal = vertcat(cell2mat(valGlobal));
            
            % Create sparse tangent stiffness matrix across orbit
            dfdx = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                   nT*obj.nDOF,nT*obj.nDOF);             
            
        end
        
        % Compute energy at specific time point
        function [energy] = Calc_Energy(obj,xt,vt)
            % This function computes the energy of a specific solution
            % 
            % Inputs: 
            %           obj = HBnlrom Class Object
            %           xt = displacement [nDOF x 1]
            % Outputs:
            %           energy = energy at solutoin [1x1]
            %           
          
            % Initialize Variables
            N = length(xt);            
            N1_x=zeros(N,N);
            N2_x=zeros(N,N);
            
            % Compute Quadratic Combinations 
            xt_xt = xt(obj.romObj.NashForm.Nlist(:,1)).*xt(obj.romObj.NashForm.Nlist(:,2));
            
            % Compute Tangent Stiffness Matrices 
            for jj=1:N
                N1_x(:,jj)= obj.romObj.NashForm.N1(:,:,jj) * xt;    % Quadractic 
                N2_x(:,jj)= obj.romObj.NashForm.N2(:,:,jj) * xt_xt; % Cubic 
            end
            
            % Compute Energy
            energy = (1/2)*xt.'*obj.K*xt + ...
                     (1/6)*xt.'*N1_x*xt + (1/12)*xt.'*N2_x*xt +...
                     0.5*vt'*obj.M*vt;            
            
%             % Compute nonlinear internal force
%             Knl = obj.romObj.Get_NlStiffness(xt);
%             
%             % Compute Energy
%             energy = (1/2)*xt.'*obj.K*xt + ...
%                      (1/6)*xt.'*Knl*xt +...
%                      0.5*vt'*obj.M*vt;
            
        end
        
        % Integrate the system (in this case goes back to full FEM)
        function [obj] = Calc_Periodicity(obj,nSteps,varargin)
            % This function integrates the previous solution point, calculates the
            % periodicity, and prints the output to the command window.
            %
            % Inputs :
            %           obj
            %           nSteps = number of integration points
            %           vaarargin{1} = solution point of interest
            % Outputs :
            %           obj
            %
            
            % Check additional arguments
            if nargin > 2
                intSol = varargin{1};
                % Print message to command window
                fprintf('Beginning integration at provided solution point \n')
            else
                intSol = obj.solpt-1;
                % Print message to command window
                fprintf('Beginning integration of previous solution point \n')
            end
            
            % Grab osfern object
            feObj = obj.romObj.osFern;
            
            % Grab solution
            qSol = obj.x(:,intSol);
            xSol = obj.romObj.basis*qSol;
            fSol = obj.w(intSol)/2/pi;
            
            % Set integration parameters
            T = 1/fSol; dt = T/nSteps; name = 'intMhbSol';
            
            % Create dynamic object
            dynamic = Dynamic(name, feObj, xSol);
            
            % dynamic.SetPlotOpts(true,5);
            
            % Integrate solution
            dynamic.Solve(T, dt);
            
            % Calculate Periodicity
            xPer = norm((dynamic.initialDisplacement - dynamic.finalDisplacement))/...
                norm(dynamic.initialDisplacement);
            
            % Plot some informatoin
            figure;
            subplot(1,3,1); feObj.Plot(dynamic.initialDisplacement);
            title('Initial Displacement');
            subplot(1,3,2); feObj.Plot(dynamic.finalDisplacement);
            title('Final Displacement');
            subplot(1,3,3); feObj.Plot(dynamic.initialDisplacement-dynamic.finalDisplacement);
            title(['Residaul, \epsilon = ',num2str(xPer)]);
            
            % Print periodicity results to command window
            fprintf(['Periodicity =', num2str(xPer), '\n'])
            
        end

        % Functions to change parameters without creating new object
        function [obj] = SetKnl(obj,Knl,tlist)
            % This function resets (indirectly) the Nash form of the
            % noninear stiffness terms by first setting the Knl values
            obj.romObj.Knl = Knl;
            obj.romObj.tlist = tlist;
            [N1,N2,Nlist] = Knl2Nash(Knl,tlist);
            obj.romObj.NashForm.N1 = N1;
            obj.romObj.NashForm.N2 = N2;
            obj.romObj.NashForm.Nlist = Nlist;
        end
        function [obj] = SetNash(obj,N1,N2,Nlist)
            % This function directly resets the nash form the nonlinear
            % stiffnss values by supplying them.
            
            % Store NLROM Data
            obj.romObj.NashForm.N1 = N1;
            obj.romObj.NashForm.N2 = N2;
            obj.romObj.NashForm.Nlist = Nlist;
        end
        function [obj] = Update(obj)
            [N1,N2,Nlist] = Knl2Nash(obj.romObj.Knl,obj.romObj.tlist);
            % Store NLROM Data
            obj.romObj.NashForm.N1 = N1;
            obj.romObj.NashForm.N2 = N2;
            obj.romObj.NashForm.Nlist = Nlist;
        end

        % Plot functions
        function [sp_axes,sp_data] = InitializePlot(obj,sp_axes,sp_data)
            
            % Get number of modes
            for ii = 1:obj.romObj.m
                lString{ii} = ['Mode ',...
                    num2str(obj.romObj.modeIndices(m))]; %#ok<AGROW>
            end
            
            % Initialize plot in upper left hand corner
            sp_data = plot(nan(1,obj.nDOF+1),nan(obj.nDOF,obj.nDOF+1),...
                'LineStyle','-','LineWidth',2,'Marker','.','MarkerSize',18);
            legend(lString); 
        end
        function PlotPhysical(obj,sp_axes,sp_data)

            % Update for full fe model
            xFullFem = obj.romObj.basis*obj.x(:,obj.solpt);

            % Plot using FERN Plotting Tool
            cla(sp_axes{2}); 
            obj.romObj.osFern.Plot(xFullFem,'norm',sp_axes{2});
        end
        function PlotModal(obj,sp_axes,sp_data)
          % Check whether frequency is in rad/s or in hz
            if strcmp(obj.HBcontrol.freq,'Hz')
                % Divide by 2 pi to convert frequency to hz
                w_plot = obj.w(1:obj.solpt)/(2*pi);
            else
                % If frequency is already in hz than just get frequency values
                w_plot = obj.w(1:obj.solpt);
            end
            
            for k = 1:numel(sp_data{1})
                set(sp_data{1}(k), 'XData',w_plot,...
                    'YData',obj.x(k,1:obj.solpt))
            end
        end
    end
    
end