classdef HbHotRom < HbRom & handle
    % This subclass of Hb is used to compute periodic orbits of 
    % reduced orbit models. This is the HbHotRom which     
    properties
%         romObj
    end
    
    methods
        % --- Initialization of NLROM form of HB Continuation --- %
        function [obj] = HbHotRom(name,saveDir,romObj)
            
            % Zero out diagonal terms
%             Kbb = diag(diag(romObj.Kbb));
            
            % Call Explicit Initialization
            obj = obj@HbRom(name,saveDir,romObj);
            
            obj.romObj.initialDisplacement = obj.romObj.thermal.finalDisplacement;

            % Assemble thermal force vector if it is missing
            if isempty(obj.romObj.fThermal)
                % Compute internal thermal load at the nodes steps(1);
                [Fn,~,~] = obj.romObj.thermal.AssembleThermalField(0);
                obj.romObj.fThermal = obj.romObj.basis'*Fn;
            end
        end
        
        % --- Abstract Methods that Must be Implemented --- %
        
        % This method computes the internal (nonlinear) and external
        % harmonics of the system
        function [b,varargout]  = Calc_b(obj,zt,wt)

            % Grab number of DOF and time samples
            nT = obj.HBcontrol.nT;
            
            % Time vector
            t = (0:nT-1)*((2*pi/wt))/nT;

            % Generate FT and IFT Tranformation Matrices
            [IFT,FT] = obj.Calc_Gamma(wt);

            % Tranform to Physical Domain
            if size(IFT,2) == size(zt,1)
                xdt = IFT*zt; 
            elseif size(IFT,2) == size(zt,2)
                xdt = IFT*zt';
            else
                error('Incorrect size')
            end

            % Call Internal Nonlinear Force Functions
            if nargout > 1
                [fIntNL,dfdx] = obj.Calc_f(xdt);
            else
                [fIntNL] = obj.Calc_f(xdt);
            end
            
            fExternal = (obj.Fext*(cos(wt*t/obj.nu)))';
            fThermal = (obj.romObj.fThermal*(ones(size(t))))';
            
            % Reorder
            fIntExt = fExternal+0*fThermal;

            % Combine and transform
            b = FT*(fIntExt(:)-fIntNL);

            % Ouput jacobian if desired
            if nargout > 1
                varargout{1} = FT*dfdx*IFT;
            end
    
        end
        
        % Compute the nonlinear internal force across the orbit
        function [f,dfdx] = Calc_f(obj,xdt)
            % Computes the internal (nonlinear) forces of the system. 
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            % Outputs : 
            %           fint = nonlinear internal force value [[nDOF*nSAMPLES , 1]
            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Take the time domain results 
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])'; 
            
            % Initialize internal force matrix
            fIntNl = zeros(obj.nDOF,nT);
%             Knl = cell(nT,1);
            rowGlobal = cell(nT,1);
            colGlobal = cell(nT,1);
            valGlobal = cell(nT,1);
            % Check if parallel option is supplied
            if obj.parallelOpts.logic
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.romObj());
                
                % Loop Through and Generate Internal Force
                parfor ii = 1:nT
                    [fIntNl(:,ii),Knl] = sharedObj.Get_NlForce(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            else
                % Loop Through and Generate Internal Force
                for ii = 1:nT
                    [fIntNl(:,ii),Knl] = obj.romObj.Get_NlForce(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            end
            
            % Reshape Nonlinear Forcing
            f = reshape(fIntNl',[],1); %(:);  
            
            % If nonlinear stiffness is requested
            if nargout > 1
                % Combine all the time samples
                rowGlobal = vertcat(cell2mat(rowGlobal));
                colGlobal = vertcat(cell2mat(colGlobal));
                valGlobal = vertcat(cell2mat(valGlobal));

                % Create sparse tangent stiffness matrix across orbit
                dfdx = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                       nT*obj.nDOF,nT*obj.nDOF);   
                varargout{1} = dfdx; 
            end
        end
        
        % Compute the nonlinear force gradient across the orbit
        function [dfdx] = Calc_dfdx(obj,xdt)
            % Computation of the first derivative of the internal forces, with
            % respect to the displacements J = d(fint_nl(q))/d(q), of NLROMs
            %
            % Inputs : 
            %           obj = HBnlrom object
            %           xdt = displacement values [nDOF*nSAMPLES , 1]
            %              xdt=[x1(t1),x1(t2)...,x1(nT),x2(t1),...].'
            % Outputs : 
            %           dfdx = Jacobian of nonlinear forces [nDOF*nSAMPLES, nDOF*nSAMPLES ]

            
            % Grab number of time samples
            nT = obj.HBcontrol.nT;
            
            % Take the time domain results 
            % Reshape x_til at the current solution point
            x_d = reshape(xdt,nT,[])'; 
            
            % Initialize internal force matrix
%             Knl = cell(nT,1);
            
            % Initalize Matrices
            rowGlobal = cell(nT,1);
            colGlobal = cell(nT,1);
            valGlobal = cell(nT,1);
                
            % Decide if doing serial or parallel
            if obj.parallelOpts.logic
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.romObj());
                
                % Loop Through and Generate Internal Force
                parfor ii = 1:nT
                    Knl = sharedObj.Get_NlStiffness(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            else
                % Loop Through and Generate Internal Force
                for ii = 1:nT
                    Knl = obj.romObj.Get_NlStiffness(x_d(:,ii));
                    [rowLocal,colLocal,val] = find(Knl);
                    rowGlobal{ii} = (rowLocal-1)*nT + ii;
                    colGlobal{ii} = (colLocal-1)*nT + ii;
                    valGlobal{ii} = val;
                end
            end
            
            % Combine all the time samples
            rowGlobal = vertcat(cell2mat(rowGlobal));
            colGlobal = vertcat(cell2mat(colGlobal));
            valGlobal = vertcat(cell2mat(valGlobal));
            
            % Create sparse tangent stiffness matrix across orbit
            dfdx = -1*sparse(rowGlobal,colGlobal,valGlobal,...
                   nT*obj.nDOF,nT*obj.nDOF);             
            
        end
        
        % Compute energy
        function [energy] = Calc_Energy(obj,xt,vt)
            % This function computes the energy of a specific solution
            % 
            % Inputs: 
            %           obj = HBnlrom Class Object
            %           xt = displacement [nDOF x 1]
            % Outputs:
            %           energy = energy at solutoin [1x1]
            %           
          

            % Compute nonlinear internal force
            Knl = obj.romObj.Get_NlStiffness(xt);
            
            % Compute Energy
            energy = (1/2)*xt.'*obj.K*xt + ...
                     (1/6)*xt.'*Knl*xt +...
                     0.5*vt'*obj.M*vt;
            
        end
        
        % Compute periodicity
        function [obj] = Calc_Periodicity(obj,nSteps,varargin)
            % This function integrates the previous solution point, calculates the
            % periodicity, and prints the output to the command window.
            %
            % Inputs :
            %           obj
            %           nSteps = number of integration points
            %           vaarargin{1} = solution point of interest
            % Outputs :
            %           obj
            %
            
            % Check additional arguments
            if nargin > 2
                intSol = varargin{1};
                % Print message to command window
                fprintf('Beginning integration at provided solution point \n')
            else
                intSol = obj.solpt-1;
                % Print message to command window
                fprintf('Beginning integration of previous solution point \n')
            end
            
            % Grab osfern object
            feObj = obj.romObj.osFern;
            
            % Grab solution
            qSol = obj.x(:,intSol);
            xSol = obj.romObj.basis*qSol + obj.romObj.initialDisplacement;
            fSol = obj.w(intSol)/2/pi;
            
            % Set integration parameters
            T = 1/fSol; dt = T/nSteps; name = 'intMhbSol';
            
            % Initialize thermal object
            dynamic = DynamicThermal(name, feObj, xSol);

            % Add thermal field to thermal object
            dynamic.AddThermalField(obj.romObj.thermal.thermalField{:});
            
            % dynamic.SetPlotOpts(true,5);
            
            % Integrate solution
            dynamic.Solve(T, dt);
            
            % Calculate Periodicity
            xPer = norm((dynamic.initialDisplacement - dynamic.finalDisplacement))/...
                norm(dynamic.initialDisplacement);
            
            % Plot some informatoin
            figure;
            subplot(1,3,1); feObj.Plot(dynamic.initialDisplacement);
            title('Initial Displacement');
            subplot(1,3,2); feObj.Plot(dynamic.finalDisplacement);
            title('Final Displacement');
            subplot(1,3,3); feObj.Plot(dynamic.initialDisplacement-dynamic.finalDisplacement);
            title(['Residaul, \epsilon = ',num2str(xPer)]);
            
            % Print periodicity results to command window
            fprintf(['Periodicity =', num2str(xPer), '\n'])
            
        end
        
        % Plot physical displacement
        function PlotPhysical(obj,sp_axes,sp_data)

            % Update for full fe model
            xFullFem = obj.romObj.initialDisplacement + ...
                       obj.romObj.basis*obj.x(:,obj.solpt);

            % Plot using FERN Plotting Tool
            cla(sp_axes{2}); 
            hold(sp_axes{2},'on') 
            obj.romObj.osFern.Plot(xFullFem,'norm',sp_axes{2});
            hold(sp_axes{2},'off')
        end
    end
    
end