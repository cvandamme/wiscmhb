function [ obj ] = CheckStopping(obj)
% This functions checks if the harmonic balance contiuation routine should
% be stopped due to the applied criteria and will ouput the criteria that
% caused the the stop.
%
% Inputs :
%           obj = WiscMhb Object
%
% Ouputs :
%           obj = WiscMhb Object


% Check if amplitude limit is exceeded
if ~isempty(obj.stopCriteria.amplitude)
    if isfield(obj.stopCriteria,'dof') && ~isempty(obj.stopCriteria.dof)
        dof2check = obj.stopCriteria.dof;
    else
        dof2check = 1; % default to first dof (works for roms, not for fem)
    end
    if obj.stopCriteria.amplitude < 0
        if max(obj.x(dof2check,obj.solpt)) < obj.stopCriteria.amplitude
            fprintf('Amplitude Exceeded --> Ending Continuation \n')
            obj.stopCriteria.criteria = 'amplitude';
            obj.HBcontrol.endcont = 0;
        end
    else
        if max(abs(obj.x(dof2check,obj.solpt))) > obj.stopCriteria.amplitude
            fprintf('Amplitude Exceeded --> Ending Continuation \n')
            obj.stopCriteria.criteria = 'amplitude';
            obj.HBcontrol.endcont = 0;
        end
    end
end

% Check if force limit is exceeded
if ~isempty(obj.stopCriteria.force)
    if abs(obj.forceCalc.fAmp(obj.solpt)) > obj.stopCriteria.force
        fprintf('Force Amplitude Exceeded --> Ending Continuation \n')
        obj.stopCriteria.criteria = 'force';
        obj.HBcontrol.endcont = 0;
    end
end

% Check if Freq limits exceeded
if ~isempty(obj.stopCriteria.frequency)
    if obj.w(obj.solpt) < obj.stopCriteria.frequency(1)
        fprintf('Frequency Boundary Exceeded --> Ending Continuation  \n')
        obj.stopCriteria.criteria = 'frequency_lb';
        obj.HBcontrol.endcont = 0;
    elseif obj.w(obj.solpt) > obj.stopCriteria.frequency(2)
        fprintf('Frequency Boundary Exceeded --> Ending Continuation  \n')
        obj.stopCriteria.criteria = 'frequency_ub';
        obj.HBcontrol.endcont = 0;
    end
end

% Check if energy limit is exceeded
if ~isempty(obj.stopCriteria.energy)
    if obj.E(obj.solpt) > obj.stopCriteria.energy
        fprintf('Energy Exceeded --> Ending Continuation \n')
        obj.stopCriteria.criteria = 'energy';
        obj.HBcontrol.endcont = 0;
    end
end

% Check if # of solutions exceeds defined max
if obj.solpt > obj.HBcontrol.nSols
    fprintf('Number of Solutions Exceeded --> Ending Continuation  \n')
    obj.stopCriteria.criteria = 'numSols';
    obj.HBcontrol.endcont = 0;
end

end

