function [obj] = InterpolateFinalSol(obj)
% This function interpolates the solutions based upon frequency of the last
% solution and previous in order to get a more precise answer at the point
% of interest. 
% Note - Use with caution because this is a linear interpolation routine. 
% 
% Inputs :
%           obj = WiscMhb Object
%           w1 = 


% Grab final solutions values
xF = obj.x(:,obj.solpt);
zF = obj.z(:,obj.solpt);
eF = obj.E(obj.solpt);
wF = obj.w(obj.solpt);

% Determine which critreia to use as query value
switch obj.stopCriteria.criteria
    case 'frequency_lb'
        queryVal = obj.stopCriteria.frequency(1);
        stateVec = obj.w([obj.solpt-1,obj.solpt]);
    case 'frequency_ub'
        queryVal = obj.stopCriteria.frequency(2);
        stateVec = obj.w([obj.solpt-1,obj.solpt]);
    case 'amplitude'
        queryVal = obj.stopCriteria.amplitude;
        stateVec = max(abs(obj.x(:,[obj.solpt-1,obj.solpt])));
    case 'force'
        queryVal = obj.stopCriteria.force;
        stateVec = abs(obj.forceCalc.fAmp([obj.solpt-1,obj.solpt]));
    case 'energy'
        queryVal = obj.stopCriteria.energy;
        stateVec = abs(obj.E([obj.solpt-1,obj.solpt]));
    otherwise 
        
end
        
% Interpolate the solutions    
xQuery = interp1(stateVec,obj.x(:,[obj.solpt-1,obj.solpt])',queryVal,'pchip'); 
zQuery = interp1(stateVec,obj.z(:,[obj.solpt-1,obj.solpt])',queryVal,'pchip'); 
eQuery = interpn(stateVec,obj.E(:,[obj.solpt-1,obj.solpt]),queryVal,'pchip'); 
wQuery = interpn(stateVec,obj.w(:,[obj.solpt-1,obj.solpt]),queryVal,'pchip'); 

if obj.forceCalc.logic
    fQuery = interpn(stateVec,obj.forceCalc.fAmp([obj.solpt-1,obj.solpt]),queryVal,'pchip'); 
end
% Assign newly interpolate values to final solution
obj.x(:,obj.solpt) = xQuery';
obj.z(:,obj.solpt)= zQuery';
obj.E(obj.solpt) = eQuery;
obj.w(obj.solpt) = wQuery;
if obj.forceCalc.logic
    obj.forceCalc.fAmp(obj.solpt) = fQuery;
end
end

