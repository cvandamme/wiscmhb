function [Jw] = Calc_dhdw(obj,wt)
% This function computes the gradient of the harmnoic balance
% function wrt to the current frequency
% Inputs:
%           obj = HB object
%           wt = Current Frequency Vale
% Ouputs:
%           Jw = Gradient of H wrt to frequency (w)

% Initialize Variables and Indexs
NABLA1 = zeros((1+2*obj.nH),(1+2*obj.nH));
NABLA2 = zeros((1+2*obj.nH),(1+2*obj.nH));
index1 = 2;index2 = 3;

% Loop Through to generate submatrices and assemble
for ii = 1:obj.nH
    nabla1 = [0 -(ii/obj.nu); (ii/obj.nu) 0];
    nabla2 = [-2*(ii/obj.nu)*(ii*wt/obj.nu) 0; ...
              0 -2*(ii/obj.nu)*(ii*wt/obj.nu)];
    NABLA1(index1:index2,index1:index2)= nabla1;
    NABLA2(index1:index2,index1:index2)= nabla2;
    index1 = index2+1;index2 = index1+1;
end

% Generate Linear Dynamic Matrix
Jw = kron(NABLA2,obj.M) + kron(NABLA1,obj.C);

end