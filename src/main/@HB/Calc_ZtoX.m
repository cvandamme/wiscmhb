function [Xt] = Calc_ZtoX(obj,zt,wt,varargin)
% This function converts the fourier coefficients to the modal
% amplitudes using the transformation.
% Inputs :
%           obj
%           zt = current solution fourier coefficients
%           wt = current solution frequency (rad/sec)
%           varargin{1} = number of points along orbit to use
% Output :
%           xt = time domain amplitude

% If additional argument is supplied are input then the 
if nargin < 4
    t = 0; nPts = 1;
else
    % Create time point of interest
    nPts = varargin{1};  t = linspace(0,2*pi/wt,nPts); 
    
    % Don't take last point because it (if a harmonic solution) is 
    % the same as the first
    if nPts == 1
        t = 0;
    else
%         nPts = nPts -1; t = t(1:end-1);
    end
end

% Identity Multiplier
In = speye(obj.nDOF); % zHv = (1/sqrt(2)).*ones(nPts,1);
zHv = ones(nPts,1);
% Generate Sine, Cosine Vectors and Generate GAMMA Matrix
% (Kinda Slow -could improve) want to use --> S = sparse(i,j,v,m,n)
index = 1; Q = zeros(nPts,obj.nH+1); Q(:,1) = zHv;
for ii = 1:obj.nH
    Q(:,index+1) = sin(ii*wt.*t/obj.nu)'; index = index + 1;
    Q(:,index+1) = cos(ii*wt.*t/obj.nu)'; index = index + 1;
end

% Transform to time domain
Xt = kron(Q,In)*zt;

% Reshape for output
Xt = reshape(Xt,obj.nDOF,[]);

end