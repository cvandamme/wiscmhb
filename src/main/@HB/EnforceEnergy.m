function [obj,E] = EnforceEnergy(obj,zt,wt)
% This function takes a current solution and checks the prediction to
% ensure that the next step will increase in energy. If that is not the
% case then the function will reverse the prediction. 

% Perturb the previous solution by stp size in either direction of
% prediction
zP(:,1) = zt + obj.HBcontrol.stp*obj.P(1:end-1,obj.solpt);
zP(:,2) = zt - obj.HBcontrol.stp*obj.P(1:end-1,obj.solpt);

% Transform to x 
xT(:,1) = obj.Calc_ZtoX(zP(:,1),wt);
xT(:,2) = obj.Calc_ZtoX(zP(:,2),wt);
vT(:,1) = obj.Calc_ZtoV(zP(:,1),wt);
vT(:,2) = obj.Calc_ZtoV(zP(:,2),wt);

% Calculate energy
E(1) = obj.Calc_Energy(xT(:,1),vT(:,1));
E(2) = obj.Calc_Energy(xT(:,2),vT(:,2));

% Call State Functions
% [b] = obj.calcb(zt,wt);

% Linear dynamic matrix and derivative
% A = obj.calcA(wt);
% [Aw] = calcJw(obj,wt);
% 
% % Harmnoic Balance Energy
% E = zt'*A*zt + zt'*b;
% 
% dEdw = zt'*Aw*zt;

end