function [zt] = Calc_XtoZ(obj,xt,wt)
% This function converts the fourier coefficients to the modal
% amplitudes using the transformation.
% Inputs :
%           obj
%           zt = current solution fourier coefficients
%           wt = current solution frequency (rad/sec)
% Output :
%           xt = physical/modal amplitude


% Generate Tranformation Matrix at t = 0 (initial amplitudes)
% Q = zeros(obj.nDOF,obj.nDOF*(2*obj.nH+1));
% In = eye(obj.nDOF);
% Q(:,1) = 1/sqrt(2);
% index = 2;
% for ii = 1:obj.nH
%     Q(:,index:index+obj.nDOF-1) = kron(sin(ii*wt.*0/obj.nu),In)';
%     index = index + obj.nDOF;
%     Q(:,index:index+obj.nDOF-1) = kron(cos(ii*wt.*0/obj.nu),In)';
%     index = index + obj.nDOF;
% end

Q = kron([1 repmat([0 1],1,obj.nH)],speye(obj.nDOF));

% Tranform to Modal Domain
zt = pinv(full(Q))*xt;

end