function [obj] = Calc_Periodicity(obj,nSteps,varargin)   
% This function integrates the previous solution point, calculates the
% periodicity, and prints the output to the command window.
% 
% Inputs :
%           obj 
%
% Outputs :
%           obj      
%

% Check additional arguments
if nargin > 2
    intSol = varargin{1};
    % Print message to command window
    fprintf('Beginning integration at provided solution point \n')
else
    intSol = obj.solpt-1;
    % Print message to command window
    fprintf('Beginning integration of previous solution point \n')
end



% Try and get FE properties
try
    mfObj = obj.mf; %#ok<*NASGU>
catch
    fprintf('Sorry, but integrate only works with OsFern FE Models \n')
    return
end

% Get FE Model properties
freeDof = mfObj.BuildConstraintVec(); %#ok<*UNRCH>
xSol = zeros(mfObj.nDof,1);

% Get displacement vector and frequency for previous solution
xSol(freeDof) =  obj.x(:,intSol);
fSol = obj.w(intSol)/2/pi;

% Set integration parameters
T = 1/fSol; dt = T/nSteps; name = 'intMhbSol';

% Choose which one to use
if isa(obj,'HbFemThermal') || isa(obj,'HBfem_thermal')
    % Initialize thermal object
    dynamic = DynamicThermal(name, mfObj, xSol);
    
    % Add thermal field to thermal object
    dynamic.AddThermalField(obj.thermal.thermalField{:});
else
    dynamic = Dynamic(name, mfObj, xSol);
end

% dynamic.SetPlotOpts(true,5); 


% Integrate solution
dynamic.Solve(T, dt);

% Calculate Periodicity
xPer = norm((dynamic.initialDisplacement - dynamic.finalDisplacement))/...
        norm(dynamic.initialDisplacement);

figure; 
subplot(1,2,1);hold on;
mfObj.Plot(dynamic.initialDisplacement);
mfObj.Plot(dynamic.finalDisplacement);
subplot(1,2,2);
mfObj.Plot(dynamic.initialDisplacement-dynamic.finalDisplacement);

% Print periodicity results to command window
fprintf(['Periodicity =', num2str(xPer), '\n'])

end