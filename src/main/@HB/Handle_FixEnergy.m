function [f,varargout] = Handle_FixEnergy(obj,eFix,xIn)
% This function can be used as a handle within an optimization to fix the 
% energy of the solution.

% Grab state variables
zC = xIn(1:end-1); wC = xIn(end);

% Calculate residual
[h,dh] = obj.Calc_h(zC,wC); % Evaluate Residual Equation

% Compute energy
[e,de] = obj.Calc_KineticEnergy(zC,wC);

% Assemble output
f = [h; e-eFix];

% If gradient is requested 
if nargout > 1
    varargout{1} = [dh; de'];
end


end