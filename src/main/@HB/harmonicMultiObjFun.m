function [f,df] = harmonicMultiObjFun(obj,yt)
% This is the function that must be satisfied by harmonic
% balance
%          H(z,w) = A(w)*z -b(z) --> 0
%

% Extract Frequency adn generate linaer matrix
wt = yt(end);
A = obj.calcA(wt);

% ----- 1st harmonic only ------ % 
z1 = zeros((2*obj.nH+1)*obj.nDOF);
z1(2*obj.nH+1) = yt(1:(2*obj.nH+1)*1);

% Call State Functions
b1 = obj.calcb(z1,wt);

% Harmnoic Balance Equation
h1 = A*zt1 - b1;
nh1 = norm(h1);

% Gradient Info
[jW1] = calcJw(zt1,wt);
[jZ1] = calcJz(zt1,wt);
jh1 = [jZ1 jW1];

% Gradient of norm of vector
dh1 = ((jh1')*h1)/nh1;

% ------ Full Harmonics -------- % 
zf = yt(1:(2*obj.nH+1)*obj.nDOF);

% Call State Functions
bf = obj.calcb(zf,wt);

% Harmnoic Balance Equation
hf = A*ztf - bf;
nhf = norm(hf);

% Gradient Info
[jWf] = calcJw(ztf,wt);
[jZf] = calcJz(ztf,wt);
jhf = [jZf jWf];

% Gradient of norm of vector
dhf = ((jhf')*h1)/nh1;

% Assemble
f = [h1;hF;w1
Geq = [dh1;dhf];


end