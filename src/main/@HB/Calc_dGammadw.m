function [IFT,FT] = Calc_dGammadw(obj,wt)
% This function computes the derivative GAMMA operator wrt to frequency.
% This is used to transform velocity from time to frequency domain.
%
% Inputs:
%       obj = contains the following
%           .nDOF = number of degrees of freedom
%           .nH = number of harmonics
%           .nT = number of time samples
%           .T = Current Solution period of oscillation
%           .nu = subharmonic mulitplier
%       wt = current solution value
%
% Outputs:
%       dFTdw = Gamma operator 
%       dIFTdw = Inverse GAMMA operator
%

% Extract time sample rate (future maybe variable??)
nT = obj.HBcontrol.nT;

% Generate Time Sequence
t = linspace(0,2*pi/wt,nT);

% Identity Multiplier
In = speye(obj.nDOF); zHm = kron(In,zeros(nT,1));

% Generate Sine, Cosine Vectors and Generate GAMMA Matrix
% (Kinda Slow -could improve) want to use --> S = sparse(i,j,v,m,n)
index = 1; tempSC = cell(1,2*obj.nH);
Q = zeros(nT,obj.nH+1); Q(:,1) = zeros(nT,1);
for ii = 1:obj.nH
    Q(:,index+1) = (ii/obj.nu)*cos(ii*wt.*t/obj.nu);
    tempSC{index} = kron(In,(ii/obj.nu)*cos(ii*wt.*t/obj.nu))';
    index = index + 1;
    Q(:,index+1) = -(ii/obj.nu)*sin(ii*wt.*t/obj.nu);
    tempSC{index} = kron(In,-(ii/obj.nu)*sin(ii*wt.*t/obj.nu))';
    index = index + 1;
end
SC = sparse(cell2mat(tempSC));

% Generate FT and IFT
IFT = horzcat([zHm SC]);

% Compute pseudo inverse - Trick version 
qPinv = pinv(Q);
ftCell = cell(2*obj.nH+1,1);
for ii = 1:size(Q,2)
    ftCell{ii} = kron(In,qPinv(ii,:));
end
FT = vertcat(cell2mat(ftCell));

% Check to make sure they are the right size
if size(IFT) ~= [obj.nDOF*nT,obj.nDOF*(2*obj.nH+1)]
    error('Inverse FT is incorrect size')
end
if size(FT) ~= [obj.nDOF*(2*obj.nH+1),obj.nDOF*nT]
    error('FT is incorrect size')
end


end