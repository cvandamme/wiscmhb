function [obj,dhdz,dhdw] = Correct(obj,Hpr,Zpr,Wpr,varargin)
% This function performs the correction portion of the
% predictor-corrector algorithm. Used basic newton update
% scheme with Moore-Penrose Option. Only initiatied if the intial
% guess is > allowed tolerance
% Inputs:
%           obj = Class Object
%           Hpr = Residual of Predicted State Variables
%           Zpr = Predicted Fourier Coefficient
%           Wpr = Predicted Frequency
% Outputs:
%           obj = Updated Class Object
%           dhdz  = Fourier coefficient Jacobian
%           dhdw  = Frequency Jacobian

% Bring in previous solution values current solution values
if obj.solpt == 1
    z0 = Zpr; zC = z0;
    w0 = Wpr; wC = w0;
    y0 = [z0;w0]; yC = y0;
    H0 = Hpr; HC = H0;
    v0 = obj.P(:,obj.solpt); vC = v0;
else
    z0 = obj.z(:,obj.solpt-1); zC = Zpr;
    w0 = obj.w(obj.solpt-1); wC = Wpr;
    y0 = [z0;w0]; yC = [zC;wC];
    H0 = Hpr; HC = H0;
    v0 = obj.P(:,obj.solpt); vC = v0;
end

% Check if variables arguments are passed 
if nargin > 4; dh = varargin{1}; else; dh = []; end

% Start iteration counts
k=0; innerIter = 0;

% try fsolve
% fun = @(xIn) obj.MhbCorrectionFsolveFun(xIn);
% options = optimoptions('fsolve','Display','iter','SpecifyObjectiveGradient',true);
% [xF,fVal] = fsolve(fun,[zC;vC;wC],options);

% --- Start Correction Loop --- %
while norm(HC)/norm(zC) > obj.HBcontrol.tol % && norm(correct1)*obj.HBcontrol.stp > obj.HBcontrol.tol
    
    % For first iteration compute jacobian
    if isempty(dh)
        tic 
        [dhdz] = obj.Calc_dhdz(zC,wC);
        [dhdw] = obj.Calc_dhdw(wC);
        dhdw = dhdw*zC;
        obj.timings.JacEval = [obj.timings.JacEval toc];
        if ~obj.HBcontrol.silent
            disp(['Took ',num2str(round(obj.timings.JacEval(end),2)),...
                 ' Second to Compute Nonlinear Jacobian'])
        end

        % Generate State Matrices
        Gy = [dhdz dhdw ; vC'];  % Jacobian
        R = [[dhdz dhdw]*vC; 0]; % Prediction Update
    else
        Gy = [dh; vC'];
        R =  [dh*vC; 0]; % Prediction Update
    end
    
    % Residual vectors
    G  = [HC; 0];       % Residual 
    
    % Filter static components during correction to save solve time
    if obj.HBcontrol.NNMflag && false
        sIndices = zeros(obj.nH,obj.nDOF);
        index = 1;
        for nn = 1:2:2*obj.nH
            sIndices(index,:) = obj.nDOF*nn+1:(1+nn)*obj.nDOF;
            index = index +1;
        end
        sIndices = sIndices(:); indices = setdiff(1:length(R),sIndices);
        Gy(sIndices,:) = []; Gy(:,sIndices) = [];
        R(sIndices) = []; G(sIndices) = [];
    end
    
    % Solve linear system
    correct = obj.SolveLinearSystem(Gy,[G,R]);
    correct1 = correct(:,1); correct2 = correct(:,2);

    % Resize correction to full system
    if obj.HBcontrol.NNMflag && false
        correct1f = zeros(size(y0)); correct1f(indices) = correct1;
        correct2f = zeros(size(y0)); correct2f(indices) = correct2;
    else
        correct1f = correct1; correct2f = correct2;
    end
    
    relax = 1;
    % ------- Update Values ---------
    yC = yC - relax*correct1f; % State Variables
    vC = vC - relax*correct2f; % Prediction Vector
    vC = vC/norm(vC);   % Renormalize Again ????
    zC = yC(1:end-1);   % Fourier Coefficients
    wC = yC(end);       % Frequency
    
    % save old correction values
    correct1_old = correct1;  correct2_old = correct2;
    
    % --- Evaluate Residual Equation --- %
    tMhb = tic;
    [HC,dh] = obj.Calc_h(zC,wC);
    tMhbTime = toc(tMhb);
    obj.timings.FunEval = [obj.timings.FunEval tMhbTime];
    
    % Update Iteration Count
    k=k+1;
    if ~obj.HBcontrol.silent
        disp(['Took ',num2str(round(tMhbTime,2)),...
              ' Seconds to Evaluate MHB Equation'])
        fprintf(['Iteration: ',num2str(k),' -> Residual = ',num2str(norm(HC)/norm(zC)),...
                 ', deltaZ = ',num2str(norm(correct1)),' \n'])
    end
    
    % ------- IF CONVERGENCE NOT MET ----------- %
    % if  k > obj.HBcontrol.optiter || norm(HC)/norm(zC) > norm(H0)/norm(z0) % MSA REplaced - use maxiter here)
    if  (k > obj.HBcontrol.optiter*1 || k > obj.HBcontrol.maxiter) && obj.solpt ~= 1 %|| norm(HC)/norm(zC) > (norm(H0)/norm(z0))*obj.HBcontrol.divfactor*10
        innerIter = innerIter + 1;
        if k >= obj.HBcontrol.maxiter
            if ~obj.HBcontrol.silent
                fprintf('Too many iterations --- Reducing Step Size  \n ');
            end
        elseif norm(HC)/norm(zC) > norm(H0)/norm(z0)
            if ~obj.HBcontrol.silent
                fprintf('Residue is increasing --- Reducing Step Size  \n');
            end
        end
        
        % Reduce stepsize of predictor if convergence isn't met
        obj.HBcontrol.stp = obj.HBcontrol.stp/obj.HBcontrol.stpdf;
        
        % Check if minimum step size is reached
        if abs(obj.HBcontrol.stp) < obj.HBcontrol.stpmin
            obj.HBcontrol.endcont = 0; 
            obj.stopCriteria.criteria = 'MinStepSize';
            obj.HBcontrol.stp = obj.HBcontrol.stpmin;
            fprintf('Minimum step size reached, algorithm terminating  \n');
        end
        
        % Exit Continuation
        if obj.HBcontrol.endcont == 0
            break
        end
        if ~obj.HBcontrol.silent
             fprintf(['New Stepsize: ',num2str(obj.HBcontrol.stp),'  \n']);
        end
        
        if k > obj.HBcontrol.maxiter + 10
            fprintf('Performing Random Perturbation Step \n');
            randPert = rand((2*obj.nH+1)*obj.nDOF,1);
            randPert = randPert/norm(randPert);
            randW = rand(1,1);
            
            % Re-Evaluate predictions
            zC = z0 + obj.HBcontrol.stp*randPert.*v0(1:end-1);
            wC = w0 + obj.HBcontrol.stp*randW.*v0(end);
            vC= v0; k = 0;
        else
            % Re-Evaluate predictions
            zC = z0 + obj.HBcontrol.stp*v0(1:end-1);
            wC = w0 + obj.HBcontrol.stp*v0(end);
            vC= v0;
        end
        
        % Reset count 
        k = 1;
        
        % --- Evaluate the New Prediction --- %
        [Hpr] = obj.Calc_h(zC,wC);
        H0 = HC; HC = Hpr;
        dy = HC-H0;
        yC = [zC;wC];
    end
end

% Grab gradients 
dhdz = dh(:,1:end-1); dhdw = dh(:,end);

% Save Converged Values
obj.w(obj.solpt) = wC;
obj.x(:,obj.solpt) = obj.Calc_ZtoX(zC,wC);
obj.v(:,obj.solpt) = obj.Calc_ZtoV(zC,wC);
obj.z(:,obj.solpt) = zC;
obj.P(:,obj.solpt) = vC;

% Store Continuation Info
obj.HBcontrol.numks(obj.solpt) = k;
obj.HBcontrol.stps(obj.solpt) = obj.HBcontrol.stp;

end