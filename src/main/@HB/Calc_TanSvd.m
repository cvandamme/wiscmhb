function [obj] = CalcTanSvd(obj,AA)
[~,~,V]=svd(AA,0);          % 0 gives compact SVD
Pta=V(:,end);               % P from this is automatically unit length
N = (size(AA,2)-1);
%             Now assign sign of P.  The above gives a perpendicular but it could
%             point in either direction.  My logic below may not be perfect (MSA)
if all(sign(Pta(1:N))<0)    % all disp P's are negative
    Pta=-1*Pta;
elseif sum(Pta(1:N))<0      % if the weighted sum is negative
    Pta=-1*Pta;
else
    % leave alone??
end
if dot(Pta,obj.P(:,obj.solpt-1))>=0
    tang = Pta;
else
    tang = -Pta;
end
obj.P(:,obj.solpt) = tang/norm(tang);
end