function [zF,wF,h] = HarmIncr(mhbObj,z0,w0)
% This function is used to calculate new solutions based upon the previous
% while minimizing the distance between the new solution and previous.
% Ideally this will serve a a continuation free approach once a single
% harmonic solution is computed. 
% The used 
%
% Input : 
%        mhbObj = HB object
%        z0 = fourier coefficient of solution that the new solutions should
%               be close to
%        w0 = frequency of solution that the new solutions should
%               be close to
% Ouput : 
%        zF = new solution
%        wF = new solution
%        h = vector of mhb residual

% Check size of provided vectors
[mZ,nZ] = size(z0); nW = length(w0);

% Initial coefficients from low harmonic solution
zC = zeros((2*mhbObj.nH+1)*mhbObj.nDOF,1);
zC(1:length(z0)) = z0;

% Setup minimization function
minFun = @(xC) mhbObj.MhbMinDistFun(xC,zC,w0);

% Setup constraint function
conFun = @(xC) mhbObj.MhbFun(xC);

% Call the Function to call fmincon 
options=optimoptions(@fmincon,...
                  'SpecifyObjectiveGradient',true,...
                  'SpecifyConstraintGradient',true,...
                  'CheckGradients',false,...
                  'Display','iter-detailed',...
                  'FunctionTolerance',1e-6,...
                  'OptimalityTolerance',1e-6,...
                  'RelLineSrchBnd',10,...
                  'Algorithm','sqp',... 
                  'StepTolerance',1e-12,...
                  'MaxFunctionEvaluations',2000);
              
% Assign constraint function
bounds = [];

% Create initial guess
zStart = zeros((2*mhbObj.nH+1)*mhbObj.nDOF,1);
zStart(1:length(z0)) = z0;
wStart = w0;

% Call "fmincon" to find state variable update dD by minimizing J
[xF,jVal,exitFlag,OUTPUT,lambda,djVal]=fmincon(minFun,[zStart;wStart],...
                                      [],[],[],[],[],[],conFun,options);

% Grab frequency and fourier coefficients
zF = xF(1:end-1); wF = xF(end);


end