function xVec = SolveLinearSystem(obj,Amat,bVec)
% This function is used to solve the linear system 
% Ax=b within the harmonic balance continuation code. This specific
% function is used for organization as well as controls for how to solve
% the linear system.
%           Amat*xVec = bVec
% inputs :
%       obj = Mhb object
%       Amat = matrix
%       bVec = vector 
% outputs :
%       xVec = vector
% obj.HBcontrol.LinSolve = 'GMRES';
% Start timer
tLinSolve = tic;

% Choose which way to solve linear system
if strcmp(obj.HBcontrol.LinSolve,'Direct') % || obj.solpt == 1
    % Try a reoording algorithm to speed up
    %p = amd(Amat);
%     tic; L = chol(Amat,'lower'); toc
%     tic; Lp = lu(Amat(p,p),'lower'); toc;
    xVec = Amat\bVec; %xVec = xVec(p);
%     tic; xVecp = Amat(p,p)\bVec(p); toc
elseif strcmp(obj.HBcontrol.LinSolve,'pcg')
    pp = amd(Amat); 
    pVals = diag(Amat(pp,pp)); pVals(pVals==0) = 1; nP = length(pVals);
    pMat = sparse([1:nP]',[1:nP]',pVals,nP,nP);
    [correct1,flag1,relres1,iter1,resvec1] = pcg(Amat(pp,pp),bVec(pp,1),1e-4,5000,pMat);
    [correct2,flag2,relres2,iter2,resvec2] = pcg(Gy(pp),R(pp),1e-4,5000,pMat);
    xVec = [correct1, correct2];
elseif strcmp(obj.HBcontrol.LinSolve,'BiCGS')
    [correct1,flag1,relres1,iter1,resvec1] = bicgstabl(Gy,G,1e-4,500,diag(diag(Gy)));
    [correct2,flag2,relres2,iter2,resvec2] = bicgstabl(Gy,R,1e-4,500,diag(diag(Gy)));
    xVec = [correct1, correct2];
elseif strcmp(obj.HBcontrol.LinSolve,'GMRES')
    setup.udiag =1; setup.droptol = 1e-8;
    if Amat(end,end) == 0; Amat(end,end) = 1e-12; end 
    pp = amd(Amat); [L,U] = ilu(Amat(pp,pp),setup);
    xVec = zeros(size(bVec));
    for ii = 1:size(xVec,2)
        [xVec(:,ii),flag,relres,iter,resvec] = gmres(Amat(pp,pp),bVec(pp,ii),...
                                                 10,1e-6,1e3,L,U);
        if flag ~= 0
            warning('Solution of Ax=b did not converge to tolerance');
            disp(num2str(relres));
        end
    end
    xVec = xVec(pp,:);
elseif strcmp(obj.HBcontrol.LinSolve,'GPU')
    tGpuTransfer = tic;
    gGy = gpuArray(Gy); gG = gpuArray(G); gR = gpuArray(R);
    disp(['Took ',num2str(round(toc(tGpuTransfer),2)),'Seconds to Send Data to GPU'])
    correct1 = gGy\gG; correct2 = gGy\gR; xVec = [correct1, correct2];
end
tLinSolveTime = toc(tLinSolve);
% Ouput timings if desired
if ~obj.HBcontrol.silent
    obj.timings.LinSolve = [obj.timings.LinSolve tLinSolveTime];
    disp(['Took ',num2str(round(tLinSolveTime,2)),...
         ' seconds to solve linear system using ', obj.HBcontrol.LinSolve])
end

end