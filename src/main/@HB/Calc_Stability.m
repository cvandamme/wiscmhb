function [obj] = Calc_Stability(obj,wt,hz)
% This function determines if a certain solution is stable or
% not based upon Hill's method. 
% 
% Inputs :
%           obj 
%           wt = current solution frequency
%           hz = jacobian of harmonic balance equation wrt to fourier
%                coefficients
% Outputs :
%           SPlogic = Stability Info (0 = stable , 1 = unstable)
%           Btil = Frequency Domain Floquet Exponents
% 
% ***** Do we just terminate the alogrithm if solution is unstable or try
% to predict a new solution *******

if isempty(hz)
    hz = obj.Calc_dhdz(obj.z(:,obj.solpt),wt);
end

% Generate Nabla Parameter
index1 = 2;
index2 = 3;
for ii = 1:obj.nH
    nabla1 = [0 -2*(ii*wt/obj.nu); 2*(ii*wt/obj.nu) 0];
    NABLA1(index1:index2,index1:index2)= nabla1;
    index1 = index2+1;
    index2 = index1+1;
end

% Generate Delta Functions
D1 = kron(NABLA1,2*obj.M) + kron(speye(2*obj.nH+1),obj.C);
D2 = kron(speye(2*obj.nH+1),obj.M);

% Genearte B Matrices (two options)
% (option 1)
% B1 = [D1 hz; -eye(size(D1)) zeros(size(D1))];
% B2 = -[D2 zeros(size(D1)); zeros(size(D1)) eye(size(D1))];
% B = B1\B2;
% (option 2)
B = [-D2\D1 -D2\hz; speye([size(D1)]), sparse(size(D1,1),size(D1,2))];
B(isnan(B)) = 0;B(isinf(B)) = 0;
% Compute and Sorted Eigenvalues (sort by imaginary part and grab only half)
if isa(obj,'HBfem') || isa(obj,'HbFemThermal')
    nEvals = 2*obj.nDOF;
    nSubSpace = size(B,2) - 1; %obj.nH*obj.nDOF; 
%     nSubSpace = min([4*nEvals,(2*obj.nH+1)*obj.nDOF]);
    lams = eigs(B,nEvals,'smallestreal','MaxIterations',10,...
                'SubspaceDimension',nSubSpace,...
                'FailureTreatment','keep',...
                'Display',true,...
                'tolerance',1e-6);
%     if isempty(lam)
%         
%     end
    [~,index]=sort(imag(lams));
    lsorted = lams(index);
    lsorted2 = lsorted(1:2*obj.nDOF);
    Btil = diag(lsorted2);
else
    lams = eig(full(B));
    [~,index]=sort(imag(lams));
    lsorted = lams(index);
    lsorted2 = lsorted(1:2*obj.nDOF);
    Btil = diag(lsorted2);
end


% Store Floquet Data
% obj.FloqExp(:,obj.solpt) = diag(Btil);
% obj.stabilityCalc.stable(obj.solpt) = true; 

% Determine stability based on the real part
if any(real(lsorted2)>sqrt(eps))
    disp('---------------------------------------------------')
    warning('Positive Real Part of Floquet -> Unstable Solution')
    disp('---------------------------------------------------')
    obj.stabilityCalc.stable(obj.solpt) = false; 
end

end