function [c,ceq,gc,gceq] = MhbFminConFun(mhbObj,xC,varargin)
% This function is used as a nonlinear constraint function in conjunction
% with matlab's fmincon. The ideal usage is to ensure that a new set of
% solutions satifies the harmonic balance equation. 
%
% Input : 
%        obj = HB object
%        x0 = starting vector
% Ouput : 
%        h = vector of mhb residual
%        jMat = Jacobian matrix

% if an additional argument is provided then it is a scaling of the
% constraint function
if nargin > 2
    pFactor = varargin{1};
else
    pFactor = 1;
end

% Grab frequency and fourier coefficients
z0 = xC(1:end-1); w0 = xC(end);

% Evaluate harmonic balance equation
[h,dh] = mhbObj.Calc_h(z0,w0);

% Format output for fmincon
ceq = 1000*pFactor*(h'*h)/norm(z0); gceq = dh'*h/norm(z0);
% ceq = pFactor*h/norm(z0);
% gceq = dh';
disp(['MHB Residual = ',num2str(norm(h))]);
% No inequality constraints
c = []; gc = [];

end