function [f,j] = MhbMinDistFun(mhbObj,xC,zTarget,wTarget)
% This function is used to calculate new solutions based upon the previous
% while minimizing the distance between the new solution and the old. The
% ideal usages is with fmincon as an objective and Handle
%
% Input : 
%        obj = HB object
%        xC = [zC,wC] = current state variables
%        z0 = fourier coefficient
%        w0 = frequency
% Ouput : 
%        zF = solution
%        wF = solution
%        h = vector of mhb residual

% Initial coefficients from low harmonic solution
f = norm(xC-[zTarget;wTarget])^2;

% Gradient
j = 2*(xC-[zTarget;wTarget]);

end