function [f,df] = MhbCorrectionFsolveFun(obj,xIn)
% This function can be used as a handle within an optimization or root
% solving routine such a fsolve. It is meant to be used in conjuction with
% finding a solution about a fixed point, i.e. frequency, energy, etc.
% 
% Inputs : 
%       xIn = [zIn;wIn);
% Outputs :
%       h = harmonic balance residual
%       dh = harmonic balance gradient 

nVars = length(xIn);nFvars = nVars-1;

% Grab state variables
zC = xIn(1:nFvars/2);vC = xIn(nFvars/2+1:nFvars+1); wC = xIn(end);

% Calculate residual 
[h,dh] = obj.Calc_h(zC,wC); % Evaluate Residual Equation

% Output the function value
f = [h;0;dh*vC;0];

spZeros = sparse(size([dh;vC'],1),size([dh;vC'],2));
% Output the gradient 
df =[[dh;vC'],spZeros;spZeros,[dh;vC']];

% disp(['MHB Residual = ',num2str(norm(h))]);
end