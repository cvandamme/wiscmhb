function [fAmp,dfAmp] = Calc_HoldForce(obj,zt,wt)
% This function computes the force required to hold a specified nnm
% solution.
%
% Inputs:
%       obj = WiscMhb Object
%            - .HoldForce.f = shape of force
%            - .HoldForce.C = damping matrix
%       zt = fourier coefficients of solution
%       wt frequency of solution

% Outputs:
%       fAmp = amplitude of force required to hold nnm solution
%
% Get Period and time vectpr
T = 1/wt/2/pi; t = linspace(0,T,obj.HBcontrol.nT);

if isempty(obj.forceCalc.C)
    obj.forceCalc.C = obj.forceCalc.alpha*obj.M + ...
                      obj.forceCalc.beta*obj.K;
end
% Check if computing in time domain or frequency
if strcmp(obj.forceCalc.domain,'time')
    
    % Calculate the velocity along the period
    x_dot = obj.Calc_ZtoV(zt,wt,obj.HBcontrol.nT);
    
    % Dissipated power
    P_diss = diag(x_dot'*(obj.forceCalc.C)*x_dot);
    
    % Power put into
    P_in = diag(x_dot'*obj.forceCalc.fShape*sin(wt*t));
    
    % Integrate to get energy
    Ediss = trapz(t,P_diss);
    Ein = trapz(t,P_in);
    
    % Force amplitude
    fAmp = Ediss/Ein;
    
    % In time domain must use finite difference
    if nargout > 1
        dfAmp = []
    end
else
    
    % Initialize Variables and Indexs
    nabla_temp = zeros((1+2*obj.nH),(1+2*obj.nH));
    dnabladw_temp = zeros((1+2*obj.nH),(1+2*obj.nH));
    index1 = 2;index2 = 3;
    
    % Fourier transformations
    [~,FT] = obj.Calc_Gamma(wt);
    fintEXT = (obj.forceCalc.fShape*(sin(wt*t/obj.nu)))';
    fTilde = fintEXT(:);  b = FT*fTilde;
    
    % Loop Through to generate submatrices and assemble
    for ii = 1:obj.nH
        nabla_temp1 = [0 -(ii*wt/obj.nu); (ii*wt/obj.nu) 0];
        dnabladw_temp1 = [0 -(ii/obj.nu); (ii/obj.nu) 0];
        nabla(index1:index2,index1:index2)= nabla_temp1;
        dnabladw(index1:index2,index1:index2)= dnabladw_temp1;
        index1 = index2+1;  index2 = index1+1;
    end
        
    
    % Force in frequency domain
    fExt_f = zeros((2*obj.nH+1)*obj.nDOF,1); dFextdw = fExt_f;
    fExt_f(obj.nDOF+1:2*obj.nDOF) = -1*obj.forceCalc.fShape;
    dFextdw(2*obj.nDOF+1:3*obj.nDOF) = obj.forceCalc.fShape;
    fExt_f = b;
    
    % Energy terms
    eDiss_f = ((1/wt/2/pi)/2)*zt'*(kron(nabla'*nabla,obj.forceCalc.C))*zt;
    eIn_f = zt'*(kron(nabla',speye(obj.nDOF)))*fExt_f;
    
    % Force amplitude
    fAmp = eDiss_f/eIn_f;
    
    % Frequency domain gradient
    dfdz = (1/eIn_f)*zt.'*kron(obj.forceCalc.C,nabla.'*nabla) ...
        + (1/eIn_f)*zt.'*kron(nabla.'*nabla,obj.forceCalc.C) ....
        - (eDiss_f/eIn_f)*fExt_f.'*kron(speye(obj.nDOF),nabla);
    
    dfdw = (1/eIn_f)*(zt.'*kron(dnabladw.'*nabla,obj.forceCalc.C)*zt)...
        +(1/eIn_f)*(zt.'*kron(nabla.'*dnabladw,obj.forceCalc.C)*zt)....
        -(eDiss_f/eIn_f)*(zt.'*kron(dnabladw.',speye(obj.nDOF))*fExt_f);
        % + zt.'*kron(nabla.',speye(obj.nDOF))*dFextdw);
    
    dfAmp = [dfdz,dfdw];
end
end