function [obj] = Continue(obj)
% This is a arc-length based continuation routine to compute
% the NNMs of NLROMs or FEMs using the Harmonic Balance
% method. The continuation function will attempt to satisfy:
%
%            H(z,w) = A(w)*z - b(z) = 0
%
% H = continuation (harmonic balance) function
% A(w) = Linear Dynamic Matrix
% z = vector containing the fourier coefficients
%       (what will be solved for at each point)
% b = vector of fourier coefficients for external and nonlinear
%       forces Fext(w) - fint(w,z)




% Perform special operations near linear solution
if obj.solpt==1
    obj.HBcontrol.stp=obj.HBcontrol.stpint; % initial stepsize
    
    % Determine Initial Prediction based on type of analysis
    if obj.HBcontrol.NNMflag
%         obj.P(:,obj.solpt) = obj.HBcontrol.XnormScale*[obj.HBcontrol.z_int/max(abs(obj.HBcontrol.z_int)); 0];
        obj.P(:,obj.solpt) = [obj.HBcontrol.z_int/norm(obj.HBcontrol.z_int); 0];
    else    
        obj.P(:,obj.solpt) = [zeros(size(obj.HBcontrol.z_int)); 1]; % hard coded to step up in frequency.
    end
    
    % if its a thermal analysis
    if isa(obj,'HbFemThermal')
        z0 = zeros(size(obj.HBcontrol.z_int)); 
        z0(obj.nDOF+1:2*obj.nDOF) = obj.thermal.phi(obj.mf.freeDof,obj.HBcontrol.mode);
        obj.P(:,obj.solpt) = [z0/norm(z0); 0];
    end
    % Check if restart flag is used
    if isfield(obj.HBcontrol,'RestartFlag')
        if obj.HBcontrol.RestartFlag
            obj.P(:,obj.solpt) = obj.HBcontrol.XnormScale*[obj.HBcontrol.z_int/norm(obj.HBcontrol.z_int); obj.HBcontrol.wNormScale];
        end    
    end
    
    if obj.predControl.enforceEnergy && obj.E(obj.solpt) > obj.predControl.eVal
        [obj,E] = obj.EnforceEnergy(obj.HBcontrol.z_int,...
                                    obj.HBcontrol.w_int);
        if E(1) < E(2) 
            obj.P(:,obj.solpt) = -1*obj.P(:,obj.solpt);
            obj.HBcontrol.w_int = obj.HBcontrol.w_int - obj.HBcontrol.stp*obj.P(end,obj.solpt);
            obj.HBcontrol.z_int  = obj.HBcontrol.z_int  - obj.HBcontrol.stp*obj.P(1:end-1,obj.solpt);
        end
    else
%         [obj,Zpr,Wpr] = obj.Predict([],[]);
    end
    
    % Evaluate Initial guess (check it is actually a solution)
    [Ht,dh] = obj.Calc_h(obj.HBcontrol.z_int,obj.HBcontrol.w_int);
    if norm(Ht) < obj.HBcontrol.tol % /norm(obj.HBcontrol.z_int)
        if ~obj.HBcontrol.silent
            fprintf('Initial Condition Guess is Correct  \n')
        end
        obj.HBcontrol.numks(obj.solpt) = 0;
        % Save Converged Values
        obj.z(:,obj.solpt) = obj.HBcontrol.z_int; % Fourier Coefficients
        obj.w(obj.solpt)   = obj.HBcontrol.w_int; % Frequency Values
        obj.x(:,obj.solpt) = obj.Calc_ZtoX(obj.z(:,obj.solpt),obj.w(obj.solpt));    % Calculate Modal Amplitudes
        obj.v(:,obj.solpt) = obj.Calc_ZtoV(obj.z(:,obj.solpt),obj.w(obj.solpt));    % Calculate Modal Amplitudes
        obj.E(obj.solpt) = obj.Calc_Energy(obj.x(:,obj.solpt),obj.v(:,obj.solpt));    % Calculate Energy
        Jz = []; hw = [];
    else
        if ~obj.HBcontrol.silent
            fprintf('Initial Conditions Guess is Incorrect -> Starting Correction  \n')
            fprintf(['Residual = ',num2str(norm(Ht)/norm(obj.HBcontrol.z_int)),' \n'])
        end
        [obj,Jz,hw] = obj.Correct(Ht,obj.HBcontrol.z_int,obj.HBcontrol.w_int);
    end
    
    
    % Store Solution Counter Info
    obj.HBcontrol.endcont = 1;
    obj.HBcontrol.stps(obj.solpt) = obj.HBcontrol.stp; % vector to store step sizes
    
    
    % Check if Freq limits exceeded
    if obj.w(obj.solpt) < obj.stopCriteria.frequency(1)
        fprintf('Frequency Boundary Exceeded --> Ending Continuation  \n')
        obj.stopCriteria.criteria = 'frequency_lb';
        obj.HBcontrol.endcont = 0;
    elseif obj.w(obj.solpt) > obj.stopCriteria.frequency(2)
        fprintf('Frequency Boundary Exceeded --> Ending Continuation  \n')
        obj.stopCriteria.criteria = 'frequency_ub';
        obj.HBcontrol.endcont = 0;
    end
    
    if norm(obj.z(:,obj.solpt)) == 0
        warning('zero norm solution found')
        obj.z(:,obj.solpt) = obj.HBcontrol.z_int*(1+obj.HBcontrol.stp); % Fourier Coefficients
        obj.w(obj.solpt)   = obj.HBcontrol.w_int; % Frequency Values
    end
    obj.solpt = obj.solpt + 1;
else
    Jz = []; hw = [];
end

  
while obj.HBcontrol.endcont ~= 0 % Continue until certain conditions are broken
    
    
    % If enforcing energy it ensures that continatuion will always result
    % in an increase in energy
    if obj.predControl.enforceEnergy && obj.solpt > 0
        [obj,Zpr,Wpr] = obj.Predict(Jz,hw);
        [obj,E] = obj.EnforceEnergy(obj.z(:,obj.solpt-1),...
                                          obj.w(obj.solpt-1));
        if E(1) <= E(2) 
            obj.P(:,obj.solpt) = -1*obj.P(:,obj.solpt);
            Wpr = obj.w(obj.solpt-1) + obj.HBcontrol.stp*obj.P(end,obj.solpt);
            Zpr = obj.z(:,obj.solpt-1) + obj.HBcontrol.stp*obj.P(1:end-1,obj.solpt);
        end
%     elseif obj.solpt > 2
        
    else
        [obj,Zpr,Wpr] = obj.Predict(Jz,hw);
    end
    
    % Check Residual of Prediction
    tStart = tic;
    [Hpr,dh] = obj.Calc_h(Zpr,Wpr);
    obj.timings.FunEval = [obj.timings.FunEval toc(tStart)];
    
    % Check residual is NAN (set to large number)
    if isnan(Hpr); Hpr = 1e5; end 
    
    % If tolerance accepted save else correct until convergence
    if norm(Hpr)/norm(Zpr) < obj.HBcontrol.tol
        if ~obj.HBcontrol.silent
            fprintf('Initial Prediction Meets Tolerance  \n')
            fprintf(['Residual = ',num2str(norm(Hpr)),'  \n'])
        end
        
        obj.HBcontrol.numks(obj.solpt) = 1;
        
        % Save Converged Values
        obj.z(:,obj.solpt) = Zpr; obj.w(obj.solpt) = Wpr;
        [obj.x(:,obj.solpt)] = obj.Calc_ZtoX(Zpr,Wpr);
        [obj.v(:,obj.solpt)] = obj.Calc_ZtoV(Zpr,Wpr);
        [obj.E(:,obj.solpt)] = obj.Calc_Energy(obj.x(:,obj.solpt),obj.v(:,obj.solpt));
        
        % Clear Prediction Values
        clear Hpr
    else
        if ~obj.HBcontrol.silent
            fprintf('Initial Prediction Does NOT Meets Tolerance --> Start Corrector  \n')
            fprintf(['Residual = ',num2str(norm(Hpr)/norm(Zpr)),' \n'])
        end
        [obj,Jz,hw] = obj.Correct(Hpr,Zpr,Wpr,dh);
    end
    
    
    if norm(obj.z(:,obj.solpt)) == 0
%         warning('zero norm solution found')
        obj.z(:,obj.solpt) = obj.z(:,obj.solpt-1)*(1+1e6*eps); % Fourier Coefficients
        obj.w(obj.solpt)   = obj.w(obj.solpt-1); % Frequency Values
    end
    
    % Calculate Energy
    [obj.E(obj.solpt)] = obj.Calc_Energy(obj.x(:,obj.solpt),obj.v(:,obj.solpt));
    
    % Update solution counters
    obj.HBcontrol.stps(obj.solpt) = obj.HBcontrol.stp; % vector to store step sizes
    obj.HBcontrol.arcLength(obj.solpt) = sum(obj.HBcontrol.stps);
    
    % Evaluate if Stability Point is present
    if obj.stabilityCalc.logic && mod(obj.solpt,obj.stabilityCalc.rate) == 0
        [obj] = obj.Calc_Stability(obj.w(obj.solpt),Jz);
    end
    
    % Bifurcation Detection (currently is useless because we need a tighter
    % step-size control around bifurcation points) 
    if obj.bifurCalc.logic && mod(obj.solpt,obj.bifurCalc.rate) == 0
        [obj] = obj.Calc_Bifurcation(Jz,hw);
    end
    
    % Calculate force required to hold nnm solution
    if obj.forceCalc.logic
        obj.forceCalc.fAmp(obj.solpt) = obj.Calc_HoldForce(...
                        obj.z(:,obj.solpt),obj.w(obj.solpt));
    end

    % output info
    if ~obj.HBcontrol.silent
        fprintf(['Frequency = ',num2str(obj.w(obj.solpt)/2/pi),' Hz, '])
        fprintf(['norm(X) = ',num2str(norm(obj.x(:,obj.solpt))),' \n'])
    end
    
    % Check ending critia
    obj = obj.CheckStopping();
    
    % If stopped check if interpolation is desired
    if ~obj.HBcontrol.endcont
        if obj.stopCriteria.interpFinal
            obj = obj.InterpolateFinalSol();
        end
        if obj.HBcontrol.start == 1
            WiscMHBGUI('stopCONT')
        end
    else
        % Plot Solution
        if obj.HBcontrol.start == 1
            WiscMHBGUI('plotSOL')
        end
        
        % Add to solution count
        obj.solpt = obj.solpt + 1;
    end
    


end
end