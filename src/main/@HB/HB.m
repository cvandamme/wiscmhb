classdef HB < handle
    % This class is used to implement a form of the hamrnoic balance with a
    % continuation algorithm to compute periodic orbits of nonlinear
    % dynamical systems. 
    %
    % Systems supported : 
    %       1) Geometrically nonlinear finite element models (HBfem)
    %       2) Geometrically nonlinear reduced order models (HbRom or
    %          HBnlrom)
    %       3) Iwan models for modeling nonlinearity in joints (HbIwan)
    %       4) Spring-mass systems with discrete nonlinearities (HbSm)
    %
    % The primary output is a vector of harmonic amplitudes describing the
    % solution at each frequency:
    %
    % HB.w = frequency vector (rad/s)
    % HB.z = Harmonic amplitudes: [c0,s1,c1,s2,c2,s3,c3,...].' at each
    %       frequency, where c_j is an Nx1 vector for an N DOF system.
    %
    
    properties (SetAccess = 'public', GetAccess = 'public')
        % ----  Model Parameters ----
        Name    % Class Name (This is what the info will be saved as)
        SAVEdir % This where the class will be saved
        % ----  Linear Properties ----
        nDOF  % Number of DOF (This is either # of FEM free DOF or # modal DOF)
        K     % Linear Stiffness : [nDOF, nDOF]
        M     % Linear Mass      : [nDOF, nDOF]
        C     % Linear Damping   : [nDOF, nDOF]
        % ---- Physical Domain Solution Point Properties ---- 
        x     % Time Domain Displacement     : [nDOF, nSOLS] 
        v     % Time Domain Velocity         : [nDOF, nSOLS]
        w     % Solution Frequency (rad/sec) : [nSOLS, 1]
        E     % Solution Energy              : [nSOLS, 1]
        % ---- Forcing Paremeters ----
        Fext  % Force Amplitude              : [nDOF, 1] 
        
        % ----  Frequency Domain Solution Parameters ----
        nH    % Number of Harmonics    : [scalar]
        z     % Fourier coefficients   : [(2*nH+1)*nDOF,nSOLS] 
        nu    % SubHamornic Multiplier : [scalar]
        solpt % Current Solution Point : [scalar]

        % ---- Solution Parameters ---- 
        P         % Prediction Vector        : [nDOF*(2*nH+1)+1,nSOLs]
        Pstatus = struct('Angle',[],'Dot',[])  % Prediction Vector Status (angle, dot, turn)
        timings = struct('LinSolve',[],'FunEval',[],'JacEval',[],'NNM',[]);
        forceCalc = struct('logic',false,'fShape',[],'fAmp',[],'C',[]);
        stabilityCalc = struct('logic',false,'rate',10,'stable',[],'floqExp',[]);
        bifurCalc = struct('logic',false,'Status',[],'foldTest1',[],...
                            'foldTest2',[],'hopfTest',[],'neimarkTest',[])
        stopCriteria = struct('frequency',[0 1e8],...
                              'amplitude',[],...
                              'force',[],'energy',inf,...
                              'interpFinal',false,'criteria',[]);
        predControl = struct('type','tangent','order',1,...
                             'maxOrder',5,'betaMin',0,...
                             'enforceEnergy',false,'eVal',1e-4);
        corrControl = struct('solver','fsolve');
        stepControl = struct('stpint',1e-2,'stpmax',100,'stpmin',1e-8,...
                             'optiter',3,'stpdf',3,'maxiter',10);
        % Structure Containing Control Parameters (Default)
        HBcontrol = struct('stpSign',1,'stpint',1e-4,'stpmax',100,'stpmin',1e-8,...
                           'optiter',3,'stpdf',3,'maxiter',10,...
                           'tol',1e-4,'nSols',200,'nT',100,...
                           'LinSolve','Direct','nbstep',1000,...
                           'divfactor',10,'freq','Hz',...
                           'start',true,'save',true,'silent',true,...
                           'startFlag','false','mode',1,...
                           'z_int',[],'w_int',[],'xScale',1e-4,...
                           'endcont',1,'defaultStpSign',1,'defaultX_int',[],'defaultStpint',[]);
                       
        % ---- GUI Information ---- 
        GUI      % GUI Control Options
        parallelOpts = struct('logic',false,'numWorkers',4);
    end
    properties (SetAccess = 'private', GetAccess = 'private')
    end
    methods (Access = 'public')
        % ------ Initialization, Control and Plotting
        function [obj] = HB(Name,SAVEdir,M,C,K)
            % This function intitializes the class and 
            % stores the dynamical system data
            
            % System Properties
            obj.Name = Name;
            obj.SAVEdir = SAVEdir; 
            
            % Check if Save Dir Exists
            if ~exist(SAVEdir,'dir')
                warning('Provided Save Directory Does Not Exist - Please Select A New Directory')
            end
           
            % Store Linear Data
            obj.M = M; obj.K = K; obj.C = C;
            
            % Obtain Number of DOF
            [~,obj.nDOF] = size(M);
            
            
        end
        
        % -- Set Functions --
        [obj] = SetPhase(obj,phases)
        [obj] = SetMainControls(obj,controls)

        % ------ Continuation Rountine Functions
        [obj] = Start(obj,nH,nu,Fext,z0,w0,HBcontrol)
        [obj] = Continue(obj)
        [obj,Zpr,Wpr] = Predict(obj,Jz,hw)
        [obj,dhdz,dhdw] = Correct(obj,Hpr,Zpr,Wpr,varargin)
        [obj,correct] = SolveLinearSystem(obj,A,b)
        
        % ------ Stability/Bifurcation Calculations -------
        Btil = Calc_Stability(obj,wt,hz)
        [obj] = Calc_Bifurcation(obj,hz,hw)
        [fold] = HB2FOLD(obj)
        [BP] = HB2BP(obj)
        
        % ------ State Conditions --------
        [Ht,varargout] = Calc_h(obj,zt,wt)
        [A]  = Calc_A(obj,wt)
        [Jz] = Calc_dhdz(obj,zt,wt)
        [Jw] = Calc_dhdw(obj,wt)
        [Jknl] = Calc_dKnldz(obj,zt,wt)
        [e,de] = Calc_KineticEnergy(obj,zt,wt)
        % ------ Transformations  --------
        [IFT,FT] = Calc_Gamma(obj,wt)
        [IFT,FT] = Calc_dGammadw(obj,wt)
        [Xt] = Calc_ZtoX(obj,zt,wt,varargin)
        [Vt] = Calc_ZtoV(obj,zt,wt,varargin)
        
        % ------- Added Numerical Computations --------
        [obj] = Calc_StepSize(obj)
        
        % ------- Administrative Stuff 
        Save(obj)
        obj = SetName(obj,newName)
        obj = SetSaveDir(obj,newSavedir)
        
        % Holding force calculation
        [fAmp,dfAmp] = Calc_HoldForce(obj,zt,wt)
        obj = SetHoldForce(obj,fHold)
        obj = SetHoldDamping(obj,C)
        
        % Interpolate for answer at desired state
        obj = InterpolateFinalSol(obj,w1,w2)
        
        % Checks whether to stop harmonic balance
        obj = CheckStopping(obj)
        [f,j] = MhbMinDistFun(obj,xC,zTarget,wTarget)
    end
    
    % ----- Abstract Methods that must be implemented in Subclasses --- %
    % Subclasses :
    %       1) FEM (HBfem or HbFemThermal)
    %       2) Rom - IcRom, EdRom, MdRom, SmRom (HbRom or HBnlrom)
    %       3) Iwan model (HbIwan)
    %       4) Spring-Mass (HbSm)
    methods (Abstract)
        Calc_f(obj,xdt)      % Calculate internal force (optional output jacobian)
        Calc_dfdx(obj,xdt)   % Calculate tangent stiffness
        Calc_Energy(obj,xdt) % Calculate potential energy
        Calc_b(obj,zt,wt)    % Calculate nonlinear force in frequency domain
        Calc_Periodicity(obj,nSteps,varargin) % Integrate system and calculate periodicity
    end
end
