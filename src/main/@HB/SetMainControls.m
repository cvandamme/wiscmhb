function [obj] = SetMainControls(obj,varargin)
% This function sets the main controls for the continuation routine
% 
% Inputs : 
%          obj = HB object
%          varargin = name-value pairs of controls 
% Output :
%         obj = HB object (with updates controls)

% Check that the number of arguments are correct
if nargin == 2 && isstruct(varargin{1})
    
    % Grab input and fieldnames
    controlIn = varargin{1};  controlFields = fieldnames(controlIn);
    
    % Loop through the controls to see if they are present
    for ii = 1:length(controlFields)
       if isfield(obj.HBcontrol,controlFields{ii})
           obj.HBcontrol.(controlFields{ii}) = controlIn.(controlFields{ii});
           disp(['Field : ' ,controlFields{ii}, ' changed']);
       else
           warning(['Field provided : ' controlFields{ii}, ' not found']);
       end
    end 
elseif mod(nargin,2) == 0 
    
    % Reshape the arguments for ease
    controlIn = reshape(varargin,2,[])';

    % Loop through the controls to see if they are present
    for ii = 1:size(controlIn,1)
       if isfield(obj.HBcontrol,controlIn{ii,1})
%            disp(['Field : ' controlIn{ii,1}, ' changed']);
           obj.HBcontrol.controlIn{ii,1} = controlIn{ii,2};
       else
%            warning(['Field provided : ' controlIn{ii,1}, ' not found']);
       end
    end
else
    error('Uneven number of arguements found ... must be name-value pairs')
end


end

