function [e,de] = Calc_KineticEnergy(obj,zC,wC)
% This function is used to calculate the kinetic energy of the solution.
% This will compute the same energy as the potential energy formulation
% within the abstract classed except that this provided gradients with
% respect to both the fourier coefficients and frequncy. 

% 
Q = kron([1/sqrt(2) repmat([1 1],1,obj.nH)],speye(obj.nDOF));

% Compute "frequency mass"
mBar = Q'*obj.M*Q;

% Compute energy
e = 0.5*(wC^2)*zC'*mBar*zC;

% Compute gradient wrt to fourier coefficients and frequency
de = [(wC^2)*mBar*zC;wC*zC'*mBar*zC];

% xC = obj.Calc_ZtoX(zC);

end