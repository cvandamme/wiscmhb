function [h,dh] = MhbFsolveFun(obj,xIn)
% This function can be used as a handle within an optimization or root
% solving routine such a fsolve. It is meant to be used in conjuction with
% finding a solution about a fixed point, i.e. frequency, energy, etc.
% 
% Inputs : 
%       xIn = [zIn;wIn);
% Outputs :
%       h = harmonic balance residual
%       dh = harmonic balance gradient 

% Grab state variables
zC = xIn(1:end-1); wC = xIn(end);

% Calculate residual 
[h,dh] = obj.Calc_h(zC,wC); % Evaluate Residual Equation

% disp(['MHB Residual = ',num2str(norm(h))]);
end