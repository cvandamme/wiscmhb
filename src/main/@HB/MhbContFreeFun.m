function [zF,wF] = MhbContFreeFun(mhbObj,z0,w0)
% This function is used to calculate new solutions based upon the previous
% while minimizing the distance between the new solution and previous.
% Ideally this will serve a a continuation free approach for model updating
% once an intial branch is computed.
%
% Input : 
%        mhbObj = HB object
%        z0 = fourier coefficient of solution that the new solutions should
%               be close to
%        w0 = frequency of solution that the new solutions should
%               be close to
% Ouput : 
%        zF = new solution
%        wF = new solution
%        h = vector of mhb residual


% Setup minimization function
minFun = @(xC) mhbObj.MhbMinDistFun(xC,z0,w0);

% Setup constraint function
conFun = @(xC) mhbObj.MhbFminConFun(xC);

% Call the Function to call fmincon 
options=optimoptions(@fmincon,...
                  'SpecifyObjectiveGradient',true,...
                  'SpecifyConstraintGradient',true,...
                  'CheckGradients',false,...
                  'Display','iter-detailed',...
                  'FunctionTolerance',1e-6,...
                  'OptimalityTolerance',1e-6,...
                  'RelLineSrchBnd',10,...
                  'Algorithm','interior-point',... 
                  'StepTolerance',1e-12,...
                  'MaxFunctionEvaluations',2000);
              
% Assign constraint function
bounds = [];

% Create initial guess
zStart = z0; wStart = w0;

% Call "fmincon" to find state variable update dD by minimizing J
[xF,jVal,exitFlag,OUTPUT,lambda,djVal]=fmincon(minFun,[zStart;wStart],...
                                      [],[],[],[],[],[],conFun,options);

% Grab frequency and fourier coefficients
zF = xF(1:end-1); wF = xF(end);


end