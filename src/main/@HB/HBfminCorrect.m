function [obj,Jz,hw] = HBfminCorrect(obj,Hpr,Zpr,Wpr)
% This function performs the correction portion of the
% predictor-corrector algorithm. Used basic newton update
% scheme with Moore-Penrose Option. Only initiatied if the intial
% guess is > allowed tolerance
% Inputs:
%           obj = Class Object
%           Hpr = Residual of Predicted State Variables
%           Zpr = Predicted Fourier Coefficient
%           Wpr = Predicted Frequency
% Outputs:
%           obj = Updated Class Object
%           Jz  = Fourier Coefficient Jacobian
%           hw  = 

% Bring in previous solution values current solution values 
% this still works for the new optimization scheme
if obj.solpt == 1
    z0 = Zpr; zC = z0;
    w0 = Wpr; wC = w0;
    y0 = [z0;w0]; yC = y0;
    H0 = Hpr; HC = H0;
    v0 = obj.P(:,obj.solpt); vC = v0;
else
    z0 = obj.z(:,obj.solpt-1); zC = Zpr;
    w0 = obj.w(obj.solpt-1); wC = Wpr;
    y0 = [z0;w0]; yC = [zC;wC];
    H0 = Hpr; HC = H0;
    v0 = obj.P(:,obj.solpt); vC = v0;
end

% Assemble new state vector [z1;w1;zF;wF]
y0S = [Zpr;Wpr]; 
    
% Set options
% options = optimoptions('fmincon','SpecifyConstraintGradient',true);
options = optimoptions('fgoalattain','SpecifyObjectiveGradient',true);

% Optimization Fun
optFun = @(yS)obj.harmonicOptFun(yS); 

% Nonlcon Function 
contraintFun = @(yS)obj.harmonicConstraintFun(yS); 

% Linear Constraints
A = []; Aeq = []; 
b = []; beq = [];

% Bounds (based on stepsize maybe?)
lb = y0 - stpBounds;
ub = y0 + stpBounds;

% Perform optimzation using fmincon
[x,fval,exitflag,output,lambda,grad,hessian] = fmincon(optFun,y0,A,b,Aeq,beq,lb,ub,contraintFun,options);

% 
% --- Start Correction Loop --- %
k=0; % number of iterations
Jz = []; hw = [];

innerIter = 0;
while norm(HC)/norm(zC) > obj.HBcontrol.tol % && norm(correct1)*obj.HBcontrol.stp > obj.HBcontrol.tol
    
    % --- Generate Update Matrices ---
    [Jz] = obj.calcJz(zC,wC);
    [Jw] = obj.calcJw(wC);
    hw = Jw*zC;
    
    % State Variables Update
    Gy = [Jz hw ; vC'];
    G  = [HC ; 0];
    correct1 = Gy\G;

    % Prediction Update
    R = [[Jz hw]*vC; 0];
    correct2 = Gy\R;
    correct2 = correct2/norm(correct2); % Need to normalize before correcting ?????
    
    % ------- Update Values ---------
    yC = yC - correct1; % State Variables
    vC = vC - correct2; % Prediction Vector
    vC = vC/norm(vC);   % Renormalize Again ????
    zC = yC(1:end-1);   % Fourier Coefficients
    wC = yC(end);       % Frequency
    
    % --- Check Residual --- %
    [HC] = obj.calcH(zC,wC); % Evaluate Residual Equation
    k=k+1; % Update Iteration Count
%     fprintf(['Iteration: ',num2str(k),' -> Residual = ',num2str(norm(HC)/norm(zC)), ' \n'])
%     fprintf(['Iteration: ',num2str(k),' -> deltaX = ',num2str(norm(correct1))])

    % ------- IF CONVERGENCE NOT MET ----------- %
    % if  k > obj.HBcontrol.optiter || norm(HC)/norm(zC) > norm(H0)/norm(z0) % MSA REplaced - use maxiter here)
    if  k > obj.HBcontrol.maxiter || norm(HC)/norm(zC) > (norm(H0)/norm(z0))*obj.HBcontrol.divfactor
        innerIter = innerIter + 1;
        fprintf('Too many attempts or residue is increasing -- prediction must be bad -- tisk tisk  \n')
        if k >= obj.HBcontrol.maxiter % optiter
            fprintf('Too many iterations --- Reducing Step Size  \n ');
        elseif norm(HC)/norm(zC) > norm(H0)/norm(z0)*obj.HBcontrol.divfactor
            fprintf('Residue is increasing --- Reducing Step Siz  \ne');
        end
        
        % Reduce stepsize of predictor if convergence isn't met
        obj.HBcontrol.stp = obj.HBcontrol.stp/obj.HBcontrol.stpdf;
        if abs(obj.HBcontrol.stp) < obj.HBcontrol.stpmin
%             obj.HBcontrol.endcont = 0;
            obj.HBcontrol.stp = obj.HBcontrol.stpmin;
            fprintf('Minimum step size reached, algorithm terminating  \n');
        end
        
        % Exit Continuation
        if obj.HBcontrol.endcont == 0
            break
        end
        
%         fprintf(['New Stepsize: ',num2str(obj.HBcontrol.stp),'  \n']);
        
        if k > obj.HBcontrol.maxiter
            fprintf('Performing Random Perturbation Step \n');
            randPert = rand((2*obj.nH+1)*obj.nDOF,1);
            randPert = randPert/norm(randPert);
            randW = rand(1,1);
            
            % Re-Evaluate predictions
            zC = z0 + obj.HBcontrol.stp*randPert.*v0(1:end-1);
            wC = w0 + obj.HBcontrol.stp*randW.*v0(end);
            vC= v0;
%             k = 0;
        else
            % Re-Evaluate predictions
            zC = z0 + obj.HBcontrol.stp*v0(1:end-1);
            wC = w0 + obj.HBcontrol.stp*v0(end);
            vC= v0;
        end
        
        % --- Evaluate the New Prediction --- %
        [Hpr] = obj.calcH(zC,wC);
        H0 = HC;
        HC = Hpr;
        yC = [zC;wC];
        
    end
end


[xC] = obj.calcZtoX(zC,wC);

% Save Converged Values
obj.w(obj.solpt) = wC;
obj.x(:,obj.solpt) = xC;
obj.z(:,obj.solpt) = zC;
obj.P(:,obj.solpt) = vC;

% Store Continuation Info
obj.HBcontrol.numks(obj.solpt) = k;
obj.HBcontrol.stps(obj.solpt) = obj.HBcontrol.stp;

end