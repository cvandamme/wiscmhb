function [IFT,FT] = Calc_Gammatemp(obj,wt)
% This function computes the GAMMA operator. The GAMMA operator
% is essentially a vecotrized form of the fourier
% transformation. Computation requires
%
% Inputs:
%       obj = contains the following
%           .nDOF = number of degrees of freedom
%           .nH = number of harmonics
%           .nT = number of time samples
%           .T = Current Solution period of oscillation
%           .nu = subharmonic mulitplier
%       wt = current solution value
%
% Outputs:
%       FT = Gamma operator
%       iFT = Inverse GAMMA operator
%

% Extract time sample rate (future maybe variable??)
nT = obj.HBcontrol.nT;

% Generate Time Sequence
% t = linspace(0,2*pi/wt,nT);
t = (0:nT-1)*((2*pi/wt))/nT;

% Identity Multiplier
In = speye(obj.nDOF);
zHv = ones(nT,1);
zHm = kron(In,zHv);

% Generate Sine, Cosine Vectors and Generate GAMMA Matrix
% (Kinda Slow -could improve) want to use --> S = sparse(i,j,v,m,n)
index = 1;
tempSC = cell(1,2*obj.nH);
Q = zeros(nT,obj.nH+1);
Q(:,1) = zHv;
for ii = 1:obj.nH
    Q(:,index+1) = sin(ii*wt.*t/obj.nu)';
    tempSC{index} = kron(In,sin(ii*wt.*t/obj.nu))'; index = index + 1;
    Q(:,index+1) = cos(ii*wt.*t/obj.nu)';
    tempSC{index} = kron(In,cos(ii*wt.*t/obj.nu))'; index = index + 1;
end
SC = sparse(cell2mat(tempSC));

% Generate FT and IFT
IFT = horzcat([zHm SC]);
clear SC zHm

% Compute pseudo inverse (still working on efficient work around)
if obj.nDOF > 4
    
    % Trick version .. still need to derive
    qPinv = pinv(Q); % independent on the number of dof!!!!
    ftCell = cell(2*obj.nH+1,1);
    for ii = 1:size(Q,2)
        % Think this kronecker can be replaced by faster implementation
        % simply bldiag(qPinv,qPinv,...)
        ftCell{ii} = kron(In,qPinv(ii,:));
    end
    FT = vertcat(cell2mat(ftCell));
    
    
else 
    FT = pinv(full(IFT));
    FT = sparse(FT);
end



% Check to make sure they are the right size
if size(IFT) ~= [obj.nDOF*nT,obj.nDOF*(2*obj.nH+1)]
    error('Inverse FT is incorrect size')
end
if size(FT) ~= [obj.nDOF*(2*obj.nH+1),obj.nDOF*nT]
    error('FT is incorrect size')
end




end