function [Jz] = Calc_dhdz(obj,zt,wt)
% Computes the jacobian of the external focing and nonlinear
% term fourier coefficient matrix wrt to the fourier
% coefficients
%
% Inputs:
%       x = time dependent displacements
%       A = Linear Dynamics Matrix
%       FT = Fourier Tranformation
%       IFT = Inverse Fourier Tranformation
%       dF = derivative of the forces wrt to displacements
% Outputs:
%       Jz = jacobian of H wrt to fourier coefficients
%


% Calculate the Tranformation Matrices (option if required)
[IFT,FT] = obj.Calc_Gamma(wt);

% Compute gamma for velocity 
[dIFTdw,~] = obj.Calc_dGammadw(wt);

% Tranform to Physical Domain
xdt = IFT*zt; vdt = dIFTdw*zt;

% Calcualte Linear Dynamics
[A] = obj.Calc_A(wt);

if isa(obj,'HbFlutterSdof')
    % Calculate Internal Force Gradient (Tangent Stiffness)
    [dFdX] = obj.Calc_dfdx(vdt);
    
    % Calculatee the Internal Force Gradient wrt fourier coefficients
    Jz = A - FT*dFdX*dIFTdw;
    
elseif isa(obj,'HBIwan')
    % Get the amplitude of the 1st harmonic
    s1 = zt(2,1);
    c1 = zt(3,1);
    amp = sqrt(s1^2 + c1^2);
    
    % Calculate the gradient of the internal force with respect to displacement 
    [dFdX] = obj.Calc_dfdx(amp);
    
    % Calculate the gradient of the internal force with respect to velocity
    [dFdV] = obj.Calc_dfdv(amp);
    
    % Calculate the gradient of the internal force wrt fourier coefficients
    Jz = A - FT*(dFdX*IFT+dFdV*obj.Calc_dGammadw(wt));
    
else
    
    % Calculate Internal Force Gradient (Tangent Stiffness)
    [dFdX] = obj.Calc_dfdx(xdt);
    
    % Calculatee the Internal Force Gradient wrt fourier coefficients
    Jz = A - FT*dFdX*IFT;
end





end