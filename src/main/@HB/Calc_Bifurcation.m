function [obj] = Calc_Bifurcation(obj,hz,hw)
% This script detects and identifies the bifurcations of the system. 
% 
% Inputs :
%           obj = Harmonic balance object
%           hz = jacobian with respect to the fourier coefficients
%           hw = jacobian with respect to the frequency 
% Outputs :
%           BIFUR = Bifurcation Identification
%                   = 1 if FOLD Bifucation is detected
%                   = 2 if Branch Point Bifurcation is detected
%                   = 3 if Neimark Schamker Bifurcation is detected
%           Test = Output Test Functions (not implemented yet)


status = 0; 

% Check if sign changes (limit point / fold bifurcation)
test1a = sign(obj.P(end,obj.solpt)) ~= sign(obj.P(end,obj.solpt-1));
if test1a
    disp('FOLD BIFURCATION DETECTED : ')
    disp('--------------------------------------------------------')
    disp('The sign on the prediction vector for frequency switched')
    disp('--------------------------------------------------------')
    pause(1); status = 1; 
end

% Check Harmonic Jacobian wrt fourier coefficients is singular (limit point / fold bifurcation)
% test1b = det(hz);
% if abs(test1b) < 1e-6 % Need to find a good number or a way to reduce stepsize
%     disp('FOLD BIFURCATION DETECTED : ')
%     disp('------------------------------------------------------')
%     disp('Harmonic jacobian wrt fourier coefficients is singular')
%     disp('------------------------------------------------------')
%     pause(1); status = 1;
% end

% Check for branch point bifurcation
test2 = det([hz hw; obj.P(:,obj.solpt-1)']);
if abs(test2) < 1e-6 % Need to find a good number or a way to reduce stepsize
    disp('BRANCH POINT DETECTED : ')
    disp('------------------------------------------------------')
    disp('The determinant of the Jacobian is zero')
    disp('------------------------------------------------------')
    pause(1); status = 2;
end

% Store data
obj.bifurCalc.status(obj.solpt) = status;
obj.bifurCalc.foldTest1(obj.solpt) = test1a;
% obj.bifurCalc.foldTest2(obj.solpt) = test1b;
obj.bifurCalc.hopfTest(obj.solpt) = test2;

% Try a basic step-size reduction technique based on linear distance from 
% bifurcation values w/ a limiting threshold




end