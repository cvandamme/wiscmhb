function [vOut] = Calc_ZtoV(obj,zt,wt,varargin)
% This function converts the fourier coefficients to the modal
% velocities using the transformation. 
%               dxdt = Q*NABLA*z 
% Inputs :
%           obj
%           zt = current solution fourier coefficients
%           wt = current solution frequency (rad/sec)
% Output :
%           vt = physical/modal amplitude across the entire period


% If additional argument is supplied are input then the 
if nargin < 4
    t = 0; nPts = 1;
else
    % Create time point of interest
    nPts = varargin{1};  t = linspace(0,2*pi/wt,nPts); 
    
    % Don't take last point because it (if a harmonic solution) is 
    % the same as the first
    if nPts == 1
        t = 0;
    else
%         nPts = nPts -1; t = t(1:end-1);
    end
end

% Identity Multiplier
In = speye(obj.nDOF); zHm = kron(In,zeros(nPts,1));

% Generate Sine, Cosine Vectors and Generate GAMMA Matrix
% (Kinda Slow -could improve) want to use --> S = sparse(i,j,v,m,n)
index = 1; Q = zeros(nPts,obj.nH+1); %Q(:,1) = zHv;
for ii = 1:obj.nH
    Q(:,index+1) = (ii*wt/obj.nu)*cos(ii*wt.*t/obj.nu)'; index = index + 1;
    Q(:,index+1) = -(ii*wt/obj.nu)*sin(ii*wt.*t/obj.nu)'; index = index + 1;
end
% SC = sparse(cell2mat(tempSC));

% Transform to time domain
vt = kron(Q,In)*zt;
vOut = reshape(vt,obj.nDOF,[]);

% % Generate FT and IFT
% dIFTdw = horzcat([zHm SC]);
% 
% % Tranform to Modal Domain
% Vt = dIFTdw*zt;
% 
% % Rearrange to nDof x nT
% index1 = 1; index2 = nT;
% vt = zeros(obj.nDOF,nT); 
% for ii = 1:obj.nDOF
%     vt(ii,:) = Vt(index1:index2);
%     index1 = index1+nT;index2 = index2+nT;
% end
% 
% if nargin > 3
%     dof = varargin{1};
% else
%     dof = obj.plotOpts.dof2plot;
% end
% 
% % Grab the maximum one based on a specific dof #
% [maxVal,maxId] = max(abs(vt(dof,:)));
% 
% % Ouput all dof at that instance in time
% vOut = vt(:,maxId);

end