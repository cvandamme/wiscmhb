function [obj,Zpr,Wpr] = Predict(obj,Jz,hw)
% This function performs the prediction portion of the
% predictor corrector algorithm
% Requires:
%     Pold = Previous Solution Prediction Vector(or correction?)
%     Zold = Previous Solution Fourier Coffients
%     Jz = Previous Solution Jacobian wrt z
%     Jw = Previous Solution Jacobian wrt z


% --- Extract Previous Step Info if Not Present --- %
if isempty(Jz)
    % **** (Should be able to use previous solution to reduce recomputation) ****
    [Jz] = obj.Calc_dhdz(obj.z(:,obj.solpt-1),obj.w(obj.solpt-1)); % Dont Need to Call (use from converged solution)
    [Jw] = obj.Calc_dhdw(obj.w(obj.solpt-1)); % Dont Need to Call (use from converged solution)
    zold = obj.z(:,obj.solpt-1);
    hw = Jw*zold;
else
    % Nothing
    aa = 1;
end

if obj.solpt > 3
        % This is because during the moore-prenros update 
    % the prediction vector is also updated
    Pnew = obj.P(:,obj.solpt-1);
    Pold = obj.P(:,obj.solpt-1);
else
        
    % --- Extract Previous Solution and Prediction --- %
    Pold = obj.P(:,obj.solpt-1);

    % --- Generate Prediction Matrices --- %
    AAp = [Jz hw; Pold'];
%     BBp = [zeros((2*obj.nH+1)*obj.nDOF,1);1];  % [fourier coefficients; 1]
    BBp = sparse((2*obj.nH+1)*obj.nDOF+1,1,1,(2*obj.nH+1)*obj.nDOF+1,1);
    % --- Calculate and Normalize Prediction --- %
    % Solve linear system
    Pnew = obj.SolveLinearSystem(AAp,BBp);
%     Pnew = AAp\BBp;
    Pnew = Pnew/norm(Pnew);

end

% Try rotation 
rotation = false;
if obj.HBcontrol.stp < obj.HBcontrol.stpmin/1e6 && rotation
    
    % Largeset and smallest eigenvalues of jacobian matrix
    [n1,lam1] = eigs(AAp,1,'LM');
    [n2,lam2] = eigs(AAp,1,'SM');
    
    % Try rotation
    nRots = 10;
    theta = linspace(0,pi/2,nRots);
    res = zeros(nRots,1);
    pRot = cell(nRots,1);
    for ii = 1:nRots
        R = eye(obj.nDOF) + (n1.'*n2 - n2.'*n1)*sin(theta(ii)) + ...
            (n1.'*n2 + n2.'*n1)*cos(theta(ii));
        pRot{ii} = R*Pnew;
        % Evaluate Predictions
        zRot = obj.z(:,obj.solpt-1) + obj.HBcontrol.stp*pRot{ii};
        wRot = obj.w(obj.solpt-1) + obj.HBcontrol.stp*pRot{ii};
        fInt = obj.CalcH(zRot,wRot);
        res(ii) = norm(fInt);
    end
%     min(res)
end

%% Angle and dot prodcut
obj.Pstatus.Angle(obj.solpt) = acosd(Pold'*Pnew/(norm(Pold)*norm(Pnew)));
% obj.Pstatus.Angle(obj.solpt) = radtodeg(subspace(Pold,Pnew));
% obj.Pstatus.Angle(obj.solpt) = atan2d(norm(cross(Pold,Pnew)),dot(Pold,Pnew));
obj.Pstatus.Dot(obj.solpt) = dot(Pnew,Pold);

% Control the sign of p
% Pnew=sign(Pnew'*Pold)*Pnew;


% Add some constraints
if all(sign(Pnew)<0) % all disp P's are negative
    Pnew=-1*Pnew;
elseif sum(Pnew)<0 % if the weighted sum is negative
    Pnew=1*Pnew;
elseif obj.Pstatus.Dot(obj.solpt) < 0.125 % Solution is turning back on itself
%     Pnew =-1*Pnew;
    disp('--------------------------------------------------------')
    disp('The prediction vector turned back on itself')
    disp('--------------------------------------------------------')
end

% z0 = obj.z(:,obj.solpt); w0 = obj.w(:,obj.solpt); h = obj.stp;
% % Try 4th order runga-kutta
% k_1 = h*obj.calcH(z0,w0);
% k_2 = h*obj.calcH(z0,w0); F_xy(x(i)+0.5*h,y(i)+0.5*h*k_1);
% k_3 = F_xy((x(i)+0.5*h),(y(i)+0.5*h*k_2));
% k_4 = F_xy((x(i)+h),(y(i)+k_3*h));
% zN = y(i) + (1/6)*(k_1+2*k_2+2*k_3+k_4)*h;  % main equation


% --- SVD Verison of Prediction --- %
% obj = obj.calcTANG([[Jz hw]]);

% If angle is be
if obj.Pstatus.Angle(obj.solpt) < 0
    warning('Negative angle is below certain  exceeded')
    Pnew = -Pnew;
end

% Save new prediction
obj.P(:,obj.solpt) = Pnew;

% --- Calculate Step Size for Curent Solution Point --- %
if obj.solpt > 4
    obj = obj.Calc_StepSize();
    if ~obj.HBcontrol.silent
        fprintf(['Current Stepsize: ',num2str(obj.HBcontrol.stp),...
            ', Num. Iter Prev. ',num2str(obj.HBcontrol.numks(obj.solpt-1)),'  \n']);
    end
else
    obj.HBcontrol.stp = obj.HBcontrol.stpint;
    if ~obj.HBcontrol.silent
        fprintf(['Initial Stepsize: ',num2str(obj.HBcontrol.stp),'  \n']);
    end
end

% Add control based upon angle or dot product
if obj.Pstatus.Angle(obj.solpt) < obj.predControl.betaMin
    warning('Minumum angle exceeded')
    obj.HBcontrol.stp = obj.HBcontrol.stp/obj.HBcontrol.stpdf;
    pause(0.2)
end

% Evaluate Predictions
Zpr = obj.z(:,obj.solpt-1) + obj.HBcontrol.stp*obj.P(1:end-1,obj.solpt);
Wpr = obj.w(obj.solpt-1) + obj.HBcontrol.stp*obj.P(end,obj.solpt);

if strcmp(obj.predControl.type,'poly')
    nTerms = 20;
    if obj.HBcontrol.stp < 1e-4 && obj.solpt > nTerms*100 + 1
        wPr(1) = obj.w(obj.solpt-1) + ...
            obj.HBcontrol.stp*obj.P(end,obj.solpt);
        wPr(2) = obj.w(obj.solpt-1) - ...
            obj.HBcontrol.stp*obj.P(end,obj.solpt);
        pp = csape(obj.w(obj.solpt-nTerms:obj.solpt-1),...
                   obj.z(:,obj.solpt-nTerms:obj.solpt-1));
        ZprPoly(:,1) = fnval(pp,wPr(1));
        ZprPoly(:,2) = fnval(pp,wPr(2));
    %     fprime = fnder(pp,1)
        fprime = fndir(pp,eye(3));
        fdir = fnval(fprime,obj.z(:,obj.solpt-1));

        % Evaluate prediction - help choose direction 
        [HprPoly(1)] = norm(obj.calcH(ZprPoly(:,1),wPr(1)));
        [HprPoly(2)] = norm(obj.calcH(ZprPoly(:,2),wPr(2)));

        while HprPoly(1) < obj.HBcontrol.tol
            wPr(1) = wPr(1)  + obj.HBcontrol.stp*obj.P(end,obj.solpt);
            wPr(2) = wPr(2)  - obj.HBcontrol.stp*obj.P(end,obj.solpt);
            [HprPoly(1)] = norm(obj.calcH(ZprPoly(:,1),wPr(1)));
            [HprPoly(2)] = norm(obj.calcH(ZprPoly(:,2),wPr(2)));
        end

        if HprPoly(1) < HprPoly(2)
            obj.P(end,obj.solpt) = obj.P(end,obj.solpt);
            Zpr = ZprPoly(:,1); Wpr = wPr(1);   
        else
            obj.P(end,obj.solpt) = -1*obj.P(end,obj.solpt);
            Zpr = ZprPoly(:,2); Wpr = wPr(2);
        end
    %     Wpr =  wPrPoly; 
    end
end

end