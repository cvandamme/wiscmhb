function [Jknl] = Calc_dBdP(obj,zt,wt)
% Computes the jacobian of the external focing and nonlinear
% term fourier coefficient matrix wrt to the fourier
% coefficients
%
% Inputs:
%       x = time dependent displacements
%       A = Linear Dynamics Matrix
%       FT = Fourier Tranformation
%       IFT = Inverse Fourier Tranformation
%       dF = derivative of the forces wrt to displacements
% Outputs:
%       Jz = jacobian of H wrt to fourier coefficients
%


% Calculate the Tranformation Matrices (option if required)
[IFT,FT] = obj.calcGAMMA(wt);

% Tranform to Physical Domain
xdt = IFT*zt;

% Calculate Internal Force Gradient (Tangent Stiffness)
[dFdP] = obj.Calc_dFdP(xdt);

% Calcualte the Internal Force Gradient (Frequency)
Jknl = FT*dFdP;


end