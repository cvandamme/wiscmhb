function [A]  = Calc_A(obj,wt)
% This function generates the linear dynamic system marix:
% The matrix is a function of frequency A = A(w)
% ADD EQUATION AND REFERENCE TO PAPER...


% Initialize Variables and Indexs
NABLA1 = zeros((1+2*obj.nH),(1+2*obj.nH));
NABLA2 = zeros((1+2*obj.nH),(1+2*obj.nH));
index1 = 2;
index2 = 3;

% Loop Through to generate submatrices and assemble
for ii = 1:obj.nH
    nabla1 = [0 -(ii*wt/obj.nu); (ii*wt/obj.nu) 0];
    nabla2 = [-(ii*wt/obj.nu)^2 0;0 -(ii*wt/obj.nu)^2];
    NABLA1(index1:index2,index1:index2)= nabla1;
    NABLA2(index1:index2,index1:index2)= nabla2;
    index1 = index2+1;
    index2 = index1+1;
end

% Generate Linear Dynamic Matrix
A = kron(NABLA2,obj.M) + kron(NABLA1,obj.C) + kron(eye(2*obj.nH+1,2*obj.nH+1),obj.K);

end