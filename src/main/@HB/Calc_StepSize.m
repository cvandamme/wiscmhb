function [obj] = Calc_StepSize(obj)
%Csign = sign(obj.HBcontrol.stps(obj.solpt-1)*(obj.P(:,obj.solpt).'*obj.P(:,obj.solpt-1)));
if obj.HBcontrol.numks(obj.solpt-1) == 0
    rat = obj.HBcontrol.optiter/0.3;
else
    rat = obj.HBcontrol.optiter/obj.HBcontrol.numks(obj.solpt-1);
end
if rat > 3
    rat = 3;
end
% if (beta<betamin) && (beta~=0)
%     h=10*(betamin/beta)*h;
% end
%             rat = 2;

Csign = sign(obj.HBcontrol.stpSign);
tempSTP = Csign*rat*abs(obj.HBcontrol.stps(obj.solpt-1));
if abs(tempSTP) > abs(obj.HBcontrol.stpmax)
    tempSTP = sign(tempSTP)*abs(obj.HBcontrol.stpmax);
end
if abs(tempSTP) < abs(obj.HBcontrol.stpmin)
    tempSTP = sign(tempSTP)*abs(obj.HBcontrol.stpmin);
end

obj.HBcontrol.stp=tempSTP;

%if sign(obj.P(:,obj.solpt).'*obj.P(:,obj.solpt)) ~= sign(obj.P(:,obj.solpt-1).'*obj.P(:,obj.solpt-1))
%    warning('Prediction vector is turning back')
%     obj.P(:,obj.solpt) = -obj.P(:,obj.solpt); 
%end

% Check if angle is small 
%if sign(obj.P(:,obj.solpt).'*obj.P(:,obj.solpt-1)) ~= sign(obj.P(:,obj.solpt-1).'*obj.P(:,obj.solpt-2))
%     warning('Prediction vector is turning back')
%     obj.P(:,obj.solpt) = -obj.P(:,obj.solpt); 
%end
% Add in Contraction Rate Calculator

% Add in Angle Calculation

% Add in

end