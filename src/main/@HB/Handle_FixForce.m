function [f,varargout] = Handle_FixForce(obj,aFix,xIn)
% This function can be used as a handle within an optimization to fix the 
% energy of the solution.

% Grab state variables
zC = xIn(1:end-1); wC = xIn(end);

% Calculate residual
[h,dh] = obj.Calc_h(zC,wC); % Evaluate Residual Equation

% Compute energy
[a,da] = obj.Calc_HoldForce(zC,wC);

% Assemble output
f = [h; a-aFix];

% If gradient is requested 
if nargout > 1
    varargout{1} = [dh; da];
end


end