function [obj] = Start(obj,nH,nu,Fext,z0,w0,varargin)
% Initialize

% Set some defaults if not present
if isempty(nH);nH = 1; end 
if isempty(nu);nu = 1; end
if isempty(Fext);Fext = zeros(obj.nDOF,1); end
if isempty(z0) || isempty(w0)
    obj.HBcontrol.startFlag = true; 
else
    obj.HBcontrol.z_int = z0;
    obj.HBcontrol.w_int = w0;
end

% Save Controls
if nargin > 6; obj.SetMainControls(varargin{1}); end

% This determines whether to use force continuation or unforced
% (NNM) calculation
if issparse(obj.C) 
    if nnz(obj.C) == 0
        obj.HBcontrol.NNMflag=true;
    else
        obj.HBcontrol.NNMflag=false; % this means we're doing forced response
    end
elseif norm(obj.C)<10*eps % C is zero
    obj.HBcontrol.NNMflag=true;
else
    obj.HBcontrol.NNMflag=false; % this means we're doing forced response
end

% Store Data
obj.nH = nH;
obj.nu = nu;
obj.Fext = Fext;
obj.solpt = 1;
obj.HBcontrol.endcont = 1;

% Check for flag
if ~isfield(obj.HBcontrol,'startFlag')
    obj.HBcontrol.startFlag = false;
end

% Determines if starting from linear solution
if obj.HBcontrol.startFlag == true
    obj.HBcontrol.x_int = zeros(obj.nDOF,1);
    obj.HBcontrol.x_int(obj.HBcontrol.mode) = 1e-6;
    Qt = zeros(1,2*obj.nH+1);  Qt(3:2:end) = 1; % ? This is wrong!??
        % Why set all cosine harmonics to unit amplitude to start?
    Q = kron(Qt,eye(obj.nDOF)); % What is this doing? (MSA)
    z0 = pinv(Q)*obj.HBcontrol.x_int; obj.HBcontrol.z_int = z0;
        % What is this? (MSA)
    if isa(obj,'HBnlrom')
        obj.HBcontrol.w_int = sqrt(obj.K(obj.HBcontrol.mode,...
                                   obj.HBcontrol.mode));
    end
end
    


% Initialize Continuation Variables (save time and memory)
obj.x = zeros(obj.nDOF,obj.HBcontrol.nSols);
obj.v = zeros(obj.nDOF,obj.HBcontrol.nSols);
obj.P = zeros((2*obj.nH+1)*obj.nDOF+1,obj.HBcontrol.nSols);
obj.z = zeros((2*obj.nH+1)*obj.nDOF,obj.HBcontrol.nSols);
obj.w = zeros(1,obj.HBcontrol.nSols);
obj.E = zeros(1,obj.HBcontrol.nSols);

% Initialize
if obj.stabilityCalc.logic
    obj.stabilityCalc.stable = ones(1,obj.HBcontrol.nSols);
end

% Check that the variables are of appropriate size
if length(z0) ~= (2*obj.nH+1)*obj.nDOF
    disp(['The # of variables in the frequency domain should be  : ',num2str((2*obj.nH+1)*obj.nDOF)])
    disp(['The # of variables in the initial condition vector is : ',num2str(length(obj.HBcontrol.z_int))])
    error('The initial condition vector in frequency domain is incorrect size')
end

% Check for gui command
if obj.HBcontrol.start == 1
    % Start GUI
    WiscMHBGUI('initialize',obj);
else
    % Run Simulation   
    [obj] = obj.Continue();
    %%--------KP: revision for resolving getting stuck during the NO-GUI continuation -------
    %%(should be further improved!...)

    iter=1;
    nStuckCriteria = 10; % criteria for defining the initial solutions (needs better adaptive criteria)
    nMaxIter = 3;  % criteria to stop iteration (changing random initial value or stepSign
    while strcmp(obj.stopCriteria.criteria,'MinStepSize')  %% when continuation stopped due to minStepSize
        if obj.solpt <nStuckCriteria   % when stuck at initial solutions: can be an issue with an initial guess...
            % re-initialize continuation
            obj.x = zeros(obj.nDOF,obj.HBcontrol.nSols);
            obj.v = zeros(obj.nDOF,obj.HBcontrol.nSols);
            obj.P = zeros((2*obj.nH+1)*obj.nDOF+1,obj.HBcontrol.nSols);
            obj.z = zeros((2*obj.nH+1)*obj.nDOF,obj.HBcontrol.nSols);
            obj.w = zeros(1,obj.HBcontrol.nSols);
            obj.E = zeros(1,obj.HBcontrol.nSols);
            obj.solpt = 1;
            
            % create a new random initial guess
            rand_min = 10^-3;
            rand_max =  1;
            randVal = (rand_max-rand_min).*rand(1,1) + rand_min;
            obj.HBcontrol.x_int(obj.HBcontrol.mode) = randVal*obj.HBcontrol.defaultX_int(obj.HBcontrol.mode,1);
            % create initial vector of harmonics and populate with initial guess
            z0 = zeros((2*obj.nH+1)*obj.nDOF,1); % Initialize
            % set appropriate cosine terms to the amplitude above
            z0(2*obj.nDOF+[1:obj.nDOF])=obj.HBcontrol.x_int;
            obj.HBcontrol.z_int = z0;
            
            obj.HBcontrol.w_int = sqrt(obj.K(obj.HBcontrol.mode,...
                                   obj.HBcontrol.mode));
            
            % restart continuation
            obj.HBcontrol.stpint = obj.HBcontrol.defaultStpint;
            obj.HBcontrol.stpSign = obj.HBcontrol.defaultStpSign;
            obj.HBcontrol.endcont = 1;
            [obj] = obj.Continue();
            
        else % stuck at solution greater than nStuckCriteria: can be an issue with step sign...
             % change stepsign
            obj.HBcontrol.stpSign = -1*obj.HBcontrol.stpSign;
                                   
            %restart continuation
            obj.HBcontrol.stpint = obj.HBcontrol.defaultStpint;
            obj.HBcontrol.endcont = 1;
            [obj] = obj.Continue();
        end
        if iter >= nMaxIter
            break
        end
        iter = iter + 1;
    end
    
    % Reduce Down if Not All the Solutions were filled
    obj.x = obj.x(:,1:obj.solpt);
    obj.P = obj.P(:,1:obj.solpt);
    obj.z = obj.z(:,1:obj.solpt);
    obj.w = obj.w(1,1:obj.solpt);
    obj.E = obj.E(1,1:obj.solpt);
%     obj.H = obj.H(1,1:obj.solpt);
    
    %dentify Data to Save
    if isfield(obj.HBcontrol,'save')
        if obj.HBcontrol.save
            save([obj.SAVEdir,'\',obj.Name],'obj','-v7.3')
        end
    end
end

end