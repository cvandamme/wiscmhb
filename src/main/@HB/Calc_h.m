function [H,varargout] = Calc_h(obj,zt,wt)
% This is the function that must be satisfied by harmonic
% balance
%          H(z,w) = A(w)*z -b(z) --> 0
%

% Call State Functions
if nargout > 1
   [b,dbdz] = obj.Calc_b(zt,wt);
else
   b = obj.Calc_b(zt,wt); 
end
A = obj.Calc_A(wt);

% Harmnoic Balance Equation
H = A*zt - b;

% Output gradients if desired
if nargout > 1
    hw = obj.Calc_dhdw(wt);
    varargout{1} = [A-dbdz,hw*zt];
end

end