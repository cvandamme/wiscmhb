function [f,g] = Handle_Fmincon(obj,xIn)
% This function can be used as a handle within an optimization or root
% solving routine such a fsolve. It is meant to be used in conjuction with
% finding a solution about a fixed point, i.e. frequency, energy, etc

% Grab state variables
zC = xIn(1:end-1); wC = xIn(end);

% Calculate residual
[h,~] = obj.Calc_h(zC,wC); % Evaluate Residual Equation

% Must provide scalar value
f = norm(h);

% Gradient is simply function value
g = h/f;

end