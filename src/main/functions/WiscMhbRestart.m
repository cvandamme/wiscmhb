function WiscMhbRestart(nH,varargin)
%% Start Harmonic Balance Solution of Different Order from
% another solution. This is useful for starting a higher-order 
% solution from a low order one. 
% It will take all of the old properties of the WiscMhb besides the
% number of harmonics.
% 


% Grab the file and extract results
[filename, pathname] = uigetfile('*.mat', 'Select a MATLAB data file');
mhbObj = load([pathname,filename]);
mhbObj = mhbObj.obj;

% Plot and grab solution of interest
figure
semilogx(mhbObj.E,mhbObj.w/2/pi,'-ro'); hold on
xlabel('Energy');ylabel('Frequency (Hz)');
[xg,yg] = ginput(1);

% Find corresponding solution
minEnergy = mhbObj.E(1:mhbObj.solpt) - xg;
minFreq =  mhbObj.w(1:mhbObj.solpt)/2/pi - yg;
min_vec = (minEnergy.^2 + minFreq.^2).^(1/2);
[~,min_id] = min(abs(min_vec));

% Extract displacement and fourier vector
xSol = mhbObj.x(:,min_id);
zSol = mhbObj.z(:,min_id);
fSol = mhbObj.w(min_id)/2/pi;
semilogx(mhbObj.E(min_id),fSol,'bs','MarkerSize',20); 

% Plot Solution
% figure; mfObj.Plot(xSol,'norm');

% Grab previous objects
try
    modal = mhbObj.modal; static = mhbObj.nlStatic;
    HBcontrol = mhbObj.HBcontrol;
    freeDof = mhbObj.mfObj.freeDof;
end
% Initial Conditions Physical
HBcontrol.x_int = xSol;

% Initial Conditions (frequency)
HBcontrol.z_int = zeros((2*nH+1)*length(xSol),1);
HBcontrol.z_int(1:(1+2*mhbObj.nH)*length(xSol)) = zSol;

% Initial Frequency
HBcontrol.w_int = fSol*2*pi;

% Name and Save Directory
Name = [mhbObj.Name,'_nHnew_',num2str(nH)];
SaveDir = cd;
 
% Initiate Class
if isa(mhbObj,'HbFemThermal') || isa(mhbObj,'HBfem_thermal')
    [hbRestart] = HbFemThermal(Name,SaveDir,...
                mhbObj.M,mhbObj.C,mhbObj.K,...
                mhbObj.mf,modal,static,mhbObj.thermal);
elseif isa(mhbObj,'HBfem')
    [hbRestart] = HBfem(Name,SaveDir,...
                mhbObj.M,mhbObj.C,mhbObj.K,...
                mhbObj.mf,modal,static);    
elseif isa(mhbObj,'HbRom') 
    [hbRestart] = HbRom(Name,SaveDir,mhbObj.romObj);    
elseif isa(mhbObj,'HbHotRom')
    [hbRestart] = HbHotRom(Name,SaveDir,mhbObj.romObj,...
                  mhbObj.romObj.thermal);        
end

% Run Continuation
hbRestart.Start(nH,mhbObj.nu,mhbObj.Fext,...
                HBcontrol.z_int,HBcontrol.w_int,...
                HBcontrol);

end
