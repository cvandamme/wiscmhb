function WiscMhbIntegrate(nSteps,varargin)
%% Start Harmonic Balance Solution of Different Order from
% another solution. This is useful for starting a higher-order 
% solution from a low order one. 
% It will take all of the old properties of the WiscMhb besides the
% number of harmonics.
% 


% Grab the file and extract results
[filename, pathname] = uigetfile('*.mat', 'Select a MATLAB data file');
mhbObj = load([pathname,filename]);
mhbObj = mhbObj.obj; mfObj = mhbObj.mf;

% Plot and grab solution of interest
figure
semilogx(mhbObj.E,mhbObj.w/2/pi,'-ro'); hold on
xlabel('Energy');ylabel('Frequency (Hz)');
[xg,yg] = ginput(1);

% Find correspodning displacement value
minEnergy = mhbObj.E(1:mhbObj.solpt) - xg;
minFreq =  mhbObj.w(1:mhbObj.solpt)/2/pi - yg;
min_vec = (minEnergy.^2 + minFreq.^2).^(1/2);
[~,min_id] = min(abs(min_vec));
xSol = zeros(mfObj.nDof,1);
xSol(mfObj.freeDof) =  mhbObj.x(:,min_id);
fSol = mhbObj.w(min_id)/2/pi;
semilogx(mhbObj.E(min_id),fSol,'bs','MarkerSize',20); 

% Plot Solution
figure; mfObj.Plot(xSol,'norm');

mhbObj = mhbObj.Calc_Periodicity(nSteps,min_id);  

end
