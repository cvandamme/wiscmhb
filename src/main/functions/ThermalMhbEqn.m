function [f,varargout] = ThermalMhbEqn(mhbObj,w0,z0)
% This function is used to calculate a harmonic balance solution using
% Matlab's fsolve as the solver. 
% Input : 
%        obj = HB object
%        x0 = starting vector
%        h0 = initial internal force in frequency domain
% Ouput : 
%        h = vector of mhb residual
%        jMat = Jacobian matrix

% Calculate residual
[h,dh] = mhbObj.Calc_h(z0,w0);

f = h;
% If varargout
if nargout > 1
    varargout{1} = sparse(dh(:,1:end-1));
%     [Jz] =  mhbObj.calcJz(z0,w0);
%     [Jw] =  mhbObj.calcJw(w0);
%     varargout{1} = [Jz Jw*z0];
end

end