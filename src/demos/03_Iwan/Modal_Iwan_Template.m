%% This scripts generates a 1 by xEles curved beam finite element model 
%  Analysis then performs
%   1) linear modal analysis
%   2) Nonlinear Normal Modes - MHB 
% 
% Required Dependancies: 
%                   2) matlabtools\WiscMHB\Scripts
%                   
% Clear command window and add dependencies
% addpath('..\..\Scripts\')
% addpath('..\..\Scripts\WiscMHBGUI\')
addpath(genpath('..\..\Scripts\'));
% clc

%% Build 1DOF Modal Iwan Model
%
% /|                           |--> u1
% /|                       _________
% /|         Kinf         |         |
% /|______/\/\/\/\/\______|         |
% /|   ________________   |   M0    |
% /|__| Fs Kt beta chi |__|         |
% /|  |________________|  |_________|
% /|

% Linear properties
M = 10;                          % Mass
Kinf = 9;               % linear stiffness

% Nonlinear Iwan Joint parameters
Fs   = 10.0;         % slip force
Kt  = 1;            % stiffness of the joint
beta  = 3.0;           % parameter associated with energy dissipation curve
chi = -0.5;            % parameter associate with slope of log(energy dissipation) vs. log(force)
iwanParam = [Fs,Kt,beta,chi];

% Parameters computed from those above
    wL = sqrt((Kinf+Kt)/M);           % linear natural frequency 
    w0=0.1*wL;
%     C = 2*zeta*M*wL;                 % linear damping
        % That would give the correct damping for the zeta above but is not what Iman used...
        % The prior version had an error here and was using damping based on w0 and not wL.
        % Iman used: C=0.00052705
    C=0.00052705;
%     zeta_lin = 0.00052705/(2*M*wL); % THIS CURRENTLY ISN'T SUPPORTED!

% %%%%%%%% Alternate Parameters, Matching what Iman used
% F_S   = 10.0;
% K_T  = 1; %
% beta  = 3.0;
% chi  = -0.5;
% params = [F_S,K_T,chi,beta];
% M=10; K=9; C=0.00052705; % gives \zeta=0.001
    % Error in whoever did this \zeta calculation??

% % Total joint stiffness in microslip regime
% K0 = Kinf+Kt;
% 
% [phi, lambda] = eig(K0, M);
% 
% % Mass Normalize Modes
% for i = 1:length(M)
%     phi(:, i) = phi(:, i)/sqrt(phi(:, i)'*M*phi(:, i));
% end
phi=1; % Used only to scale plot of physical displacements.

% Set up HB 
nH      = 3;    % # of Harmonics
mode    = 1;    % which mode

% Initialize HBIwan instance
Name = ['Iwan','_NNM_1_nH_',num2str(nH)];
SaveDir = cd;
[HBparam] = HBIwan(Name,SaveDir,M,C,Kinf,iwanParam,phi);



%%
nu      = 1;     % Subharmonic

% Initial amplitude of the mode of interest.
x0 = 1e-7*phi(mode); 

% Initial Fourier Coefficients
    % MSA - These are currently pretty much arbitrary. Should update to
    % solve forced response to get these.
Qt = zeros(1,2*nH); 
Qt(2) = 1; 
Q = kron([0 Qt],speye(length(x0)));
pinvQ = Q';   
z0 = pinvQ*x0;

% External Forcing Amplitude ##############################################
Fext = 10;

% Set parallel options
HBparam.parallelOpts.parallel = true;
HBparam.parallelOpts.numWorkers = 4;

% Establish stopping criteria
HBparam.stopCriteria.frequency = [0.05*wL wL*5];
HBparam.stopCriteria.amplitude = [];
HBparam.stopCriteria.force = [];
HBparam.stopCriteria.interpFinal = false;


%% Harmonic Balance Portion

HBcontrol.stpSign       = 1;
HBcontrol.stpint        = 1e-08;      % initial step size
HBcontrol.stpmax        = 1;         % Max step size
HBcontrol.stpmin        = 1e-016;      % Min step size
HBcontrol.optiter       = 3;          % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf         = 3;          % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter       = 1000;         % Maximum number of iterations before giving up and cutting stepsize.
HBcontrol.tol           = .001;       % Convergence tolerance
HBcontrol.nSols         = 5000;       % Max number of solutions
HBcontrol.nT            = 100;        % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve      = 'Direct';   % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep        = 1000;       % Number of steps in Linear Solution Method
HBcontrol.divfactor     = 5;          % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start         = true;       % 1 for starting GUI, 0 for not
HBcontrol.save          = false;
HBcontrol.silent        = false;
HBcontrol.startFlag     = false;
HBcontrol.mode          = mode;
HBcontrol.freq          = 'rad/s';  % 'rad/s' or 'Hz'

HBcontrol.XnormScale    = 1e0; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale    = 1; % This value determine scaling of initial step (needs some work)
HBcontrol.betamin       = 0; % This value determine scaling of initial step (needs some work)
HBcontrol.sil           = false;

HBparam.HBcontrol = HBcontrol;
HBparam.predControl.enforceEnergy = 0;


% Run Continuation
tic
HBparam = HBparam.Start(nH,nu,Fext,z0,w0);
simTime=toc;

return
%%
% Create a plot
figure(100);
    % Estimate amplitude of vibration
    AMHB=sqrt(HBparam.x.^2+HBparam.v.^2./HBparam.w.^2);
line(HBparam.w/2/pi, AMHB/Fext,'DisplayName',num2str(Fext))
xlabel('Frequency (Hz)')
ylabel('FRF Amplitude (m/N)')
set(gca,'Yscale','Log'); 

