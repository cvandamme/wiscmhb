%% This scripts generates a 1 by xEles curved beam finite element model 
%  Analysis then performs
%   1) linear modal analysis
%   2) Nonlinear Normal Modes - MHB 
% 
% Required Dependancies: 
%                   1) OsFern
%                   2) matlabtools\WiscMHB\Scripts
%                   
% Clear command window and add dependencies
% Path Dependancies
%    1. Add OsFern master directory with subfolders
%    2. Add scripts directory with subfolders
%

clc

%% Specify OsFern FE input paramters 

% Specify which curved structure we want to generate
model = 'curvedBeam';

% Specify properties of the FE mesh
xEles = 40; 
xNodes = xEles + 1;

% Specify geometric properties of FE model
riseRatio = 1.0;
xLength = 1; 
thk = 0.01; 
width = 0.01;
asymmetryScaling = 0.001;

% Specify boundary conditions
endConstraints = true;  
sideConstraints = true;

% Specify material properties of FE model
materialIn = 'steel';
E = 210e9; 
rho = 7850; 
nu = 0.29;
alpha = 6.3e-06; 
refTemp = 77;

% Specify which plots 
plotFem = 1;
plotLinModes = 0;

%% Generat

% Call the OsFern constructor to generate FE object 
curvedFEM = OsFern(model);

% Update FE cross section properties
beamSection.w = width; 
beamSection.t = thk;

% Use the rise ratio to calculate the height and radius of curvature 
if riseRatio == 0
    zPosMax = 0; 
    r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Update material properties using OsFern Material Class
material = Material(materialIn);
material = material.setProp('E',E);
material = material.setProp('rho',rho);
material = material.setProp('nu',nu);
material = material.setProp('alpha',alpha);
material = material.setProp('tRef',refTemp);

% Creat basic mesh
nodeId = 1; 
eleId = 1;
endNodes = []; 
sideNodes = [];
dVars = struct();
dVars.x = cell(xNodes,1);
dVars.z = cell(xNodes,1);
index = 1;

% Generate mesh
for ii = 1:xNodes
        
    % Coordinates
    xPos = DesignVariable(xLength*((ii-1)/xEles),['xPos',num2str(ii)]);
    yPos = 0; 
    if riseRatio == 0
        zPos = 0;
    else
        zPos = DesignVariable(sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax,['zPos',num2str(ii)]);
    end
    
    dVars.x{ii} = xPos;
    dVars.z{ii} = zPos; 

  
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node and add to mesh
    curvedFEM = curvedFEM.AddNodeObject(tempNode);

    % Check for constrained boundary conditions
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    % Label center node
    if ii == xEles/2 + 1
        curvedFEM.Add2Nset('CenterNode', tempNode);
    end

    % Create element and add to mesh
    if ii > 1
        b31Ele = B2(eleId); 
        eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; 
        csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedFEM.GetNodes(eleNodeIds),...
                 material, beamSection,csVec);
        curvedFEM.AddElementObject(b31Ele); 
    end
    
    % Increment node number
    nodeId = nodeId + 1;
end

% Make sure the model is self-consistent 
curvedFEM = curvedFEM.Update();

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', curvedFEM);
modal.Solve(nmodes);
scale = 1/100;

% Nonlinear static solution
static = NLStatic('static', curvedFEM);

% Plot linear mode shapes
if plotLinModes
    f = figure; %#ok<*UNRCH>
    tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
    for ii = 1:length(modal.fn)
        modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
        axes('Parent',modalTab{ii});
        title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
        curvedFEM.Plot();
        curvedFEM.Plot(scale*modal.phi(:,ii),'norm');  view([0 -1 0]); colorbar
    end
end
    
% Get mass normalized mode 2 
mode2 = modal.phi(:,2)/norm(modal.phi(:,2));

mode2 = reshape(mode2,[6,curvedFEM.nNode]);
mode2 = mode2';

asymmetryScaling = (xLength/2-asymmetryScaling*xLength);

% Add mode shapes to dVars
for ii = 1:xNodes


    % Get xPos and zPos design variables
    dVars.x{ii}.SetVal(dVars.x{ii}.GetVal + asymmetryScaling*mode2(ii,1));
    dVars.z{ii}.SetVal(dVars.z{ii}.GetVal + asymmetryScaling*mode2(ii,3));


end

% Make sure model is self consistent after adding asymmetry
curvedFEM = curvedFEM.Update();

if plotFem
    figure;curvedFEM.Plot(); view([0 -1 0]); 
end


%% Harmonic Balance Portion
nH      = 1;     % # of Harmonics
nu      = 1;     % Subharmonic
mode    = 1;   % which mode

w0      = modal.fn(mode)*2*pi;    % Initial Freq   
x0 = modal.phi(curvedFEM.freeDof,mode); % Initial amplitude of the mode of interest.

HBcontrol.stpSign     = 1; % Sign of continuation step
HBcontrol.stpint      = 1e-05;  % initial step size
HBcontrol.stpmax      = 15;    % Max step size
HBcontrol.stpmin      = 1e-05; % Min step size
HBcontrol.optiter     = 4;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf       = 3;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter     = 10;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol         = .001;  % Convergence tolerance
HBcontrol.nSols       = 5000;  % Max number of solutions
HBcontrol.nT          = 100;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve    = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep      = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor   = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start       = true;     % 1 for starting GUI, 0 for not
HBcontrol.save        = true;
HBcontrol.silent      = false;
HBcontrol.startFlag   = false;
HBcontrol.mode        = mode;
HBcontrol.freq     = 'Hz';  % 'rad/s' or 'Hz'

HBcontrol.XnormScale = 1e0; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale = 1; % This value determine scaling of initial step (needs some work)
HBcontrol.betamin = 0; % This value determine scaling of initial step (needs some work)

% Obtain vector of free DOF
freeDof = curvedFEM.BuildConstraintVec();

% Initial Conditions Modal Coordinates
if x0(round(xEles/2)) < 0
    HBcontrol.x_int = -1*1e-7*x0; % scale   
else
     HBcontrol.x_int = 1e-7*x0; % scale 
end   

% Initial Fourier Coefficients
Qt = zeros(1,2*nH); Qt(2) = 1; %repmat([0 1],1,nH);
Q = kron([0 Qt],speye(length(x0)));
pinvQ = Q';   
z0 = pinvQ*HBcontrol.x_int;

% Name and Save Directory
newStr = strrep(num2str(round(riseRatio,2)),'.','p');
Name = ['CurvedBeam_FEM_rR_',newStr,'_NNM_',num2str(mode),'_nH_',num2str(nH)];
SaveDir = cd;
            
% Create linear properites
M = curvedFEM.GetMassMatrix();
K = curvedFEM.GetStiffnessMatrix();
C = sparse(zeros(size(M)));
Fext = zeros(size(x0));

% If performing forced analysis
if false
    fScale = 1e-4;
    dampAlpha = 0.02;
    dampBeta = 1e-6;
    Fext = -fScale*x0;
    C = dampAlpha*M + dampBeta*K;
end

% Instantiate Class
[HBparam] = HBfem(Name,SaveDir,M,C,K,curvedFEM,modal,static);

% Set parallel options
HBparam.parallelOpts.parallel = true;
HBparam.parallelOpts.numWorkers = 4;

% Establish stopping criteria
HBparam.stopCriteria.frequency = [0.25*modal.fn(mode)*2*pi modal.fn(mode)*2*pi*2];
HBparam.stopCriteria.amplitude = [];
HBparam.stopCriteria.force = [];
HBparam.stopCriteria.interpFinal = false;

% Run Continuation
tic
HBparam = HBparam.Start(nH,nu,Fext,z0,w0);
simTime=toc;

