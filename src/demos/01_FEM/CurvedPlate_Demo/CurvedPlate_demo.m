%% Test Harmonic Balance Continuation Routine applied Directly to Finite
% element modes
clear; clc; close all;

% Setup Model 
SAVEdir = cd;
clear classes
%% Build Model


CurvedPlate = OsFern('CurvedBeam');

% Plate Dimensions and mesh Properties (keep even # of elements)
% metric units - aluminum
xLength = 0.5; xEles = 24; xNodes = xEles + 1;
yLength = 0.5; yEles = 24; yNodes = yEles + 1;

% Element/shell thickeness
thk = 1.5/1000;

% Polynomial Coefficients for Curvature (from picture)
riseRatio = 2.5;

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Material Properties
E = 2.07e11; rho = 7.83e3; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Material Properties
E = 71000; rho = 2.7e-9; nu = 0.33; alpha = 6.3e-06;
aluminum = Material('aluminum');
aluminum = aluminum.setProp('E',E);
aluminum = aluminum.setProp('rho',rho);
aluminum = aluminum.setProp('nu',nu);

shellSection.t = thk;

% Decidie if doing force/damped analysis or just NNM
forced = false;
figure; hold on

% Creat basic mesh
nodeId = 1; elementId = 1;
for ii = 1:yNodes
    for jj = 1:xNodes
        
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles); 
        if riseRatio == 0
            zPos = 0;
        else
            zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
        end
        % Create node object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node object
        CurvedPlate = CurvedPlate.AddNodeObject(tempNode);
        
        % Contrain ends
        if (jj == 1 || jj == xNodes)
            CurvedPlate.Add2Nset('BoundaryNodes', tempNode);
            tempNode = tempNode.SetFixed();
        end
        
        % Contrain ends
        if (jj == 1 || jj == xNodes)
            CurvedPlate.Add2Nset('BoundaryNodes', tempNode);
            tempNode = tempNode.SetFixed();
        end
        
        % Create Center Node Object
        if ii == yEles/2 + 1 && jj == xEles/2 + 1
            CurvedPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId+1;
            nodeIds = [(ii - 2) * xNodes + (jj - 1),...
                       (ii - 2) * xNodes + (jj),...
                	    (ii-1) * xNodes + (jj),...
                        (ii-1) * xNodes + (jj - 1)];
            s4Ele = s4Ele.Build(CurvedPlate.GetNodes(nodeIds), steel, shellSection);
            CurvedPlate.AddElementObject(s4Ele);
        end
                
        % Add to node count
        nodeId = nodeId + 1;
    end
end


% Build Constraints and DOF
CurvedPlate = CurvedPlate.Update();
CurvedPlate.Plot();


% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', CurvedPlate);
modal.Solve(nmodes);
modal.phi = real(modal.phi);
modoal.fn = real(modal.fn);
% CurvedPlate.Plot(modal.phi(:,2)/norm(modal.phi(:,2)));

% Initialize static object
static = NLStatic('static', CurvedPlate);

% Plot Mode shapes
f = figure; scale = 1/100;
tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
for ii = 1:length(modal.fn)
    modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    axes('Parent',modalTab{ii});
    title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    CurvedPlate.Plot(scale*modal.phi(:,ii),'norm');  view([0 0 -1]); colorbar
end

%% Harmonic Balance Portion
% Define Harmonic Balance Controls 
nH = 1;     % # of Harmonics
nu = 1;     % Subharmonic
mode = 1;   %


HBcontrol.stpint   = 1e-4;  % initial step size
HBcontrol.stpmax   = 25;    % Max step size
HBcontrol.stpmin   = 1e-8; % Min step size
HBcontrol.optiter  = 4;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf    = 4;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter  = 10;    % Maximum number of iterations before giving up and cutting stepsize.
HBcontrol.wlim     = [0 modal.fn(mode)*2*pi*2]; % frequency boundary
HBcontrol.nSols    = 500;  % Max number of solutions
HBcontrol.nT       = 100;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.nbstep   = 1000;  % Number of steps in Linear Solution Method
HBcontrol.w_int    = modal.fn(mode)*2*pi;    % Initial Freq
HBcontrol.tol      = 1e-4;  % Convergence tolerance
HBcontrol.mode     = 1;  % Initial condition has a value for this mode of amplitude set below.
HBcontrol.LinSolve = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.freq     = 'Hz';  % 'rad/s' or 'Hz'
HBcontrol.nH       = nH;    % Number of Harmonics
HBcontrol.start    = 1;     % 1 for starting GUI, 0 for not
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
                            % should eventually base on quadractic convergence of PC methods

HBcontrol.XnormScale = 1e0; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale = 1; % This value determine scaling of initial step (needs some work)
HBcontrol.betamin = 0; % This value determine scaling of initial step (needs some work)
HBcontrol.silent = false;

% Obtain vector of free DOF
freeDof = CurvedPlate.BuildConstraintVec();

% Initial Conditions Modal Coordinates
x0 = modal.phi(freeDof,mode); % Initial amplitude of the mode of interest.
HBcontrol.x_int = 1e-6*x0; % scale 

% Initial Fourier Coefficients
Qt = zeros(1,2*nH); Qt(2) = 1; %repmat([0 1],1,nH);
Q = kron([0 Qt],speye(length(x0)));
pinvQ = Q';   
HBcontrol.z_int = pinvQ*HBcontrol.x_int;

% Name and Save Directory
Name = ['CurvedPlate_FEM_NNM_',num2str(mode),'_nH_',num2str(nH)];
SaveDir = cd;
            
% Create linear properites
M = CurvedPlate.GetMassMatrix();
K = CurvedPlate.GetStiffnessMatrix();
C = sparse(zeros(size(M)));
Fext = zeros(size(x0));

% If performing forced analysis
if forced
    fScale = 1e-4;
    dampAlpha = 0.02;
    dampBeta = 1e-6;
    Fext = -fScale*x0;
    C = dampAlpha*M + dampBeta*K;
end

% Initiate Class
[HBparam] = HBfem(Name,SaveDir,M,C,K,CurvedPlate,modal,static);

% Set parallel options
HBparam.parallelOpts.parallel = true;
HBparam.parallelOpts.numWorkers = 4;

% Establish stopping criteria
HBparam.stopCriteria.frequency = [0.25*modal.fn(mode)*2*pi modal.fn(mode)*2*pi*2];
HBparam.stopCriteria.amplitude = [];
HBparam.stopCriteria.force = [];
HBparam.stopCriteria.interpFinal = false;

% Run Continuation
tic
HBparam = HBparam.Start(nH,nu,Fext,HBcontrol.z_int,...
                        HBcontrol.w_int,HBcontrol);
simTime=toc;
