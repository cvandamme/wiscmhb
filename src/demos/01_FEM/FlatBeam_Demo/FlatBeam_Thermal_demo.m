%% This script creates a finite element model of a flat-beam using 
%  beam elements. The script performs the following analyses :
%  1) Linear Modal
%  2) Thermal
%  3) Linaer Modal about Thermally Deformed State
clear; clc; close all;
clear classes;
%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.031; width = 0.5;
beamSection.w = width; beamSection.t = thk;

% Material Properties (steel)
E = 2.97e7; rho = 0.000736; nu = 0.280712;
alpha = 6.3e-06;refTemp = 0;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; zPos = 0;
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if ii == 1 || ii == xNodes
        tempNode = tempNode.SetFixed();
    else
        % Constrain in plane motion
        if sideConstraints
            tempNode = tempNode.SetFixed([2,4,6]);
        end
    end

    % Identify center node
    if ii == xEles/2 + 1
%         tempNode.SetXYZ(xPos,yPos,zPos+1e-4);
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, beamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update();
figure; flatBeam.Plot(); 

%% Linear modal analysis
nmodes = 40;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

%% Thermal step
xScale = 0;
xDisp = xScale*modal.phi(:,2)/norm(modal.phi(:,2));

name = 'fullDynamic';
thermal = Thermal(name, flatBeam, xDisp);

% Create thermal field object
temperature = 2;
thermalField = ThermalField('UniformField',flatBeam,temperature);
thermalField.AddFieldVector(ones(length(flatBeam.node),1));

% Add thermal field to thermal object
thermal.AddThermalField(thermalField);

% Grab center node and apply force
forceNode = flatBeam.nset('CenterNode'); 
% forceNode = {flatBeam.node{10}}
force = Force('CenterNode',flatBeam);
force.AddForce(forceNode,3,0);
thermal.AddForce(force);
thermal.AddMonitor(forceNode{1}.id, 3);  

% Solve thermal problem
thermal.Solve(6);

% Plot final displacement field
figure;flatBeam.Plot(thermal.finalDisplacement,'Z'); colorbar;
view([0 -1 0]);

%% Modal solution about equilibrium state - recommended and quicker
thermal.Frequency(20);
static = NLStatic('Static',flatBeam);

% Compare with abaqus
abqFn_Nom = [79.02;217.92;427.46];
abqFn_Heated = [65.38;200.43;408.78];

figure; hold on
subplot(2,2,1); 
bar([abqFn_Nom, modal.fn(1:3)]); 
legend('Abaqus','OsFern');
xlabel('Mode #'); ylabel('Frequency (Hz)');
title('Base State Modes')

subplot(2,2,2); 
bar([abqFn_Heated, thermal.fn(1:3)]); 
legend('Abaqus','OsFern');
xlabel('Mode #'); ylabel('Frequency (Hz)');
title('Heated Modes')

subplot(2,2,3); 
bar(100*(abqFn_Nom-modal.fn(1:3))./abqFn_Nom); 
xlabel('Mode #'); ylabel('% Error');
title('Error in Base State Modes')

subplot(2,2,4); 
bar(100*(abqFn_Heated-thermal.fn(1:3))./abqFn_Heated); 
xlabel('Mode #'); ylabel('% Error');
title('Error in Heated Modes')
% thermal.fn(1) = imag(thermal.fn(1));
% Plot mode comaprison - eventually overlay results to show axial
% contribution
figure
subplot(3,2,1); hold on
title(['Unheated : Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatBeam.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,2)
title(['Heated : Mode 1 : ',num2str(round(thermal.fn(1))),' Hz'])
flatBeam.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,3)
title(['Unheated : Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatBeam.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,4)
title(['Heated : Mode 2 : ',num2str(round(thermal.fn(2))),' Hz'])
flatBeam.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,5)
title(['Unheated : Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatBeam.Plot(modal.phi(:,3)/norm(modal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,6)
title(['Heated : Mode 3 : ',num2str(round(thermal.fn(3))),' Hz'])
flatBeam.Plot(thermal.phi(:,3)/norm(thermal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
% return
%% Integrate Model
name = 'FullDynamic';

% Set duration and number of steps 
numCycles = 90; T = numCycles*(1/modal.fn(1));
dt = 1e-4; nSteps = floor(T/dt);
t = linspace(0,T,nSteps); dt = T/length(t);

% Get the center node
cNode = flatBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Create ammplitude object
rampAmp = [linspace(0,1,nSteps/3),linspace(1,1,nSteps/3),linspace(1,0,nSteps/3)];
amp = Amplitude('thermalAmp',t,rampAmp);

% % Create thermal field object
% thermalField = ThermalField('Uniform',flatBeam);
% thermalField.AddFieldVector(nodalTemp);
% thermalField.AddAmplitude(amp);

% Initial Displacement 
xScale = 0.001;
xDisp = thermal.finalDisplacement + xScale*thermal.phi(:,2)/norm(modal.phi(:,2));

% Initialize dynamic object
dynamic = DynamicThermal(name, flatBeam, xDisp);

% Add rayleigh damping
dynamic.SetDamping(34,1e-4); % (mass proportional, stiffness proportional)

% Add nodes and dof to monitor
dynamic.AddMonitor(cNode{1}.id, 3);

% Add plotting options
dynamic.SetPlotOpts(true,5);

% Apply thermal field
dynamic.AddThermalField(thermalField);

% Aplly force

% Integrate model
tic
% dynamic.Solve(T, dt);
tSimSerial = toc;

% Plot the response
figure;
plot(dynamic.t,dynamic.response);
xlabel('Time (s)'); ylabel('Response (in)')

%% Harmonic Balance Portion
% Define Harmonic Balance Controls 
nH = 5;     % # of Harmonics
nu = 1;     % Subharmonic
mode = 1;   % Mode #


HBcontrol.stpint   = 1e-6;  % initial step size
HBcontrol.stpmax   = 10;    % Max step size
HBcontrol.stpmin   = 1e-15; % Min step size
HBcontrol.optiter  = 3;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf    = 4;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter  = 5;    % Maximum number of iterations before giving up and cutting stepsize.
HBcontrol.wlim     = [0 modal.fn(mode)*2*pi*2]; % frequency boundary
HBcontrol.nSols    = 2000;  % Max number of solutions
HBcontrol.nT       = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.nbstep   = 1000;  % Number of steps in Linear Solution Method
HBcontrol.w_int    = thermal.fn(mode)*2*pi;    % Initial Freq
HBcontrol.tol      = 1e-4;  % Convergence tolerance
HBcontrol.mode     = mode;  % Initial condition has a value for this mode of amplitude set below.
HBcontrol.LinSolve = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.freq     = 'Hz';  % 'rad/s' or 'Hz'
HBcontrol.nH       = nH;    % Number of Harmonics
HBcontrol.start    = true;     % 1 for starting GUI, 0 for not
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
                            % should eventually base on quadractic convergence of PC methods

HBcontrol.XnormScale = 1e0; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale = 1; % This value determine scaling of initial step (needs some work)
HBcontrol.betamin = 0; % This value determine scaling of initial step (needs some work)
HBcontrol.silent = false;
HBcontrol.stpSign = 1;

% Obtain vector of free DOF
freeDof = flatBeam.BuildConstraintVec();

% Initial Conditions Modal Coordinates
x0 = thermal.phi(freeDof,mode); % Initial amplitude of the mode of interest.
HBcontrol.x_int = 1*thermal.finalDisplacement(freeDof)+1e-5*x0; % scale 

% Initial Fourier Coefficients
Qt = zeros(1,2*nH); Qt(2) = 1;
Q = kron([1 Qt],speye(length(x0)));
HBcontrol.z_int = Q'*HBcontrol.x_int;

% Name and Save Directory
Name = ['flatBeam_Heated_FEM_',num2str(mode),'_nH_',num2str(nH),'_dT_',num2str(round(temperature))];
SaveDir = cd;
            
% Create linear properites
M = flatBeam.GetMassMatrix();
K = flatBeam.GetStiffnessMatrix();
[Kn, Kt] = flatBeam.GetTangentStiffness(thermal.finalDisplacement);
C = sparse(zeros(size(M)));

% Initiate Class
[flatBeamMhb] = HbFemThermal(Name,SaveDir,M,C,K+thermal.Kss,flatBeam,modal,static,thermal);

% Set parallel options
flatBeamMhb.parallelOpts.logic = false;
flatBeamMhb.parallelOpts.numWorkers = 4;
flatBeamMhb.predControl.enforceEnergy = false;

% Set options
flatBeamMhb.HBcontrol = HBcontrol;
flatBeamMhb.nH = nH; flatBeamMhb.nu = nu;
fExt = zeros(flatBeam.nDof,1);
flatBeamMhb.Fext = fExt(flatBeam.freeDof);

%% Try to find first solution

% Initial fourier coefficent guess
z0 = HBcontrol.z_int; w0 = HBcontrol.w_int;

% Create function handle
mhbFun = @(x0) ThermalMhbEqn(flatBeamMhb,w0,x0);

% Try fsolve on the system
options = optimoptions(@fsolve,'Display','iter',...
                      'SpecifyObjectiveGradient',true,...
                      'CheckGradient',false,...
                      'ScaleProblem','none',...
                      'StepTolerance',1e-10,...
                      'MaxIterations',50);
                  
[x,F,exitflag,output,JAC] = fsolve(mhbFun,z0,options);


%%
HBcontrol.z_int = x;
% HBcontrol.w_int = x(end);

% Run Continuation
tic
flatBeamMhb = flatBeamMhb.Start(nH,nu,fExt(flatBeam.freeDof),...
                            HBcontrol.z_int,w0,HBcontrol);
simTime=toc