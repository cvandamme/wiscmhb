%% This scripts generates a basic finite element model of a flat
%  beam modeled with 2-Node Beam Elements.
%  Analysis then performs
%   1) linear modal analysis
%   2) Nonlinear Normal Modes - MHB 
clear; clc; close all;
clear classes;
%% Build Model

% Initialize model
flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
% thk = 0.000508; width = 0.0127;
beamSection.w = width; beamSection.t = thk;

% Polynomial Coefficients for Curvature (from picture)
riseRatio = 0;

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% r = 120; 
% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);


% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    if riseRatio == 0
        zPos = 0;
    else
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    end
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, beamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update(); 
figure; flatBeam.Plot(); view([0 -1 0]);
%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

% Nonlinaer static solution
static = NLStatic('static', flatBeam);

% Plot Mode shapes
f = figure; scale = 1/100;
tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
for ii = 1:length(modal.fn)
    modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    axes('Parent',modalTab{ii});
    title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    flatBeam.Plot();
    flatBeam.Plot(scale*modal.phi(:,ii),'norm');  view([0 -1 0]); colorbar
end

%% Harmonic Balance Portion
% Define Harmonic Balance Controls 
nH = 1;     % # of Harmonics
nu = 1;     % Subharmonic
mode = 1;   %


HBcontrol.stpint   = 1e-5;  % initial step size
HBcontrol.stpmax   = 25;    % Max step size
HBcontrol.stpmin   = 1e-8; % Min step size
HBcontrol.optiter  = 4;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf    = 4;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter  = 10;    % Maximum number of iterations before giving up and cutting stepsize.
HBcontrol.wlim     = [0 modal.fn(mode)*2*pi*2]; % frequency boundary
HBcontrol.nSols    = 1000;  % Max number of solutions
HBcontrol.nT       = 100;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.nbstep   = 1000;  % Number of steps in Linear Solution Method
HBcontrol.w_int    = modal.fn(mode)*2*pi;    % Initial Freq
HBcontrol.tol      = 1e-4;  % Convergence tolerance
HBcontrol.mode     = 1;  % Initial condition has a value for this mode of amplitude set below.
HBcontrol.LinSolve = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.freq     = 'Hz';  % 'rad/s' or 'Hz'
HBcontrol.nH       = nH;    % Number of Harmonics
HBcontrol.start    = 1;     % 1 for starting GUI, 0 for not
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
                            % should eventually base on quadractic convergence of PC methods

HBcontrol.XnormScale = 1e0; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale = 1; % This value determine scaling of initial step (needs some work)
HBcontrol.betamin = 0; % This value determine scaling of initial step (needs some work)
HBcontrol.sil = false;

% Obtain vector of free DOF
freeDof = flatBeam.BuildConstraintVec();

% Initial Conditions Modal Coordinates
x0 = modal.phi(freeDof,mode); % Initial amplitude of the mode of interest.
HBcontrol.x_int = 1e-8*x0; % scale 

% Initial Fourier Coefficients
Qt = zeros(1,2*nH); Qt(2) = 1; %repmat([0 1],1,nH);
Q = kron([0 Qt],speye(length(x0)));
pinvQ = Q';   
HBcontrol.z_int = pinvQ*HBcontrol.x_int;

% Name and Save Directory
newStr = strrep(num2str(round(riseRatio,2)),'.','p');
Name = ['flatBeam_FEM_rR_',newStr,'_NNM_',num2str(mode),'_nH_',num2str(nH)];
SaveDir = cd;
            
% Create linear properites
M = flatBeam.GetMassMatrix();
K = flatBeam.GetStiffnessMatrix();
C = sparse(zeros(size(M)));
Fext = zeros(size(x0));

% If performing forced analysis
if false
    fScale = 1e-4;
    dampAlpha = 0.02;
    dampBeta = 1e-6;
    Fext = -fScale*x0;
    C = dampAlpha*M + dampBeta*K;
end

% Initiate Class
[HBparam] = HBfem(Name,SaveDir,M,C,K,flatBeam,modal,static);

% Set parallel options
HBparam.parallelOpts.parallel = false;
HBparam.parallelOpts.numWorkers = 4;

% Establish stopping criteria
HBparam.stopCriteria.frequency = [0.25*modal.fn(mode)*2*pi modal.fn(mode)*2*pi*2];
HBparam.stopCriteria.amplitude = [];
HBparam.stopCriteria.force = [];
HBparam.stopCriteria.interpFinal = false;

% Run Continuation
tic
HBparam = HBparam.Start(nH,nu,Fext,HBcontrol.z_int,HBcontrol.w_int);
simTime=toc;
