%% Test Harmonic Balance Continuation Routine applied Directly to Finite
% element models, this example is on the cummins exhaust plate
clear; clc; %close all;

% Setup Model 
SAVEdir = cd;

%% Build Model

% Read in Curved Beam Raw Data
load('exhaustCoverPlate.mat');

% Initialize MatFem Object
exhaustPlate = OsFern('ExhaustCover');

% Modulus and density
E = 168e9; rho = 5120; 

% Material Properties
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
shellSection.t = thickness;

% Setup constraints
endConstraints = [];

% Decidie if doing force/damped analysis or just NNM
forced = false;

% Creat basic mesh
nodeId = 1; elementId = 1;
for ii = 1:length(nodes)
        
    % Node Id 
    nodeId = nodes(ii,1);
    
    % Coordinates
    xPos = nodes(ii,2);
    yPos = nodes(ii,3);
    zPos = nodes(ii,4);
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node object
    exhaustPlate = exhaustPlate.AddNodeObject(tempNode);

    % Fixe nodes
    if any(nodeId==spcs(:,1))
        tempNode = tempNode.SetFixed();
        exhaustPlate.Add2Nset('BoundaryNodes',tempNode)
    end
    
end

for ii = 1:length(elements)
    
    % create 4-node shell element
    s4Ele = S4S(elementId); elementId = elementId + 1;
    nodeId = elements(ii,2:5);
    eleObjNodes = [exhaustPlate.node(nodeId(1)),...
                exhaustPlate.node(nodeId(2)),...
                exhaustPlate.node(nodeId(3)),...
                exhaustPlate.node(nodeId(4))];
    s4Ele = s4Ele.Build(eleObjNodes, steel, shellSection);
    exhaustPlate.AddElementObject(s4Ele);
    
end

% Build Constraints and DOF
exhaustPlate = exhaustPlate.Update();
exhaustPlate.Plot();

% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', exhaustPlate);
modal.Solve(nmodes);

% Initialize static object
static = NLStatic('static', exhaustPlate);

% Plot Mode shapes
f = figure; scale = 1/100;
tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
for ii = 1:length(modal.fn)
    modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    axes('Parent',modalTab{ii});
    title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    exhaustPlate.Plot(scale*modal.phi(:,ii),'norm');  view([0 0 -1]); colorbar
end

%% Harmonic Balance Portion
% Define Harmonic Balance Controls 
nH = 3;     % # of Harmonics
nu = 1;     % Subharmonic
mode = 1;   %

HBcontrol.stpSign  = 1;     % direction of the step
HBcontrol.stpint   = 1e-3;  % initial step size
HBcontrol.stpmax   = 25;    % Max step size
HBcontrol.stpmin   = 1e-8;  % Min step size
HBcontrol.optiter  = 1;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf    = 4;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter  = 10;    % Maximum number of iterations before giving up and cutting stepsize.
HBcontrol.wlim     = [0 modal.fn(mode)*2*pi*1.77]; % frequency boundary
HBcontrol.nSols    = 500;   % Max number of solutions
HBcontrol.nT       = 100;   % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.nbstep   = 1000;  % Number of steps in Linear Solution Method
HBcontrol.w_int    = modal.fn(mode)*2*pi;    % Initial Freq
HBcontrol.tol      = 1e-4;  % Convergence tolerance
HBcontrol.mode     = 1;     % Initial condition has a value for this mode of amplitude set below.
HBcontrol.LinSolve = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.freq     = 'Hz';  % 'rad/s' or 'Hz'
HBcontrol.nH       = nH;    % Number of Harmonics
HBcontrol.start    = 1;     % 1 for starting GUI, 0 for not

% Obtain vector of free DOF
freeDof = exhaustPlate.BuildConstraintVec();

% Initial Conditions Modal Coordinates
x0 = modal.phi(freeDof,mode); % Initial amplitude of the mode of interest.
HBcontrol.x_int = 1e-6*x0; % scale 

% Initial Fourier Coefficients
Qt = zeros(1,2*nH); Qt(2) = 1; %repmat([0 1],1,nH);
Q = kron([0 Qt],speye(length(x0)));
pinvQ = Q';   
HBcontrol.z_int = pinvQ*HBcontrol.x_int;

% Name and Save Directory
Name = ['exhaustPlate_FEM_NNM_',num2str(mode),'_nH_',num2str(nH)];
SaveDir = cd;
            
% Create linear properites
M = exhaustPlate.GetMassMatrix();
K = exhaustPlate.GetStiffnessMatrix();
C = sparse(zeros(size(M)));
Fext = zeros(size(x0));

% If performing forced analysis
if forced
    fScale = 1e-4;
    dampAlpha = 0.02;
    dampBeta = 1e-6;
    Fext = -fScale*x0;
    C = dampAlpha*M + dampBeta*K;
end

% Initiate Class
[exhaustPlateMhb] = HBfem(Name,SaveDir,M,C,K,exhaustPlate,modal,static);

% Set parallel options
exhaustPlateMhb.parallelOpts.parallel = true;
exhaustPlateMhb.parallelOpts.numWorkers = 4;

% Run Continuation
tic
exhaustPlateMhb = exhaustPlateMhb.Start(nH,nu,Fext,HBcontrol.z_int,HBcontrol.w_int,HBcontrol);
simTime=toc;
