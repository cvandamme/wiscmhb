%% This scripts generates a finite element model of a flat beam, creates
%  a nonlinear reduced order model and then computes NNMs using the
%  multi-harmonic balance method. 
%  *** Note this demo requires the OsFern finite element library to work
%
%  Analyses Performed
%  1) Linear Modal
%  2) ROM Genearation via Implicit Condensation
%  3) NNM via Multi-Harmonic Balance method
clear; clc; close all; clear classes;
%% Build Model

% Initialize OsFern Object
try
    curvedBeam = OsFern('CurvedBeam');
catch
    disp('--------------------------------------------------------------')
    disp('--------------------------------------------------------------')
    error('Must have the OsFern finite element code to perform this demo')
end

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Element/shell thickeness - Metric
thk = 0.031; w = 0.5;
beamSection.w = w; beamSection.t = thk;

% Material Properties (steel) 
E = 10.6*10^6; rho = 2.588*10^-4; nu = 0.29; alpha = 6.3e-06;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
        % Coordinates
        xPos = xLength*((ii-1)/xEles);
        yPos = 0;
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node object
        curvedBeam = curvedBeam.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            tempNode = tempNode.SetFixed([2,4,6]);
        end
        
        if ii == xEles/2 + 1
            curvedBeam.Add2Nset('CenterNode', tempNode);
        end
        
        if ii == xEles/4 + 1
            curvedBeam.Add2Nset('QuarterNode', tempNode);
        end
        
        % Create element
        if ii > 1
            b31Ele = B2(elementId); elementId = elementId + 1;
            eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
            b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                steel, beamSection,csVec);
            curvedBeam.AddElementObject(b31Ele);
        end
        
        nodeId = nodeId + 1;
end

% Build Constraints and DOF
curvedBeam = curvedBeam.Update();

% PLot Mesh
curvedBeam.Plot();

% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);
%% Perform linear modal analysis and plot a few modes
nmodes = 30; modal = Frequency('modal', curvedBeam); modal.Solve(nmodes);

figure; scale = 0.01;
for ii = 1:8
subplot(4,2,ii)
title(['Mode ', num2str(ii), ' : ',num2str(round(modal.fn(ii))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,ii),'norm'); view([0 -1 0]); grid('on');
end

%% Create different type Roms 

% Modes to use
mind = [1,3];
loadFactors = [0.5];
loadScalings = thk*ones(length(mind),1).*loadFactors;

% Create and build implicit condensation rom
icRom = IcRom('IcRom',curvedBeam);
icRom.Build(mind,loadScalings);

%% Compute NNMs

% Define Harmonic Balance Controls 
nH                  = 5;     % # of Harmonics
nu                  = 1;     % Subharmonic
mode                = 1;   % Mode #
w0                  = modal.fn(mode)*2*pi; % Starting frequency

HBcontrol.stpSign   = 1; % Sign of continuation step
HBcontrol.stpint    = 1e-4;  % initial step size
HBcontrol.stpmax    = 10;    % Max step size
HBcontrol.stpmin    = 1e-8; % Min step size
HBcontrol.optiter   = 10;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf     = 3;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter   = 20;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol       = 1e-3;  % Convergence tolerance
HBcontrol.nSols     = 10000;  % Max number of solutions
HBcontrol.nT        = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve  = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep    = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start     = true;
HBcontrol.save      = false;
HBcontrol.silent    = false;
HBcontrol.mode      = mode;  % Initial condition has a value for this mode of amplitude set below.  
HBcontrol.freq      = 'Hz';  % 'rad/s' or 'Hz'

% Initial Conditions
wn = modal.fn(mode)'*2*pi;
HBcontrol.x_int = zeros(icRom.m,1);
HBcontrol.x_int(mode) = 1e-6; % Initial amplitude of the mode of interest.

% Transform initial conditions in modal domain to frequency
Qt = zeros(1,2*nH); Qt(2) = 1;
Q = kron([0 Qt],eye((icRom.m)));
z0 = pinv(Q)*HBcontrol.x_int;

% Force function
fExt = zeros(length(mind),1);

% Instantiate classes
icRomMhb = HbRom('IcRomMhb',cd,icRom);

% Establish stopping criteria
stopCriteria.frequency = [0.25*modal.fn(mode)*2*pi modal.fn(mode)*2*pi*2];
stopCriteria.amplitude = [];
stopCriteria.force = [];
stopCriteria.energy = inf;
stopCriteria.interpFinal = false;

% Put controls onto each class
icRomMhb.stopCriteria = stopCriteria;

% Run Continuation (single mode roms)
icRomMhb = icRomMhb.Start(nH,nu,fExt,z0,w0,HBcontrol);
