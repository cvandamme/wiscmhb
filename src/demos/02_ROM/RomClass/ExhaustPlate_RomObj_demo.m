%% This scripts generates a finite element model of an exhuast plate,
%  creates a nonlinear reduced order model and then computes NNMs using the
%  multi-harmonic balance method. 
%  *** Note this demo requires the OsFern finite element library to work
%
%  Analyses Performed
%  1) Linear Modal
%  2) ROM Genearation via Implicit Condensation
%  3) NNM via Multi-Harmonic Balance method
clear; clc; close all; clear classes;

%% Build Model

% Read in exhuast plate mesh data
try
    load('exhaustCoverPlate.mat');
catch % incase its not on the path but should be if RunMe.m was ran
    load('..\..\01_FEM\ExhaustCover_Demo\exhaustCoverPlate.mat');
end

% Initialize MatFem Object
exhaustPlate = OsFern('ExhaustCover');

% Modulus and density
E = 168e9; rho = 5120; 

% Material Properties
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
shellSection.t = thickness;

% Setup constraints
endConstraints = [];

% Decidie if doing force/damped analysis or just NNM
forced = false;

% Creat basic mesh
nodeId = 1; elementId = 1;
for ii = 1:length(nodes)
        
    % Node Id 
    nodeId = nodes(ii,1);
    
    % Coordinates
    xPos = nodes(ii,2);
    yPos = nodes(ii,3);
    zPos = nodes(ii,4);
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node object
    exhaustPlate = exhaustPlate.AddNodeObject(tempNode);

    % Fixe nodes
    if any(nodeId==spcs(:,1))
        tempNode = tempNode.SetFixed();
        exhaustPlate.Add2Nset('BoundaryNodes',tempNode)
    end
    
end

for ii = 1:length(elements)
    
    % create 4-node shell element
    s4Ele = S4S(elementId); elementId = elementId + 1;
    nodeId = elements(ii,2:5);
    eleObjNodes = [exhaustPlate.node(nodeId(1)),...
                exhaustPlate.node(nodeId(2)),...
                exhaustPlate.node(nodeId(3)),...
                exhaustPlate.node(nodeId(4))];
    s4Ele = s4Ele.Build(eleObjNodes, steel, shellSection);
    exhaustPlate.AddElementObject(s4Ele);
    
end

% Build Constraints and DOF
exhaustPlate = exhaustPlate.Update();
exhaustPlate.Plot();


% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', exhaustPlate);
modal.Solve(nmodes);

% Initialize static object
static = NLStatic('static', exhaustPlate);

% Plot Mode shapes
f = figure; scale = 1/100;
tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
for ii = 1:length(modal.fn)
    modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    axes('Parent',modalTab{ii});
    title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    exhaustPlate.Plot(scale*modal.phi(:,ii),'norm');  view([0 0 -1]); colorbar
end

%% Create different type Roms

% Modes to use
mind = [1,6];
loadFactors = [0.5];
loadScalings = thickness*ones(length(mind),1).*loadFactors;

% Create and build implicit condensation rom
icRom = IcRom('IcRom',exhaustPlate);
icRom.Build(mind,loadScalings);

%% Compute NNMs

% Define Harmonic Balance Controls 
nH                  = 5;     % # of Harmonics
nu                  = 1;     % Subharmonic
mode                = 1;   % Mode # (within ROM, see mind above)
w0                  = modal.fn(mode)*2*pi; % Starting frequency
nDOF                = length(mind); % Number of dof in ROM

HBcontrol.stpSign   = 1; % Sign of continuation step
HBcontrol.stpint    = 1e-4;  % initial step size
HBcontrol.stpmax    = 10;    % Max step size
HBcontrol.stpmin    = 1e-8; % Min step size
HBcontrol.optiter   = 10;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf     = 3;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter   = 20;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol       = 1e-3;  % Convergence tolerance
HBcontrol.nSols     = 10000;  % Max number of solutions
HBcontrol.nT        = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve  = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep    = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start     = true;
HBcontrol.save      = false;
HBcontrol.silent    = false;
HBcontrol.mode      = mode;  % Initial condition has a value for this mode of amplitude set below.  
HBcontrol.freq      = 'Hz';  % 'rad/s' or 'Hz'

% Initial Conditions
wn = modal.fn(mode)'*2*pi;
HBcontrol.x_int = zeros(icRom.m,1);
HBcontrol.x_int(mode) = 1e-6; % Initial amplitude of the mode of interest.

% Create initial vector of harmonics and populate with initial guess
z0 = zeros((2*nH+1)*nDOF,1); % Initialize
    % Set appropriate cosine terms to the amplitude above
    z0(2*nDOF+[1:nDOF])=HBcontrol.x_int;

% Force function
fExt = zeros(length(mind),1); % zero for NNM

% Instantiate classes
icRomMhb = HbRom('IcRomMhb',cd,icRom);

% Establish stopping criteria
stopCriteria.frequency = [0.25*modal.fn(mode)*2*pi modal.fn(mode)*2*pi*2];
stopCriteria.amplitude = [];
stopCriteria.force = [];
stopCriteria.energy = inf;
stopCriteria.interpFinal = false;

% Put controls onto each class
icRomMhb.stopCriteria = stopCriteria;

% Run Continuation (single mode roms)
icRomMhb = icRomMhb.Start(nH,nu,fExt,z0,w0,HBcontrol);
