%% Test Harmonic Balance Continuation Routine on a 4-mode ROM of the
% flat clamped-clamped beam.
% Updated by MSA to fix initial conditions and check settings to get the
% algorithm to turn around the peak of the resonance.
clear; clc; close all;

% Setup Model 
SAVEdir = cd;
load('ccbeam_mind1357_cdpt5_rf1_withquads.mat')

% Which NLROM
ROMnum = 6;

% Nonlinearities 
[N1,N2,Nlist]=Knl2Nash(NLROM.Knl,NLROM.tlist);

% Define Harmonic Balance Controls 
nH                  = 3;     % # of Harmonics
nu                  = 1;     % Subharmonic
mode                = 1;   % Mode # % IGNORED FOR FORCED RESPONSE
nDOF                = length(NLROM.fn); % Number of dof

HBcontrol.stpSign     = 1; % Sign of continuation step
HBcontrol.stpint    = 1e-8;  % initial step size
HBcontrol.stpmax    = 10;    % Max step size
HBcontrol.stpmin    = 1e-10; % Min step size
HBcontrol.optiter   = 6;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf     = 2;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter   = 20;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol       = 1e-3;  % Convergence tolerance
HBcontrol.nSols     = 10000;  % Max number of solutions
HBcontrol.nT        = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve  = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep    = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start     = true;
HBcontrol.save      = false;
HBcontrol.silent    = false;
HBcontrol.startFlag = false; % True=start from oan initial ocndition defined by a mode
HBcontrol.mode      = mode;  % Initial condition has a value for this mode of amplitude set below.  
HBcontrol.freq      = 'Hz';  % 'rad/s' or 'Hz'

HBcontrol.XnormScale    = 1e8; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale    = 1; % This value determines scaling of initial step
HBcontrol.betamin       = 0; % This value determines scaling of initial step

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial Conditions - Start of FRF Curve
% Initialize MHB Solver - NEEDED?
%     wn = NLROM.fn'*2*pi;
%     HBcontrol.x_int = zeros(size(wn));
%     HBcontrol.x_int(mode) = 1e-6; % Initial amplitude of the mode of interest.

% Define the forcing function
w0 = 0.4*NLROM.fn(mode)*2*pi; % Starting frequency for FRF
    % Note, HBcontrol.stpSign above controls which direction it proceeds.
A = 0.1; % Forcing amplitude (A*cos(w*t) at some point)
    % Point where force is applied in physical space
    f_ind=116; % note, NLROM.DOF(116)=20.2
    Fextp=zeros(size(NLROM.phi,1),1);
        Fextp(f_ind)=A;
    % Convert to modal force
    Fext = NLROM.phi.'*Fextp; % Force in modal coordinates, assumed to be 
        % a cosine.

% Compute initial conditions for a liniear system forced at that frequency.
% Linear Properties
M = eye(length(NLROM.mind));
K = diag((NLROM.fn*2*pi).^2);
C = 20*M+1e-5*K;  % Define Mass and Stiffness Propotional Damping
    wn = NLROM.fn'*2*pi;
    zts = diag(C)./sqrt(diag(K))/2

    % Solve forced response problem for linearized system
    X0=(-w0^2*M+1i*w0*C+K)\(Fext); % complex amplitude
    % Create initial vector of harmonics and populate with initial guess
    z0 = zeros((2*nH+1)*nDOF,1); % Initialize\\
        % Set appropriate cosine terms to the amplitude above
        z0(nDOF+[1:2*nDOF])=[-imag(X0), real(X0)];
    
% THIS PART WAS IN ERROR, MATT REOMVED IT
% % Transform initial conditions in modal domain to frequency
% Qt = repmat([0 1],1,nH); 
% Q = kron([0 Qt],eye(length(wn)));
% z0 = pinv(Q)*HBcontrol.x_int;

% Added Loading in FE data to plot (right now not fully functional)
FEdata.phi = NLROM.phi;
FEdata.dof2plot = 116; % 2:6:length(DOF);
Fextphys =  NLROM.phi(:,1); % zeros(size(DOF))

Name = ['CC_Beam_Forced_',num2str(ROMnum),'_nH_',num2str(nH)];
SaveDir = cd;



% Instantiate Class
[HBO] = HBnlrom(Name,SAVEdir,M,C,K,N1,N2,Nlist,FEdata);

% Establish stopping criteria
HBO.stopCriteria.frequency = [0.01*NLROM.fn(mode)*2*pi 10*NLROM.fn(mode)*2*pi];
HBO.stopCriteria.amplitude = [];
HBO.stopCriteria.force = [];
HBO.stopCriteria.interpFinal = false;
HBO.HBcontrol = HBcontrol;

% Run Continuation
HBO = HBO.Start(nH,nu,Fext,z0,w0);