%% Harmonic Balance Continuation Routine
clear; clc; close all;

% Setup Model 
SAVEdir = cd;
load('ccbeam_mind1357_cdpt5_rf1_withquads.mat')

% Nonlinearities 
[N1,N2,Nlist]=Knl2Nash(NLROM.Knl,NLROM.tlist);

% Define Harmonic Balance Controls 
nH                  = 3;     % # of Harmonics
nu                  = 1;     % Subharmonic
mode                = 1;   % Mode #
w0                  = NLROM.fn(mode)*2*pi; % Starting frequency
nDOF                = length(NLROM.fn); % Number of dof

HBcontrol.stpSign     = 1; % Sign of continuation step
HBcontrol.stpint    = 1e-8;  % initial step size
HBcontrol.stpmax    = 10;    % Max step size
HBcontrol.stpmin    = 1e-8; % Min step size
HBcontrol.optiter   = 6;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf     = 2;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter   = 20;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol       = 1e-3;  % Convergence tolerance
HBcontrol.nSols     = 10000;  % Max number of solutions
HBcontrol.nT        = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve  = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep    = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start     = true;
HBcontrol.save      = false;
HBcontrol.silent    = false;
HBcontrol.startFlag = false; % true=start automatically based on a mode.
HBcontrol.mode      = mode;  % Initial condition has a value for this mode of amplitude set below.  
HBcontrol.freq      = 'Hz';  % 'rad/s' or 'Hz'

HBcontrol.XnormScale    = 1e8; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale    = 1; % This value determines scaling of initial step
HBcontrol.betamin       = 0; % This value determines scaling of initial step

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial Conditions
wn = NLROM.fn'*2*pi;
HBcontrol.x_int = zeros(nDOF,1);
HBcontrol.x_int(mode) = 1e-5; % Initial amplitude of the mode of interest.

% Create initial vector of harmonics and populate with initial guess
z0 = zeros((2*nH+1)*nDOF,1); % Initialize
    % Set appropriate cosine terms to the amplitude above
    z0(2*nDOF+[1:nDOF])=HBcontrol.x_int;

% Added Loading in FE data to plot (right now not fully functional)
FEdata.phi = NLROM.phi;
FEdata.dof2plot = 116; % note, NLROM.DOF(116)=20.2, midpoint of beam

Name = ['CC_Beam_ROM_nH_',num2str(nH)];
SaveDir = cd;

% Linear Properties
M = eye(length(NLROM.mind));
K = diag((NLROM.fn*2*pi).^2);
C = zeros(size(M)); % set to zero to ignore for NNM computation.
Fext = zeros(length(M(:,1)),1); % set to zero to ignore for NNM computation.

% Instantiate Class
[HBO] = HBnlrom(Name,SAVEdir,M,C,K,N1,N2,Nlist,FEdata);
    % HBO is an object that contains the data and is tied to functions that
    % can operate on the data, see below.

% Establish stopping criteria
HBO.stopCriteria.frequency = [0.25*NLROM.fn(mode)*2*pi 2*NLROM.fn(mode)*2*pi];
HBO.stopCriteria.amplitude = [];
HBO.stopCriteria.force = [];
HBO.stopCriteria.interpFinal = false;
HBO.HBcontrol = HBcontrol;

% Run Continuation
HBO = HBO.Start(nH,nu,Fext,z0,w0);
