%% Test Harmonic Balance Continuation Routine
%  This system is the 2DOF Nonlinear Tuned Vibration Absorber (NLTVA)
%   
%                    -->Fcos(wt)
%                   |
%                   |
%         Klin(1)  DOF(1)  Klin,abs   DOF(2)
%   |||---/\/\/\----[m]-----\/\/\/-----[m]
%         Knl(1)           Knl,abs       

clear; clc; close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exercise to get accustomed to the code:  Compute the forced response from
% 10% of resonance to 500% (5x) the resonance frequency for three force
% levels, A=0.05, 0.10, A=0.4.  Steps:
%   1.) Change the name of the save file below, Name='NLTVA_0p05';
%   2.) Change the forcing level below, A=0.05;
%   3.) Set the start frequency below, w0 = wn(mode)/10;
%   4.) Set the frequency range: HBparam.stopCriteria.frequency = [0.01*fn(mode)*2*pi fn(mode)*2*pi*5];
%   5.) Execute this script then click "Start Continuation" in the GUI.
%   6.) Load the data files NLTVA_0p05.mat, etc... to compare the curves
%   for each forcing level.
%   

% Setup Model 
Name = 'NLTVA_0p1';
SAVEdir = cd;

% Linear Properties
m1 = 1; m2 = 0.05*m1;
c = 0.002; cabs = 0.013; % was c=0.002
k1 = 1; kabs = 0.0453;

% Linear Matrices
M = [m1 0; 0 m2];
K = [k1+kabs, -kabs; 0, kabs];
C = [c+cabs, -cabs; 0, cabs];

% External forcing (not present for NNMs)
A = 0.1;
Fext = [A; 0];

% Perfrom Eigen Analysis
[phi,lam] = eig(K,M);
phi = phi*diag(diag(phi.'*M*phi).^-0.5);
wn = sqrt(diag(lam));
fn = wn/(2*pi);

% Nonlinaer Terms
knl = 1; knlabs = 0.0042;

% Nonlinear matrix
Knl = [0,0,0,knl+knlabs,-3*knlabs,3*knlabs,-knlabs;...
       0,0,0,-knlabs,3*knlabs,-3*knlabs,knlabs];
        
tlist = [1,1,0;
         2,2,0;
         1,2,0;
         1,1,1;
         1,1,2;
         2,2,1;
         2,2,2];
     
% Put in nash form
[N1,N2,Nlist]=Knl2Nash(Knl,tlist);

% Define Harmonic Balance Controls 
nH                  = 3;     % # of Harmonics
nu                  = 1;     % Subharmonic
mode                = 1;   % Mode #
w0 = wn(mode)/10; % Starting frequency

HBcontrol.stpSign     = 1; % Sign of continuation step
HBcontrol.stpint    = 1e-8;  % initial step size
HBcontrol.stpmax    = 0.5;    % Max step size
HBcontrol.stpmin    = 1e-8; % Min step size
HBcontrol.optiter   = 3;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf     = 2;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter   = 20;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol       = 1e-3;  % Convergence tolerance
HBcontrol.nSols     = 10000;  % Max number of solutions
HBcontrol.nT        = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve  = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep    = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start     = true;
HBcontrol.save      = true;
HBcontrol.silent    = false;
HBcontrol.startFlag = false;
HBcontrol.mode      = mode;  % Initial condition has a value for this mode of amplitude set below.  
HBcontrol.freq      = 'Hz';  % 'rad/s' or 'Hz'

HBcontrol.XnormScale    = 1e10; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale    = 1e0; % This value determines scaling of initial step
HBcontrol.betamin       = 0; % This value determines scaling of initial step

% Initial Conditions
HBcontrol.x_int = zeros(size(wn));
HBcontrol.x_int(mode) = 1e-6; % Initial amplitude of the mode of interest.

% Transform initial condtions to frequency domain
Qt = repmat([0 1],1,nH);
Q = kron([0 Qt],eye(length(wn)));
z0 = pinv(Q)*HBcontrol.x_int;

% Model is already in physical domain so phi is identity 
FEdata.phi = eye(2);
FEdata.dof2plot = [1,2];

% Instantiate Class
[HBparam] = HBnlrom(Name,SAVEdir,M,C,K,N1,N2,Nlist,FEdata);

% Establish stopping criteria
HBparam.stopCriteria.frequency = [0.01*fn(mode)*2*pi fn(mode)*2*pi*5];
HBparam.stopCriteria.amplitude = [];
HBparam.stopCriteria.force = [];
HBparam.stopCriteria.interpFinal = false;
HBparam.HBcontrol = HBcontrol;

% Run Continuation
HBparam = HBparam.Start(nH,nu,Fext,z0,w0);
