%% Test Harmonic Balance Continuation Routine
clear; clc; close all;

% Setup Model 
SAVEdir = cd;
load('Curved_Beam_4_Roms.mat')

% Which ROM
ROMnum = 6;
ROM = ICEarray(ROMnum);

% Nonlinearities 
[N1,N2,Nlist]=Knl2Nash(ROM.Knl,ROM.tlist);

% Added Loading in FE data to plot (right now not fully functional)
FEdata.phi = ROM.phi;
FEdata.dof2plot = 116; % 2:6:length(DOF);
Fextphys =  ROM.phi(:,1); % zeros(size(DOF));

% Define Harmonic Balance Controls 
nH                  = 3;     % # of Harmonics
nu                  = 1;     % Subharmonic
mode                = 1;   % Mode #
w0                  = ROM.fn(mode)*2*pi; % Starting frequency

HBcontrol.stpSign     = 1; % Sign of continuation step
HBcontrol.stpint    = 1e-8;  % initial step size
HBcontrol.stpmax    = 10;    % Max step size
HBcontrol.stpmin    = 1e-8; % Min step size
HBcontrol.optiter   = 6;     % Optimal number of iterations.  Stepsize is increased/decreased to push towarsd this target.
HBcontrol.stpdf     = 2;     % amount by which to cut stepsize if a prediction fails to converge.
HBcontrol.maxiter   = 20;    % Maximum number of iterations before giving up and cutting stepsize.

HBcontrol.tol       = 1e-3;  % Convergence tolerance
HBcontrol.nSols     = 10000;  % Max number of solutions
HBcontrol.nT        = 50;    % Number of time samples in HB.  Minimum = 2*nH+1
HBcontrol.LinSolve  = 'Direct'; % Direct solution with Full Matrices or implement Sparse Matrices
HBcontrol.nbstep    = 1000;  % Number of steps in Linear Solution Method
HBcontrol.divfactor = 5;    % Allowable factor for a specific correction to diverge before cutting stepsize
HBcontrol.start     = true;
HBcontrol.save      = false;
HBcontrol.silent    = false;
HBcontrol.startFlag = false;
HBcontrol.mode      = mode;  % Initial condition has a value for this mode of amplitude set below.  
HBcontrol.freq      = 'Hz';  % 'rad/s' or 'Hz'

HBcontrol.XnormScale    = 1e8; % This value determine scaling of initial step (needs some work)
HBcontrol.wNormScale    = -1; % This value determines scaling of initial step
HBcontrol.betamin       = 0; % This value determines scaling of initial step

% Initial Conditions
wn = ROM.fn'*2*pi;
HBcontrol.x_int = zeros(size(wn));
HBcontrol.x_int(mode) = 1e-6; % Initial amplitude of the mode of interest.

% Transform initial conditions in modal domain to frequency
Qt = repmat([0 1],1,nH); 
Q = kron([0 Qt],eye(length(wn)));
z0 = pinv(Q)*HBcontrol.x_int;

% Added Loading in FE data to plot (right now not fully functional)
FEdata.phi = ROM.phi;
FEdata.dof2plot = 116; % 2:6:length(DOF);

Name = ['CurveBeam_Forced_ROM_',num2str(ROMnum),'_nH_',num2str(nH)];
SaveDir = cd;

% Linear Properties
M = eye(length(ROM.mind));
K = diag((ROM.fn*2*pi).^2);
alph = 0.02;
beta = 1e-7;
C = alph*M + beta*K;
wn = ROM.fn'*2*pi;

A = 0.225;
Fext = zeros(length(ROM.mind),1);
Fext(mode) = A;

% Instantiate Class
[curvedBeamMhb] = HBnlrom(Name,SAVEdir,M,C,K,N1,N2,Nlist,FEdata);

% Establish stopping criteria
curvedBeamMhb.stopCriteria.frequency = [0.25*ROM.fn(mode)*2*pi ROM.fn(mode)*2*pi*2];
curvedBeamMhb.stopCriteria.amplitude = [];
curvedBeamMhb.stopCriteria.force = [];
curvedBeamMhb.stopCriteria.interpFinal = false;
curvedBeamMhb.HBcontrol = HBcontrol;

% Run Continuation
curvedBeamMhb = curvedBeamMhb.Start(nH,nu,Fext,z0,w0);